/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @module MessageSingleCtrl
 */
'use strict';
var _msgActions = {
    abuse: {
        key: 'standalone.REPORT_ABUSE',
        fn: function (self) {
            return self._abuseModal.activate({
                content: {
                    DISPLAY_TYPE: 'Message/Conversation',
                    SUBTYPE: 'messages',
                    CONTENT_ID: self._$scope.convo.msgID,
                    TIMESTAMP: self._Config.format.date.toString('basic_date_time_no_millis')
                },
                modal: {
                    deactivate: self._abuseModal.deactivate
                }
            });
        }
    }
};
var MessageSingleCtrl = (function () {
    function MessageSingleCtrl(_$scope, _$ionicScrollDelegate, _Conversation, _User, _Messaging, _SocketIo, _abuseModal, _Config, _keyboardFilter, _locStorage, DataBus) {
        this._$scope = _$scope;
        this._$ionicScrollDelegate = _$ionicScrollDelegate;
        this._Conversation = _Conversation;
        this._User = _User;
        this._Messaging = _Messaging;
        this._SocketIo = _SocketIo;
        this._abuseModal = _abuseModal;
        this._Config = _Config;
        this._keyboardFilter = _keyboardFilter;
        this._locStorage = _locStorage;
        var appendFn, eventKey, self = this, autoEnter = _locStorage.get('messaging.autoEnter'), people = _Conversation.people, otherPersonID = _Conversation.conversation.participants[0], participant = (people || _User.users)[otherPersonID];
        // If in the route resolve, people were requested, then we need to add them to the service
        if (people) {
            _User.addPeople(people, null, true);
        }
        _$scope.convo = _Conversation.conversation;
        _$scope.messages = _.map(_Conversation.messages.reverse(), function (msg) {
            msg.parsed = _Messaging.parseMessage(msg.message);
            return msg;
        });
        _$scope.people = _User.users;
        this.conversationRead();
        _.defer(function () {
            _$ionicScrollDelegate.scrollBottom();
        });
        this.data = this.vars = {
            message: '',
            ddVisible: false,
            ddOptions: _msgActions,
            autoEnter: false
        };
        if (typeof autoEnter === 'string') {
            this.data.autoEnter = autoEnter === 'true';
        }
        if (!MessageSingleCtrl.prototype.whenFrom) {
            MessageSingleCtrl.prototype.whenFrom = _Messaging.whenFrom.bind(_Messaging);
        }
        _.defer(function () {
            DataBus.emit('/tb/message', { text: participant.fullName });
        });
        appendFn = this.newMsg.bind(this);
        eventKey = _Messaging.generateKey(_Conversation.conversation.msgID);
        // Join the socket room
        this._messageHandle = this._SocketIo.join(eventKey, appendFn);
        _$scope.$on('$destroy', function () {
            // remove footer nav
            DataBus.emit('/nav/subfooter/change');
            // remove the handle on destroy so we don't get duplicate calls if we rejoin the same room
            self._messageHandle.remove();
        });
    }
    MessageSingleCtrl.prototype.isMe = function (userID) {
        return userID === this._User.userID;
    };
    MessageSingleCtrl.prototype.sendMsg = function (event) {
        // prevent a page refresh
        event.preventDefault();
        var self = this, message = this.data.message;
        self.data.message = '';
        if (!message) {
            return false;
        }
        return this._Messaging.send({
            to: this._Conversation.conversation.participants,
            message: message,
            channel: this._messageHandle.channel
        })
            .then(function () {
            $('.write')[0].focus();
        }).catch(function () {
            self.data.message = message;
        }).finally(function () {
            self._$scope.$root.safeDigest(self._$scope);
        });
    };
    MessageSingleCtrl.prototype.enterCheck = function (event) {
        if (this.data.autoEnter && this._keyboardFilter(event.keyCode) === 'enter') {
            return this.sendMsg();
        }
    };
    MessageSingleCtrl.prototype.autoEnterChange = function () {
        return this._locStorage.set('messaging.autoEnter', this.data.autoEnter);
    };
    /**
     * When a new message comes in will append the message to the body and notify the notifications
     *
     * @param {object} msg  the message object received from the server
     */
    MessageSingleCtrl.prototype.newMsg = function (msg) {
        var self = this;
        // Append the message
        this.appendMsg(msg);
        // Notify the notification that this has been viewed
        _.delay(function () {
            self._Messaging.markRead(self._$scope.convo.msgID);
        }, 25);
    };
    MessageSingleCtrl.prototype.appendMsg = function (msg) {
        var $scroll = this._$ionicScrollDelegate;
        msg.parsed = this._Messaging.parseMessage(msg.message);
        this._$scope.messages.push(msg);
        $scroll.scrollBottom();
        this._$scope.$root.safeDigest(this._$scope);
    };
    MessageSingleCtrl.prototype.conversationRead = function () {
        var conversation = this._Conversation.conversation;
        // Update notifications
        return this._Messaging.markRead(conversation.msgID);
    };
    return MessageSingleCtrl;
})();
MessageSingleCtrl.$inject = [
    '$scope',
    '$ionicScrollDelegate',
    'Conversation',
    'User',
    'Interaction',
    'SocketIo',
    'abuseModal',
    'Config',
    'keyboardFilter',
    'localStorageService',
    'DataBus',
];
require('../../../app').controller('MessageSingleCtrl', MessageSingleCtrl);
//# sourceMappingURL=Ctrl.js.map