/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * @module fMessagingListCtrl
 */
'use strict';

class MessagingListCtrl {
    private _sortedConversations:cp.interaction.IConversation[];

    public people:cp.user.IUserCache;
    public conversations:cp.interaction.IConversationCache;

    constructor(
        private _$scope:cp.IScope,
        private _Interaction:cp.interaction.IService,
        private _Util:any,
        private _User:cp.user.IService,
        private _$state:ng.ui.IStateService,
        feedbackModal:any,
        Notifications:cp.notification.IService,
        locale:any,
        DataBus:any
    ) {
        _$scope.vars = {
            triggerFeedback: function () {
                return feedbackModal.activate({
                    modal: {
                        deactivate: feedbackModal.deactivate
                    }
                });
            },
            from: 0,
            size: 10,
            loadingMore: true
        };

        /**
         * @description will store an array of all of the conversations - sorted
         * @type {(null|array<object>)}
         */
        this._sortedConversations = null;
        this.conversations = this.getConvos();
        this.people = _Interaction.people;

        var checkNotification = this.checkNewNotification.bind(this);
        Notifications.on('/new', checkNotification);


        locale.ready('notifications').then(function () {
            _.defer(function () {
                DataBus.emit('/tb/message', { text: locale.getString('notifications.ALL_MSGS') });
            })
        });

        _$scope.$on('$destroy', function () {
            Notifications.removeListener('/new', checkNotification);
        });
    }

    public getConvos():cp.interaction.IConversationCache {
        var vars = this._$scope.vars,
            curLength = this._Interaction.convosLen;

        vars.loadingMore = true;

        this._Interaction.fetch({
            withUsers: true,
            from: vars.from,
            size: vars.size
        })
        .then(function () {
            if (this._Interaction.convosLen !== curLength) {
                this._sortedConversations = null;
            }

            vars.from += vars.size;
            vars.loadingMore = false;
            this._$scope.$root.safeDigest(this._$scope);
        }.bind(this));

        return this._Interaction.conversations;
    }

    /**
     * @description gets called whenever a new notification comes in
     * This checks whether the notification is relevant to messaging and updates the conversations if so
     * @param {object} data
     */
    checkNewNotification(data) {
        var key = CP.Settings.NOTIFICATION;

        // Iterate each new notification
        _.each(data, function (note) {
            // If message type matches
            if ((key.source[note.source] === 'INTERACTIONS' ||
                 key.sourceTypes[note.sourceType] === 'MESSAGE') &&

                !_.some(this.conversations, function (convo) {
                    // Check through all of our existing conversations
                    // If it's a match then update the convo
                    if (convo.msgID === note.sourceID) {

                        this._sortedConversations = null;
                        convo.snippet = note.snippet;
                        convo.fromNow = this._Interaction.whenFrom(note.when, true);
                        convo.upDate = this._Interaction.upDate(note.when);

                        // Adding the Z is a hack to have this timestamp appear ahead of all the others
                        convo.updated = 'Z' + String((new Date()).getTime());

                        if (this._$state.params.msgID !== note.sourceID) {
                            return this._Util.arrRemove(convo.read, this._User.userID);
                        }

                        this._$scope.$root.safeDigest(this._$scope);

                        return true;
                    }

                    return false;
                }, this)
            ) {
                // If a completely new message then fetch the conversation and add it
                return this._Interaction.get({
                    conversation: true,
                    msgID: note.sourceID
                }).then(function () {
                    return this._$scope.$root.safeDigest(this._$scope);
                }.bind(this));
            }
        }, this);
    }

    /**
     * @description Returns the current list of sorted conversations
     * @returns {array<object>}
     */
    sortConvos() {
        if (this._sortedConversations) {
            return this._sortedConversations;
        }

        return this._sortedConversations = _.map(Object.keys(this.conversations), function (key) {
            return this.conversations[key];
        }, this).sort(function (a, b) {
            return a.updated < b.updated;
        });
    }

    /**
     * @description returns whether there are more conversations to load
     * @returns {boolean}
     */
    showLoadMore() {
        return this._Interaction.convosLen < this._Interaction.totalConvosLen ||
            this._$scope.vars.from < this._Interaction.convosLen;
    }

    /**
     * @description loads the next conversation
     * @returns {Promise}
     */
    nextConversation() {
        return this._$state.go('pStandalone.messages.single', {
            msgID: this.sortConvos()[0].msgID
        });
    }

    /**
     * @description returns whether a conversation has been viewed by the current user
     * @param conversation
     * @returns {boolean}
     */
    isUnread(conversation) {
        return this._Interaction.isUnread(conversation);
    }

    /**
     * @description returns whether the current user has any conversations
     * @name #hasConversations
     * @returns {boolean}
     */
    hasConversations() {
        return this._Util.obj.notEmpty(this.conversations);
    }
}

MessagingListCtrl.$inject = [
    '$scope',
    'Interaction',
    'Util',
    'User',
    '$state',
    'feedbackModal',
    'Notifications',
    'locale',
    'DataBus',
];

require('../../../app').controller('MessagingListCtrl', MessagingListCtrl);
