/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import App = require('../app');
import {Interaction} from 'root/interaction/Service';

App.service('Interaction', [
    '$q',
    'SocketIo',
    'User',
    'Config',
    'DataBus',
    '$sce',
    'InteractionHelper',
    'InteractionMap',
    Interaction,
]);
