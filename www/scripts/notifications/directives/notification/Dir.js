/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @module notificationDirective
 * @restrict E
 */
'use strict';
var listeners = 0, initListener = function (event) {
    var scope = angular.element(event.currentTarget).scope();
    return scope.vars[event.data.source](event);
};
require('../../../app').directive('notification', [
    'Notifications', 'Util', '$templateCache',
    function (Notifications, Util, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                data: '='
            },
            template: $templateCache.get('views/notifications/directives/notification/Template.html'),
            link: {
                post: function (scope) {
                    scope.vars = {
                        /**
                         * @ngdoc method
                         * @description marks a notification as read without without reading it
                         *
                         * @param {Event} event  click event
                         * @returns {Boolean} false
                         */
                        manualMark: function (event) {
                            Notifications.markAsRead([scope.data.nID], !scope.data.read);
                            return false;
                        },
                        read: function (event) {
                            Notifications.markAsRead([scope.data.nID], !scope.data.read);
                            return true;
                        }
                    };
                    if (++listeners === 1) {
                        CP.Cache.$document.on('click', '.notification .mark-read', {
                            source: 'manualMark'
                        }, initListener);
                        CP.Cache.$document.on('click', '.notification', {
                            source: 'read'
                        }, initListener);
                    }
                    return scope.$on('$destroy', function () {
                        if (--listeners === 0) {
                            return CP.Cache.$document.off('click', '.toggle-comment', initListener);
                        }
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map