/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {Notifications} from 'root/notification/Service';

require('../app').service('Notifications', [
    'DataBus',
    '$q',
    'SocketIo',
    'locale',
    'User',
    'Config',
    '$state',
    Notifications,
]);
