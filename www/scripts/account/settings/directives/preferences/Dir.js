/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var Directive = require('../../../../app').directive, dirModule = require('root/profile/settings/preferencesDir');
Directive('settingsEmailPreferences', [
    'Util',
    dirModule.email,
]);
Directive('settingsRequestPreferences', [
    'Util',
    dirModule.requests,
]);
//# sourceMappingURL=Dir.js.map