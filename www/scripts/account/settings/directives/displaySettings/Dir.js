/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app.js').directive('settingsDisplaySettings', [
    'DataBus', 'Util',
    function (DataBus, Util) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: CP.site.views + '/account/settings/directives/displaySettings/Template.html',
            scope: {
                source: '=',
                onSave: '&'
            },
            link: {
                pre: function (scope) {
                    scope.vars = {
                        clone: _.cloneDeep(scope.source)
                    };
                },
                post: function (scope) {
                    return scope.vars.save = function () {
                        var updates = Util.objectDifferences(scope.source, scope.vars.clone);
                        if (!_.keys(updates).length) {
                            return false;
                        }
                        scope.onSave({
                            updates: updates
                        });
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map