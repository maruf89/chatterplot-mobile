/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app').directive('settingsTimeZone', [
    'API',
    function (API) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: CP.site.views + '/account/settings/directives/timeZone/Template.html',
            scope: {
                current: '='
            },
            compile: function () {
                var vars = {
                    timeZones: []
                };
                return {
                    pre: function (scope) {
                        scope.vars = vars;
                        API.settings.timeZone()
                            .then(function (timeZones) {
                            _.each(timeZones, function (tz) {
                                if (tz.offset === scope.current) {
                                    vars.current = tz;
                                    return false;
                                }
                            });
                            // Get all the zones and save as array to preserve order
                            vars.timeZones = _.values(timeZones);
                            scope.$root.safeDigest(scope);
                        });
                    }
                };
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map