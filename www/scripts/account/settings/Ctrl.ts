/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

class SettingsCtrl {
    public my:any;
    public setting:any;

    constructor(
        private _$scope:cp.IScope,
        private _modalConfirm:any,
        private _User:cp.user.IService,
        private _Config:any,
        private _locale:any,
        private _Auth:any,
        passwordNotSet:any
    ) {
        this.my = _User.data;

        _$scope.obj = {
            passwordNotSet: passwordNotSet
        };

        this.setting = _Config.settings;
    }

    /**
     * @description Activates/Deactivates a modal
     * 
     * @param  {string} action  activate|deactivate
     * @param  {string} which   which modal to activate. Corresponds to factory object
     * @return {Boolean}        Whether it was successful
     */
    modalAction(action, which) {
        var ref = this['_' + which];

        if (ref) {
            return !!ref[action]();
        }

        return false;
    }

    /**
     * @description Saves the user's settings
     * 
     * @param  {string} which    Different sections have different things to update
     * @param  {object} updates  Updates to make
     * @return {Promise}
     */
    saveSettings(which, updates) {
        var doUpdate;

        switch (which) {
            case 'emailPref':
                doUpdate = { notifs: updates };
                break;
            case 'requests':
                doUpdate = { requests: updates };
                break;
            case 'settings':
                doUpdate = updates;
        }

        // Update settings and log the success message 
        return this._Config.updateSettings(doUpdate);
    }

    /**
     * @description Updates the user's password
     * TODO: Build in a check on the backend to require the old password to check
     * against the new one | Currently: a user could reset the password from the inspector
     * 
     * @param  {string} newPassword
     * @return {Promise}
     */
    savePassword(newPassword) {
        var $scope = this._$scope;

        return this._User.update({
            password: newPassword
        }, true)
        .then(function () {
            // require the old password if not already set
            $scope.obj.passwordNotSet = false;
            $scope.$root.safeDigest($scope);
        });
    }

    /**
     * @description Updates the user's profile document with the passed in changes
     * 
     * @param  {object} updateObj
     * @return {Promise}
     */
    save(updateObj) {
        return this._User.update(updateObj).then(function () {
            return _.extend(this.my, updateObj);
        }.bind(this));
    }

    /**
     * @description Delete's the users own account
     * !IMPORTANT: this action is undoable once deleted
     * 
     * @return {Promise}
     */
    deleteAccount() {
        var context = this;

        return this._locale.ready('admin')
            .then(function () {
                var values = {
                    title: context._locale.getString('admin.DELETE_ACCNT'),
                    message: context._locale.getString('admin.DELETE_OWN_ACCNT_MESSAGE'),
                    confirmText: context._locale.getString('common.DELETE'),
                    cancelClass: 'btn-white',
                    confirmClass: 'btn-red'
                };
                return context._modalConfirm.activate(values);
            })
            .then(function (close) {
                return context._Auth.deleteAccount({
                    deleteSelf: true
                })["finally"](close);
            });
    }
}

SettingsCtrl.$inject = [
    '$scope',
    'modalConfirm',
    'User',
    'Config',
    'locale',
    'Auth',
    'passwordNotSet'
];

require('../../app.js').controller('SettingsCtrl', SettingsCtrl);
