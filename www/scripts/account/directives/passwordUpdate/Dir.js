/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('settingsPasswordUpdate', [
    'SocketIo', '$templateCache',
    function (SocketIo, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/account/directives/passwordUpdate/Template.html'),
            scope: {
                disableOldPassword: '=',
                onSave: '&',
                forgotPassword: '&',
            },
            link: {
                pre: function (scope) {
                    scope.vars = {
                        oldPassword: null,
                        oldPasswordValid: null,
                        disableOldPassword: scope.disableOldPassword
                    };
                    scope.$watch('disableOldPassword', function (newer) {
                        scope.vars.disableOldPassword = newer;
                    });
                },
                post: function (scope) {
                    var vars = scope.vars;
                    vars.save = function (pass) {
                        var func = scope.onSave(pass);
                        if (!scope.disableOldPassword) {
                            func.then(function () {
                                scope.vars.oldPassword = '';
                                scope.passwordUpdate.$setPristine();
                                return scope.passwordUpdate.$setUntouched();
                            });
                        }
                        return func;
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map