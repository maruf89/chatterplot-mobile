/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var Auth_1 = require('root/services/Auth');
require('../../app').service('Auth', [
    'DataBus',
    'localStorageService',
    'SocketIo',
    'Request',
    '$q',
    'Permissions',
    '$rootScope',
    '$http',
    'Config',
    '$state',
    'Util',
    'AuthenticateModal',
    'ipCookie',
    Auth_1.AuthService,
]);
//# sourceMappingURL=AuthService.js.map