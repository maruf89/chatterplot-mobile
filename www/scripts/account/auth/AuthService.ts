/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {AuthService} from 'root/services/Auth';

require('../../app').service('Auth', [
    'DataBus',
    'localStorageService',
    'SocketIo',
    'Request',
    '$q',
    'Permissions',
    '$rootScope',
    '$http',
    'Config',
    '$state',
    'Util',
    'AuthenticateModal',
    'ipCookie',
    AuthService,
]);
