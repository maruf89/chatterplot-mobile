/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
class ModalService implements cp.modal.IService {
    private _deferred:ng.IDeferred<string>;
    private _modal:any;
    private _onDeactivate:() => void;
    private _template:string;

    public modalScope:any;

    /**
     * If true won't deactivate on route change
     */
    private _pagePersist:boolean;

    public activeName:string;

    constructor(
        private _$ionicModal:any,
        private _$q:ng.IQService,
        private _$templateCache:ng.ITemplateCacheService,
        private _$rootScope:cp.IRootScopeService
    ) {
        // close modal on state change
        _$rootScope.$on('$stateChangeSuccess', this._closeUnpersist.bind(this));

        this._template = _$templateCache.get('views/modals/Template.html');
    }

    private _initModal(opts:{template?:string;templateUrl?:string}):ng.IPromise<any> {
        var context = this,
            method = opts.templateUrl ? 'fromTemplateUrl' : 'fromTemplate';

        return this._$q.when(this._$ionicModal[method](opts.template || opts.templateUrl, opts)).then(function (modal) {
            return context._modal = modal;
        });
    }

    /**
     * @description Initiates the modal
     *
     * @param {object} opts
     * @param {string} opts.template - class to apply to modal box
     * @param {string=} opts.class - class to apply to root template
     * @param {object=} opts.vars
     */
    public activate(opts:any):ng.IPromise<string> {
        this._deferred = this._$q.defer();

        this._modal && this._modal.isShown() && this.deactivate();

        var deactivate = this.deactivate.bind(this),
            vars = opts.vars || {},
            onSubmit = this._onSubmit.bind(this, vars),
            context = this;

        opts.scope = this._$rootScope.$new(true);
        this._pagePersist = opts.pagePersist;

        _.extend(opts.scope, {
            window: {
                deactivate: deactivate,
                onSubmit: onSubmit,
                'class': opts.class || '',
            }
        }, vars);

        this._initModal(opts).then(function (modal) {
            modal.show();
            context.modalScope = modal.scope;
            context.activeName = opts.name;
        });

        return this._deferred.promise;
    }

    public isActive(name:string):boolean {
        return this.activeName && name === this.activeName;
    }

    private _onSubmit(vars:any, what?:any):void {
        this._deferred && this._deferred.resolve(what || vars);
        this._deferred = null;
    }

    private _closeUnpersist():void {
        !this._pagePersist && this.deactivate();
    }

    public deactivateName(name:string):void {
        if (this.activeName === name) {
            this.deactivate();
        }
    }

    public show():ng.IPromise<any> {
        return this._modal.show();
    }

    public hide():ng.IPromise<any> {
        return this._modal.hide();
    }

    public deactivate(noReject?:boolean):void {
        if (!this._modal || !this._modal.isShown()) {
            return;
        }

        this._modal.remove();
        this._modal = null;
        this._pagePersist = null;
        this.activeName = null;
        this.modalScope = null;

        if (!noReject) {
            this._deferred && this._deferred.reject();
            this._deferred = null;
        }

        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    }
}

ModalService.$inject = [
    '$ionicModal',
    '$q',
    '$templateCache',
    '$rootScope',
    '$state',
];

require('../app').service('ModalService', ModalService);
