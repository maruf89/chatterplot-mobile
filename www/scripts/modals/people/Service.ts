/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * @description What we need from each user
 *
 * TODO: since this got moved to a peopleDisplay directive, this doesn't belong here
 *
 * @type {array<string>}
 * @memberof PeopleDisplay
 */
var _requiredUserFields = [
    'userID',
    'firstName',
    'lastName',
    'languages',
    'details',
    'city',
    'state',
    'country',
    'picture.default'
];

/**
 * @classdesc A modal that loads a people-display directive which showcases users
 * @class
 * @requires btfModal
 * @requires $rootScope
 * @requires $state
 */
class PeopleDisplay {
    constructor(
        private _Modal:any
    ) {}

    activate(opts, force) {
        return this._Modal.activate({
            name: 'peopleShow',
            templateUrl: 'views/modals/people/Template.html',
            vars: {
                modal: {
                    sections: opts.sections,
                    category: opts.category,
                }
            }
        });
    }

    getRequiredFields() {
        return _.clone(_requiredUserFields);
    }
}

PeopleDisplay.$inject = [
    'ModalService',
];

require('../../app').service('PeopleDisplay', PeopleDisplay);
