/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * @description What we need from each user
 *
 * TODO: since this got moved to a peopleDisplay directive, this doesn't belong here
 *
 * @type {array<string>}
 * @memberof PeopleDisplay
 */
var _requiredUserFields = [
    'userID',
    'firstName',
    'lastName',
    'languages',
    'details',
    'city',
    'state',
    'country',
    'picture.default'
];
/**
 * @classdesc A modal that loads a people-display directive which showcases users
 * @class
 * @requires btfModal
 * @requires $rootScope
 * @requires $state
 */
var PeopleDisplay = (function () {
    function PeopleDisplay(_Modal) {
        this._Modal = _Modal;
    }
    PeopleDisplay.prototype.activate = function (opts, force) {
        return this._Modal.activate({
            name: 'peopleShow',
            templateUrl: 'views/modals/people/Template.html',
            vars: {
                modal: {
                    sections: opts.sections,
                    category: opts.category,
                }
            }
        });
    };
    PeopleDisplay.prototype.getRequiredFields = function () {
        return _.clone(_requiredUserFields);
    };
    return PeopleDisplay;
})();
PeopleDisplay.$inject = [
    'ModalService',
];
require('../../app').service('PeopleDisplay', PeopleDisplay);
//# sourceMappingURL=Service.js.map