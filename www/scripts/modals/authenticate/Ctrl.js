/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var AuthenticateCtrl_1 = require('root/modals/AuthenticateCtrl');
require('../../app').controller('AuthenticateModalCtrl', [
    'FB',
    'LinkedIn',
    'GPlus',
    '$scope',
    'Auth',
    '$state',
    'DataBus',
    'Config',
    '$location',
    '$analytics',
    'locale',
    AuthenticateCtrl_1.AuthenticateModalCtrl,
]);
//# sourceMappingURL=Ctrl.js.map