/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
var AuthenticateModal = (function () {
    function AuthenticateModal(_DataBus, _Modal, _$templateCache) {
        this._DataBus = _DataBus;
        this._Modal = _Modal;
        this._$templateCache = _$templateCache;
        this._name = 'authenticate';
        this._deactivating = null;
        this._isHidden = null;
    }
    AuthenticateModal.prototype.activate = function (which, opts) {
        var vars = _.extend({
            view: which,
            'class': opts.modalClass,
        }, opts), DataBus = this._DataBus, options = {
            name: this._name,
            pagePersist: true,
            vars: {
                vars: vars,
                modal: {
                    hide: this.hide.bind(this),
                    deactivate: this.deactivate.bind(this),
                    onReactivate: null,
                },
            },
            template: this._$templateCache.get('views/modals/authenticate/Template.html'),
        }, modalScope, closeFn, promise;
        if (!this._deactivating && this.isActive()) {
            modalScope = this._Modal.modalScope;
            if (this._isHidden) {
                this.show();
            }
            if (modalScope.modal.onReactivate) {
                modalScope.modal.onReactivate(vars);
            }
        }
        else {
            closeFn = this._Modal.deactivateName.bind(this._Modal, this._name);
            promise = this._Modal.activate(options).finally(function () {
                DataBus.removeListener('/site/authChange', closeFn);
            });
            DataBus.once('/site/authChange', closeFn);
        }
        return opts.dedicated ? true : promise;
    };
    AuthenticateModal.prototype.isActive = function () {
        return this._Modal.isActive(this._name);
    };
    AuthenticateModal.prototype.show = function () {
        this._isHidden = false;
        return this._Modal.show();
    };
    AuthenticateModal.prototype.hide = function () {
        this._isHidden = true;
        return this._Modal.hide();
    };
    AuthenticateModal.prototype.deactivate = function (callback) {
        this._Modal.deactivateName(this._name);
        callback && callback();
    };
    return AuthenticateModal;
})();
AuthenticateModal.$inject = [
    'DataBus',
    'ModalService',
    '$templateCache',
];
require('../../app').service('AuthenticateModal', AuthenticateModal);
//# sourceMappingURL=Service.js.map