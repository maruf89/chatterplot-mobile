/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
class AuthenticateModal {
    private _name:string = 'authenticate';
    private _deactivating:boolean = null;
    private _isHidden:boolean = null;


    constructor(
        private _DataBus:NodeJS.EventEmitter,
        private _Modal:any,
        private _$templateCache:ng.ITemplateCacheService
    ) {}

    public activate(which, opts:{view:string; dedicated?:boolean; modalClass?:string}):boolean | ng.IPromise<any> {
        var vars = _.extend({
                view: which,
                'class': opts.modalClass,
            }, opts),

            DataBus = this._DataBus,
            options = {
                name: this._name,
                pagePersist: true,
                vars: {
                    vars: vars,
                    modal: {
                        hide: this.hide.bind(this),
                        deactivate: this.deactivate.bind(this),
                        onReactivate: null,
                    },
                },
                template: this._$templateCache.get('views/modals/authenticate/Template.html'),
            },
            modalScope:any,
            closeFn:Function,
            promise;

        if (!this._deactivating && this.isActive()) {
            modalScope = this._Modal.modalScope;

            if (this._isHidden) {
                this.show();
            }

            if (modalScope.modal.onReactivate) {
                modalScope.modal.onReactivate(vars);
            }

        } else {
            closeFn = this._Modal.deactivateName.bind(this._Modal, this._name);
            promise = this._Modal.activate(options).finally(function () {
                DataBus.removeListener('/site/authChange', closeFn);
            });

            DataBus.once('/site/authChange', closeFn);
        }

        return opts.dedicated ? true : promise;
    }

    public isActive():boolean {
        return this._Modal.isActive(this._name);
    }

    public show():ng.IPromise<any> {
        this._isHidden = false;
        return this._Modal.show();
    }

    public hide():ng.IPromise<any> {
        this._isHidden = true;
        return this._Modal.hide();
    }

    public deactivate(callback?:Function):void {
        this._Modal.deactivateName(this._name)

        callback && callback();
    }
}

AuthenticateModal.$inject = [
    'DataBus',
    'ModalService',
    '$templateCache',
];

require('../../app').service('AuthenticateModal', AuthenticateModal);
