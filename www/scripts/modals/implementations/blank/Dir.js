/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var compileFn_1 = require('../compileFn');
require('../../../app').directive('blankModal', [
    'locale', 'DataBus', '$templateCache',
    function (locale, Bus, $templateCache) {
        return {
            restrict: 'E',
            scope: false,
            replace: true,
            transclude: true,
            template: $templateCache.get('views/modals/implementations/blank/Transclude.html'),
            compile: function (tElement, tAttrs) {
                var container = tElement.find('ion-content'), content = tElement.find('.modal-inner');
                compileFn_1.Prereq(tAttrs, content, container);
                return {
                    pre: function (scope, iElem, iAttrs) {
                        scope.transclude = {
                            disableClose: iAttrs.disableClose === 'true',
                        };
                    }
                };
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map