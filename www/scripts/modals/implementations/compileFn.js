/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
exports.Prereq = function (tAttrs, content, container) {
    var contClasses = [];
    if (tAttrs.padding === 'true') {
        content.addClass('modal-padding');
    }
    if (tAttrs.absCenter) {
        contClasses.push('abs-center');
    }
    if (tAttrs.boxClass) {
        contClasses.push(tAttrs.boxClass);
    }
    if (tAttrs.inlineX) {
        contClasses.push('inline-x');
    }
    // Add all potential classes 
    if (contClasses.length) {
        container.addClass(contClasses.join(' '));
    }
};
//# sourceMappingURL=compileFn.js.map