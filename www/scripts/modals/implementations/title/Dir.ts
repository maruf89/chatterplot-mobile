/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {Prereq} from '../compileFn';

require('../../../app').directive('titleModal', [
    'locale', 'DataBus',
    function (locale) {
        return {
            restrict: 'E',
            scope: false,
            replace: true,
            transclude: true,
            templateUrl: 'views/modals/implementations/title/Transclude.html',
            compile: function (tElement, tAttrs) {
                var container = tElement,
                    content = tElement.find('.title-inner');

                Prereq(tAttrs, content, container);

                return {
                    pre: function (scope, iElem, iAttrs) {

                        // template variables
                        scope.transclude = {
                            title: null
                        };

                        locale.ready(locale.getPath(iAttrs.title))
                            .then(function () {
                                scope.transclude.title = locale.getString(iAttrs.title);
                            });
                    },
                };
            }
        };
    }
]);
