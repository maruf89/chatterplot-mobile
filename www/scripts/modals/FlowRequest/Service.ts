/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var commonInteraction = require('common/type/interaction'),
    commonUser = require('common/type/user');

/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
class FlowRequest implements cp.modal.flow.IRequestService {
    private _modalName:string;
    private _shared:any;
    private _sections:string[];
    private _type:string;
    private _reqUserID:number;
    private _editRequest:cp.interaction.IRequest;
    private _onSucessRedirect:string;
    private _templates:{ [key:string]:string } = {
        create: 'views/modals/FlowRequest/Template.html',
        deny: 'views/modals/FlowRequest/DenyTemplate.html',
        view: 'views/modals/FlowRequest/ViewTemplate.html',
    };

    private _onDeactivate:any;

    constructor(
        private _User:cp.user.IService,
        private _DataBus:NodeJS.EventEmitter,
        private _Interaction:cp.interaction.IService,
        private _$q:ng.IQService,
        private _$state:ng.ui.IStateService,
        private _$rootScope:cp.IRootScopeService,
        private _Modal:cp.modal.IService,
        private _$templateCache:ng.ITemplateCacheService,
        private _Venues:cp.venue.IService,
        private _Activities:cp.activities.IService,
        private _Maps:cp.map.IService,
        private _Util:any,
        Config:any
    ) {
        this._onSucessRedirect = Config.routes.myActivities;
    }

    private _initModal(modalName:string, opts?:any) {
        opts = opts || {};

        if (this._modalName) {
            if (this._modalName === modalName) {
                return this._$q.reject();
            }

            this.deactivate();
        }

        opts.template = this._$templateCache.get(this._templates[modalName]);
        opts.container = '.map-pane';

        this._modalName = modalName;

        return this._Modal.activate(opts).finally(this.deactivate.bind(this));
    }

    /**
     * @description Initiates the modal
     *
     * @param {object} opts  will be one of `signup|login`
     */
    public activate(opts:any):ng.IPromise<string> {
        this._shared = opts.data;
        this._sections = opts.sections;
        this._type = opts.type;
        this._reqUserID = opts.reqUserID;

        var opts:any = {
            vars: {
                modal: {
                    sections: this._sections,
                    shared: this._shared,
                    ngIncludify: this._ngIncludify,
                    title: opts.title || 'activities.REQ_' + opts.type.toUpperCase() + '_TITLE',
                    processing: false,
                    withUser: this._User.users[opts.reqUserID],
                }
            }
        };

        return this._initModal('create', opts).then(this._submit.bind(this));
    }

    /**
     * @description /views/modals/FlowRequest/sections/{{::section}}/Template.html'
     * @param path
     * @param section
     * @returns {string}
     */
    private _ngIncludify(path:string, section:string):string {
        return path + section + '/Template.html';
    }

    private _getFullRequest(reqID:string):ng.IPromise<cp.modal.flow.IFullRequestObj> {
        var requestPromise = this._Interaction.requests[reqID] ?
                                 this._$q.when(this._Interaction.requests[reqID]) :
                                 this._Interaction.getType({ objID: reqID, type: 'request' }),

            otherUser:number[] = commonInteraction.getParticipants(reqID, this._User.userID),
            getFields = ['firstName', 'lastName', 'locations', 'userID', 'picture'],
            personPromise = this._User.cacheGet(otherUser, getFields);

        return this._$q.all([
            requestPromise,
            personPromise,
        ]).then(function (response:any[]):cp.modal.flow.IFullRequestObj {
            return {
                request: response[0],
                otherID: otherUser[0],
                users: response[1],
            }
        })
    }

    public edit(reqID:string):ng.IPromise<any> {
        return this._getFullRequest(reqID).then(function (response:cp.modal.flow.IFullRequestObj) {
            var request = response.request,

                // the persons locations
                personLocs:cp.user.ILocation[] = response.users[response.otherID].locations,
                sectionData:any = this._deduceSections(request),
                sharedData:any = sectionData.shared;

            // Only edit if it's editable
            if (request.status !== 'pending' &&
                request.status !== 'accept'
            ) {
                throw false;
            }

            this._editRequest = request;
            sharedData._isModified = reqID;
            sharedData.suggestedVenues = commonUser.combineUserLocations(personLocs, this._User.data.locations);

            return this.activate({
                type: request.type,
                editRequest: request,
                reqUserID: response.otherID,
                sections: sectionData.sections,
                title: 'activities.REQ_EDIT_TITLE',
                data: sharedData,
            })
        }.bind(this));
    }

    private _deduceSections(request:cp.interaction.IFormattedRequest):{ sections:string[]; shared:any; } {
        var sections:string[] = ['venue', 'when', 'message'],
            shared = {};

        _.each(sections, function (section) {
            shared['_' + section] = request[section];
        });

        // Future logic to show different sections will go here

        return {
            sections: sections,
            shared: shared,
        };
    }

    /**
     * @description packages up the request and sends it to the server
     * @param $scope
     */
    private _submit($scope:cp.modal.flow.IServiceScope):ng.IPromise<any> {
        var DataBus = this._DataBus,
            vars = $scope.modal;

        vars.processing = true;
        $scope.$root.safeDigest($scope);

        // Start the progress loader animation
        DataBus.emit('progressLoader', { start: true });

        return this._triggerRequest($scope)
            .then(this._prepareRequest.bind(this))
            .then(this._Interaction.sendRequest.bind(this._Interaction))
            .then(function (request:cp.interaction.IFormattedRequest) {
                this.deactivate(true);
                DataBus.emit('yapServerResponse', {
                    response: 200,
                    type: 'updates.SENT',
                });

                // Send the users to the activities so they know where it lives
                if (this._$state.current.name !== this._onSucessRedirect) {
                    _.defer(function () {
                        this._$state.go(this._onSucessRedirect, {
                            action: this._Util.encodeActionArgs({
                                action: 'view',
                                objID: request.reqID,
                                type: 'request',
                            })
                        });
                    }.bind(this));
                }

                return { completed: true, request: request };
            }.bind(this))
            .catch(function (serverResponse) {
                vars.processing = false;
                DataBus.emit('yapServerResponse', serverResponse);
                throw serverResponse;
            })
            .finally(function () {
                $scope.$root.safeDigest($scope)
                DataBus.emit('progressLoader');
            })
    }

    /**
     * @description calls all of the children preparers
     *
     * @param {object} $scope
     * @returns {Promise}
     * @private
     */
    private _triggerRequest($scope:cp.IScope):ng.IPromise<any> {
        return this._$q.all(
            _.map(this._sections, function (section:string) {
                var deferred = this._$q.defer(),
                    event = $scope.$broadcast(section + 'Prepare', deferred);

                return event.defaultPrevented ? deferred.promise : this._$q.when();
            }, this)
        )
    }

    /**
     * @description Broadcasts a trigger to all child scopes that need to process any data after which it will
     * be available on the shared object under either underscore + name (_field) or just the field name
     *
     * @returns {object}
     * @private
     */
    private _prepareRequest():ng.IPromise<cp.interaction.IRequestBuild> {
        var requestObj:cp.interaction.IRequestBuild = {
                type: this._type,
                creator: this._User.userID,
                participants: [this._User.userID, this._reqUserID],
                status: 'pending',
            },
            shared:any = this._shared;

        _.each(this._sections, function (section:string) {
            var curData:any;

            curData = shared['_' + section] || shared[section];

            if (curData !== void 0) {
                requestObj[section] = curData;
            }
        }, this);

        // Add additional data if this was an edit
        if (this._editRequest) {
            // First check if anything actually changed
            if (!commonInteraction.requestsDiffer(this._editRequest, requestObj)) {
                // if nothing changed -> do nothing!
                return this._$q.reject({
                    response: 400,
                    type: 'activities.REQUEST_NO_CHANGE_MADE',
                });
            }

            requestObj.oldReqID = this._editRequest.reqID;
            requestObj.oldRequest = this._editRequest;
        }

        return this._$q.when(requestObj)
    }

    public deny(datum:{ reqID:string; status:string }):ng.IPromise<any> {
        var STATUS = datum.status.toUpperCase();

        return this._initModal('deny', {
            vars: {
                modal: {
                    title: 'activities.REQ_' + STATUS + '_TITLE',
                    sendText: 'common.DELETE',
                    processing: false,
                    reqObj: {
                        reqID: datum.reqID,
                        status: datum.status,
                        message: null,
                    },
                }
            },
        }).then(this._submitDecline.bind(this));
    }

    private _submitDecline($scope:cp.modal.flow.IServiceScope):ng.IPromise<void> {
        var DataBus = this._DataBus,
            vars = $scope.modal;

        vars.processing = true;
        $scope.$root.safeDigest($scope);

        // Start the progress loader animation
        DataBus.emit('progressLoader', { start: true });

        return this._Interaction.respondRequest(vars.reqObj)
            .then(function (response) {
                this.deactivate(true);
                DataBus.emit('yapServerResponse', {
                    response: 200,
                    type: 'updates.SENT',
                });

                return response
            }.bind(this))
            .catch(function (serverResponse) {
                vars.processing = false;
                DataBus.emit('yapServerResponse', serverResponse);
                throw serverResponse;
            })
            .finally(function () {
                $scope.$root.safeDigest($scope);
                // Start the progress loader animation
                DataBus.emit('progressLoader');
            });
    }

    public view(reqID:string):ng.IPromise<any> {
        var request:cp.interaction.IRequest;

        // get full request object
        return this._getFullRequest(reqID)
            .then(function (response:cp.modal.flow.IFullRequestObj) {
                request = response.request;
                this._User.addPeople(response.users, null, true);
                this._Interaction.formatRequest(request);

                this._focusMarker(request);

                return this._Venues.getCacheFormat(request.venue);
            }.bind(this))

            .then(function () {
                return this._initModal('view', {
                    'class': 'modal',
                    vars: {
                        modal: {
                            Activity: request
                        }
                    }
                });
            }.bind(this));
    }

    private _focusMarker(request:cp.interaction.IFormattedRequest) {
        var markers = this._Activities.map.formatMarker('request', [], null, [], request),
            setName = 'focusRequest',
            instance = this._Maps.getInstance();

        instance.selectSet(setName);
        instance.clearMarkers(setName);
        instance.pushMarkers(markers, setName);
        instance.center();

        this._onDeactivate = function () {
            instance.deselectSet(setName);
            instance.center();
        };
    }

    public deactivate(noReject?:boolean):void {
        this._Modal.deactivate(noReject);

        this._$rootScope.hideKeyboard();

        this._modalName = null;

        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    }
}

FlowRequest.$inject = [
    'User',
    'DataBus',
    'Interaction',
    '$q',
    '$state',
    '$rootScope',
    'ModalService',
    '$templateCache',
    'Venues',
    'Activities',
    'Maps',
    'Util',
    'Config',
];

require('../../app').service('FlowRequest', FlowRequest);
