/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var NAME = 'venue';

/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
class FlowReqVenueCtrl {
    public data:any;
    public shared:any;

    constructor(
        private _$q:ng.IQService,
        $scope:cp.modal.flow.IRequestScope,
        Maps:cp.map.IService,
        Activities:cp.activities.IService
    ) {
        var shared = this.shared = $scope.shared,
            instance:cp.map.IMapCtrl = Maps.getInstance(),
            mapSetName = 'requestCreate',
            data = this.data = {
                venuesArr: [],
                selected: {
                    index: null     // {number} will hold the array index of the currently selected venue
                },
            };

        this._$scope = $scope;
        this._prefill();

        instance.selectSet(mapSetName);

        $scope.$watch('VM.data.selected.index', function (post, prev) {
            if (post && post !== prev) {
                shared[NAME] = data.venuesArr[data.selected.index].venue;
                instance.createMarker(shared[NAME].coords, mapSetName, true);
                instance.center();
            } else {
                instance.clearMarkers(mapSetName);
            }
        });

        $scope.$on('venuePrepare', this._prepare.bind(this));

        $scope.$on('$destroy', function () {
            instance.deselectSet(mapSetName);
            instance.center();
        })
    }

    /**
     * @description if we are editing a request we will have a location selected already
     * @private
     */
    private _prefill():void {
        var selected:cp.interaction.ILocation = this.shared['_' + NAME],
            suggested:cp.interaction.ILocation[] = this.shared.suggestedVenues,
            data:any = this.data,
            $scope:cp.modal.flow.IRequestScope = this._$scope,
            index:number;

        if (!selected) { return; }

        _.each(suggested, function (venue:cp.interaction.ILocation, _index:number):boolean {
            if (selected.vID === venue.vID) {
                index = _index;
                return false;
            }
        });

        // The venue does not belong to one of our locations
        if (typeof index !== 'number') {
            index = suggested.length;
            suggested.push(selected);
        }

        this._onVenueListLoad().then(function ():void {
            data.selected.index = index;
            $scope.$root.safeDigest($scope);
        })
    }

    /**
     * @description a promise returning function that resolves when the venuesSelect
     * module has finished loading the venues. This needs to be resolved before we can
     * select a venue
     *
     * @returns {Promise}
     * @private
     */
    private _onVenueListLoad():ng.IPromise<any> {
        if (this.data.venuesArr.length) {
            return this._$q.when();
        }

        var deferred = this._$q.defer(),
            listener = this._$scope.$watch('VM.data.venuesArr', function (post:cp.venue.IVenueObj[]):void {
                if (post.length) {
                    listener();
                    deferred.resolve();
                }
            }, true);

        return deferred.promise;
    }

    /**
     * @description Called from above. Packages the variables to be sent of
     * @callback
     * @private
     */
    private _prepare() {
        var source = this.shared[NAME],
            venue:cp.interaction.ILocation = {
                coords: source.coords,
            };

        if (source.vID) {
            venue.vID = source.vID;
        }
        if (source.google) {
            venue.google = source.google;
        }
        if (source.yelp) {
            venue.yelp = source.yelp;
        }

        this.shared['_' + NAME] = venue;
    }
}

FlowReqVenueCtrl.$inject = [
    '$q',
    '$scope',
    'Maps',
    'Activities',
];

require('../../../../app.js').controller('FlowReqVenueCtrl', FlowReqVenueCtrl);
