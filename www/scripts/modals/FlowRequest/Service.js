/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var commonInteraction = require('common/type/interaction'), commonUser = require('common/type/user');
/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
var FlowRequest = (function () {
    function FlowRequest(_User, _DataBus, _Interaction, _$q, _$state, _$rootScope, _Modal, _$templateCache, _Venues, _Activities, _Maps, _Util, Config) {
        this._User = _User;
        this._DataBus = _DataBus;
        this._Interaction = _Interaction;
        this._$q = _$q;
        this._$state = _$state;
        this._$rootScope = _$rootScope;
        this._Modal = _Modal;
        this._$templateCache = _$templateCache;
        this._Venues = _Venues;
        this._Activities = _Activities;
        this._Maps = _Maps;
        this._Util = _Util;
        this._templates = {
            create: 'views/modals/FlowRequest/Template.html',
            deny: 'views/modals/FlowRequest/DenyTemplate.html',
            view: 'views/modals/FlowRequest/ViewTemplate.html',
        };
        this._onSucessRedirect = Config.routes.myActivities;
    }
    FlowRequest.prototype._initModal = function (modalName, opts) {
        opts = opts || {};
        if (this._modalName) {
            if (this._modalName === modalName) {
                return this._$q.reject();
            }
            this.deactivate();
        }
        opts.template = this._$templateCache.get(this._templates[modalName]);
        opts.container = '.map-pane';
        this._modalName = modalName;
        return this._Modal.activate(opts).finally(this.deactivate.bind(this));
    };
    /**
     * @description Initiates the modal
     *
     * @param {object} opts  will be one of `signup|login`
     */
    FlowRequest.prototype.activate = function (opts) {
        this._shared = opts.data;
        this._sections = opts.sections;
        this._type = opts.type;
        this._reqUserID = opts.reqUserID;
        var opts = {
            vars: {
                modal: {
                    sections: this._sections,
                    shared: this._shared,
                    ngIncludify: this._ngIncludify,
                    title: opts.title || 'activities.REQ_' + opts.type.toUpperCase() + '_TITLE',
                    processing: false,
                    withUser: this._User.users[opts.reqUserID],
                }
            }
        };
        return this._initModal('create', opts).then(this._submit.bind(this));
    };
    /**
     * @description /views/modals/FlowRequest/sections/{{::section}}/Template.html'
     * @param path
     * @param section
     * @returns {string}
     */
    FlowRequest.prototype._ngIncludify = function (path, section) {
        return path + section + '/Template.html';
    };
    FlowRequest.prototype._getFullRequest = function (reqID) {
        var requestPromise = this._Interaction.requests[reqID] ?
            this._$q.when(this._Interaction.requests[reqID]) :
            this._Interaction.getType({ objID: reqID, type: 'request' }), otherUser = commonInteraction.getParticipants(reqID, this._User.userID), getFields = ['firstName', 'lastName', 'locations', 'userID', 'picture'], personPromise = this._User.cacheGet(otherUser, getFields);
        return this._$q.all([
            requestPromise,
            personPromise,
        ]).then(function (response) {
            return {
                request: response[0],
                otherID: otherUser[0],
                users: response[1],
            };
        });
    };
    FlowRequest.prototype.edit = function (reqID) {
        return this._getFullRequest(reqID).then(function (response) {
            var request = response.request, 
            // the persons locations
            personLocs = response.users[response.otherID].locations, sectionData = this._deduceSections(request), sharedData = sectionData.shared;
            // Only edit if it's editable
            if (request.status !== 'pending' &&
                request.status !== 'accept') {
                throw false;
            }
            this._editRequest = request;
            sharedData._isModified = reqID;
            sharedData.suggestedVenues = commonUser.combineUserLocations(personLocs, this._User.data.locations);
            return this.activate({
                type: request.type,
                editRequest: request,
                reqUserID: response.otherID,
                sections: sectionData.sections,
                title: 'activities.REQ_EDIT_TITLE',
                data: sharedData,
            });
        }.bind(this));
    };
    FlowRequest.prototype._deduceSections = function (request) {
        var sections = ['venue', 'when', 'message'], shared = {};
        _.each(sections, function (section) {
            shared['_' + section] = request[section];
        });
        // Future logic to show different sections will go here
        return {
            sections: sections,
            shared: shared,
        };
    };
    /**
     * @description packages up the request and sends it to the server
     * @param $scope
     */
    FlowRequest.prototype._submit = function ($scope) {
        var DataBus = this._DataBus, vars = $scope.modal;
        vars.processing = true;
        $scope.$root.safeDigest($scope);
        // Start the progress loader animation
        DataBus.emit('progressLoader', { start: true });
        return this._triggerRequest($scope)
            .then(this._prepareRequest.bind(this))
            .then(this._Interaction.sendRequest.bind(this._Interaction))
            .then(function (request) {
            this.deactivate(true);
            DataBus.emit('yapServerResponse', {
                response: 200,
                type: 'updates.SENT',
            });
            // Send the users to the activities so they know where it lives
            if (this._$state.current.name !== this._onSucessRedirect) {
                _.defer(function () {
                    this._$state.go(this._onSucessRedirect, {
                        action: this._Util.encodeActionArgs({
                            action: 'view',
                            objID: request.reqID,
                            type: 'request',
                        })
                    });
                }.bind(this));
            }
            return { completed: true, request: request };
        }.bind(this))
            .catch(function (serverResponse) {
            vars.processing = false;
            DataBus.emit('yapServerResponse', serverResponse);
            throw serverResponse;
        })
            .finally(function () {
            $scope.$root.safeDigest($scope);
            DataBus.emit('progressLoader');
        });
    };
    /**
     * @description calls all of the children preparers
     *
     * @param {object} $scope
     * @returns {Promise}
     * @private
     */
    FlowRequest.prototype._triggerRequest = function ($scope) {
        return this._$q.all(_.map(this._sections, function (section) {
            var deferred = this._$q.defer(), event = $scope.$broadcast(section + 'Prepare', deferred);
            return event.defaultPrevented ? deferred.promise : this._$q.when();
        }, this));
    };
    /**
     * @description Broadcasts a trigger to all child scopes that need to process any data after which it will
     * be available on the shared object under either underscore + name (_field) or just the field name
     *
     * @returns {object}
     * @private
     */
    FlowRequest.prototype._prepareRequest = function () {
        var requestObj = {
            type: this._type,
            creator: this._User.userID,
            participants: [this._User.userID, this._reqUserID],
            status: 'pending',
        }, shared = this._shared;
        _.each(this._sections, function (section) {
            var curData;
            curData = shared['_' + section] || shared[section];
            if (curData !== void 0) {
                requestObj[section] = curData;
            }
        }, this);
        // Add additional data if this was an edit
        if (this._editRequest) {
            // First check if anything actually changed
            if (!commonInteraction.requestsDiffer(this._editRequest, requestObj)) {
                // if nothing changed -> do nothing!
                return this._$q.reject({
                    response: 400,
                    type: 'activities.REQUEST_NO_CHANGE_MADE',
                });
            }
            requestObj.oldReqID = this._editRequest.reqID;
            requestObj.oldRequest = this._editRequest;
        }
        return this._$q.when(requestObj);
    };
    FlowRequest.prototype.deny = function (datum) {
        var STATUS = datum.status.toUpperCase();
        return this._initModal('deny', {
            vars: {
                modal: {
                    title: 'activities.REQ_' + STATUS + '_TITLE',
                    sendText: 'common.DELETE',
                    processing: false,
                    reqObj: {
                        reqID: datum.reqID,
                        status: datum.status,
                        message: null,
                    },
                }
            },
        }).then(this._submitDecline.bind(this));
    };
    FlowRequest.prototype._submitDecline = function ($scope) {
        var DataBus = this._DataBus, vars = $scope.modal;
        vars.processing = true;
        $scope.$root.safeDigest($scope);
        // Start the progress loader animation
        DataBus.emit('progressLoader', { start: true });
        return this._Interaction.respondRequest(vars.reqObj)
            .then(function (response) {
            this.deactivate(true);
            DataBus.emit('yapServerResponse', {
                response: 200,
                type: 'updates.SENT',
            });
            return response;
        }.bind(this))
            .catch(function (serverResponse) {
            vars.processing = false;
            DataBus.emit('yapServerResponse', serverResponse);
            throw serverResponse;
        })
            .finally(function () {
            $scope.$root.safeDigest($scope);
            // Start the progress loader animation
            DataBus.emit('progressLoader');
        });
    };
    FlowRequest.prototype.view = function (reqID) {
        var request;
        // get full request object
        return this._getFullRequest(reqID)
            .then(function (response) {
            request = response.request;
            this._User.addPeople(response.users, null, true);
            this._Interaction.formatRequest(request);
            this._focusMarker(request);
            return this._Venues.getCacheFormat(request.venue);
        }.bind(this))
            .then(function () {
            return this._initModal('view', {
                'class': 'modal',
                vars: {
                    modal: {
                        Activity: request
                    }
                }
            });
        }.bind(this));
    };
    FlowRequest.prototype._focusMarker = function (request) {
        var markers = this._Activities.map.formatMarker('request', [], null, [], request), setName = 'focusRequest', instance = this._Maps.getInstance();
        instance.selectSet(setName);
        instance.clearMarkers(setName);
        instance.pushMarkers(markers, setName);
        instance.center();
        this._onDeactivate = function () {
            instance.deselectSet(setName);
            instance.center();
        };
    };
    FlowRequest.prototype.deactivate = function (noReject) {
        this._Modal.deactivate(noReject);
        this._$rootScope.hideKeyboard();
        this._modalName = null;
        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    };
    return FlowRequest;
})();
FlowRequest.$inject = [
    'User',
    'DataBus',
    'Interaction',
    '$q',
    '$state',
    '$rootScope',
    'ModalService',
    '$templateCache',
    'Venues',
    'Activities',
    'Maps',
    'Util',
    'Config',
];
require('../../app').service('FlowRequest', FlowRequest);
//# sourceMappingURL=Service.js.map