/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').factory('translatorModal', [
    'btfModal', '$templateCache',
    function (Modal, $templateCache) {
        return Modal({
            template: $templateCache.get('views/modals/standalone/translator/Template.html')
        });
    }
]);
//# sourceMappingURL=Factory.js.map