/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var FeedbackCtrl, app, categories, extend = function (child, parent) {
    for (var key in parent) {
        if (hasProp.call(parent, key))
            child[key] = parent[key];
    }
    function ctor() {
        this.constructor = child;
    }
    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
}, hasProp = {}.hasOwnProperty;
(app = require('../../../app.js')).factory('feedbackModal', [
    'btfModal', '$templateCache',
    function (Modal, $templateCache) {
        return Modal({
            template: $templateCache.get('views/modals/standalone/feedback/Template.html')
        });
    }
]);
categories = require('./categories.json');
FeedbackCtrl = (function (superClass) {
    extend(FeedbackCtrl, superClass);
    function FeedbackCtrl($scope, SocketIo, User, DataBus) {
        this.$scope = $scope;
        this.SocketIo = SocketIo;
        this.User = User;
        this.DataBus = DataBus;
        this.$scope.categoryOptions = categories;
        FeedbackCtrl.__super__.constructor.apply(this, arguments);
        this.$scope.message.type = 'feedback';
        return this;
    }
    return FeedbackCtrl;
})(require('../standaloneModal.js'));
FeedbackCtrl.$inject = ['$scope', 'SocketIo', 'User', 'DataBus'];
app.controller('FeedbackCtrl', FeedbackCtrl);
//# sourceMappingURL=Ctrl.js.map