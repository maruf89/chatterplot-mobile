/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var AbuseCtrl, app, categories,
    extend = function (child, parent) {
        for (var key in parent) {
            if (hasProp.call(parent, key)) child[key] = parent[key];
        }

        function ctor() {
            this.constructor = child;
        }
        ctor.prototype = parent.prototype;
        child.prototype = new ctor();
        child.__super__ = parent.prototype;
        return child;
    },
    hasProp = {}.hasOwnProperty;

(app = require('../../../app'))
.factory('abuseModal', [
    'btfModal',
    function (Modal) {
        return Modal({
            templateUrl: 'views/modals/standalone/abuse/Template.html'
        });
    }
]);

categories = require('./categories.json');

AbuseCtrl = (function (superClass) {
    extend(AbuseCtrl, superClass);

    function AbuseCtrl($scope, SocketIo, User, DataBus) {
        this.$scope = $scope;
        this.SocketIo = SocketIo;
        this.User = User;
        this.DataBus = DataBus;
        this.$scope.categoryOptions = categories;
        AbuseCtrl.__super__.constructor.apply(this, arguments);
        this.$scope.message.type = 'abuse';
        return this;
    }

    return AbuseCtrl;

})(require('../standaloneModal'));

AbuseCtrl.$inject = ['$scope', 'SocketIo', 'User', 'DataBus'];

app.controller('AbuseCtrl', AbuseCtrl);
