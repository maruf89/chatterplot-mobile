/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app').factory('ModalFactory', [function () {
    return (function () {
        function Factory() {}

        Factory.prototype.load = function () {};

        return Factory;
    })();
}]);
