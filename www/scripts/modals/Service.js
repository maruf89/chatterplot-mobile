/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * This service is used to open the Signup / Login modals
 * as well as any other related modals including 'password-reset' & 'email-signup'
 */
var ModalService = (function () {
    function ModalService(_$ionicModal, _$q, _$templateCache, _$rootScope) {
        this._$ionicModal = _$ionicModal;
        this._$q = _$q;
        this._$templateCache = _$templateCache;
        this._$rootScope = _$rootScope;
        // close modal on state change
        _$rootScope.$on('$stateChangeSuccess', this._closeUnpersist.bind(this));
        this._template = _$templateCache.get('views/modals/Template.html');
    }
    ModalService.prototype._initModal = function (opts) {
        var context = this, method = opts.templateUrl ? 'fromTemplateUrl' : 'fromTemplate';
        return this._$q.when(this._$ionicModal[method](opts.template || opts.templateUrl, opts)).then(function (modal) {
            return context._modal = modal;
        });
    };
    /**
     * @description Initiates the modal
     *
     * @param {object} opts
     * @param {string} opts.template - class to apply to modal box
     * @param {string=} opts.class - class to apply to root template
     * @param {object=} opts.vars
     */
    ModalService.prototype.activate = function (opts) {
        this._deferred = this._$q.defer();
        this._modal && this._modal.isShown() && this.deactivate();
        var deactivate = this.deactivate.bind(this), vars = opts.vars || {}, onSubmit = this._onSubmit.bind(this, vars), context = this;
        opts.scope = this._$rootScope.$new(true);
        this._pagePersist = opts.pagePersist;
        _.extend(opts.scope, {
            window: {
                deactivate: deactivate,
                onSubmit: onSubmit,
                'class': opts.class || '',
            }
        }, vars);
        this._initModal(opts).then(function (modal) {
            modal.show();
            context.modalScope = modal.scope;
            context.activeName = opts.name;
        });
        return this._deferred.promise;
    };
    ModalService.prototype.isActive = function (name) {
        return this.activeName && name === this.activeName;
    };
    ModalService.prototype._onSubmit = function (vars, what) {
        this._deferred && this._deferred.resolve(what || vars);
        this._deferred = null;
    };
    ModalService.prototype._closeUnpersist = function () {
        !this._pagePersist && this.deactivate();
    };
    ModalService.prototype.deactivateName = function (name) {
        if (this.activeName === name) {
            this.deactivate();
        }
    };
    ModalService.prototype.show = function () {
        return this._modal.show();
    };
    ModalService.prototype.hide = function () {
        return this._modal.hide();
    };
    ModalService.prototype.deactivate = function (noReject) {
        if (!this._modal || !this._modal.isShown()) {
            return;
        }
        this._modal.remove();
        this._modal = null;
        this._pagePersist = null;
        this.activeName = null;
        this.modalScope = null;
        if (!noReject) {
            this._deferred && this._deferred.reject();
            this._deferred = null;
        }
        if (this._onDeactivate) {
            this._onDeactivate();
            this._onDeactivate = null;
        }
    };
    return ModalService;
})();
ModalService.$inject = [
    '$ionicModal',
    '$q',
    '$templateCache',
    '$rootScope',
    '$state',
];
require('../app').service('ModalService', ModalService);
//# sourceMappingURL=Service.js.map