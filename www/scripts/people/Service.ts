/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

class People implements cp.user.ITandemsService {
    constructor(
        private _$q:ng.IQService,
        private _SocketIo:any,
        private _Util:any,
        private _User:cp.user.IService,
        Helper,
        TandemMap
    ) {
        this.helper = Helper.get(this);
        this.map = new TandemMap(this);
    }

    fetch(opts) {
        return this._SocketIo.onEmitSock('/user/fetch', opts, true);
    }

    format(tandems) {
        var noPhoto = [],
            userID = this._User.userID;

        if (!_.isArray(tandems)) {
            tandems = [tandems];
        }

        return _.reduce(tandems, function (all, tandem:any) {
                var which;
                tandem = tandem._source || tandem;
                if (tandem.userID === userID) {
                    return all;
                }
                tandem.fullName = this._User.format.name(tandem);
                which = tandem.picture ? all : noPhoto;
                which.push(tandem);
                return all;
            }, [], this)
            .concat(noPhoto);
    }

    get(data) {
        return this._User.get(data);
    }
}

People.$inject = [
    '$q',
    'SocketIo',
    'Util',
    'User',
    'PeopleHelper',
    'PeopleMap',
];

require('../app.js').service('People', People);
