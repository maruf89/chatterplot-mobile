/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var app = require('../app'),
    Helper = require('root/people/Helper');

app.factory('PeopleHelper', Helper.Helper);

app.factory('PeopleMap', [
    'Util',
    Helper.Map,
]);
