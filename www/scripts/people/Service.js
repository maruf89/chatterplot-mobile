/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var People = (function () {
    function People(_$q, _SocketIo, _Util, _User, Helper, TandemMap) {
        this._$q = _$q;
        this._SocketIo = _SocketIo;
        this._Util = _Util;
        this._User = _User;
        this.helper = Helper.get(this);
        this.map = new TandemMap(this);
    }
    People.prototype.fetch = function (opts) {
        return this._SocketIo.onEmitSock('/user/fetch', opts, true);
    };
    People.prototype.format = function (tandems) {
        var noPhoto = [], userID = this._User.userID;
        if (!_.isArray(tandems)) {
            tandems = [tandems];
        }
        return _.reduce(tandems, function (all, tandem) {
            var which;
            tandem = tandem._source || tandem;
            if (tandem.userID === userID) {
                return all;
            }
            tandem.fullName = this._User.format.name(tandem);
            which = tandem.picture ? all : noPhoto;
            which.push(tandem);
            return all;
        }, [], this)
            .concat(noPhoto);
    };
    People.prototype.get = function (data) {
        return this._User.get(data);
    };
    return People;
})();
People.$inject = [
    '$q',
    'SocketIo',
    'Util',
    'User',
    'PeopleHelper',
    'PeopleMap',
];
require('../app.js').service('People', People);
//# sourceMappingURL=Service.js.map