/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var MODULE_TYPE = 'people', MODULE_NAME = MODULE_TYPE + 'List', // peopleList Directive
_defaultSearch = { size: 15 };
var AbstractListDir_1 = require('root/search/AbstractListDir');
/**
 * @ngdoc directive
 * @module peopleListDirective
 *
 * @requires DataBus
 * @requires $compile
 * @requires $analytics
 * @requires $q
 * @requires Util
 * @requires $templateCache
 */
require('../../../app').directive(MODULE_NAME, [
    'DataBus', '$compile', '$analytics', '$q', 'Util', '$templateCache', '$ionicScrollDelegate',
    function (DataBus, $compile, $analytics, $q, Util, $templateCache, $ionicScrollDelegate) {
        var listDirective = new AbstractListDir_1.AbstractDirective({
            moduleName: MODULE_NAME,
            moduleType: MODULE_TYPE,
            itemKeyID: 'userID',
        }, _defaultSearch, DataBus, $analytics, $compile, $q, Util, $ionicScrollDelegate);
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                options: '=?',
                onRefresh: '=?',
                zeroDataDirective: '@',
                stack: '=?',
                group: '=?',
                subType: '@',
            },
            template: $templateCache.get('views/people/directives/list/Template.html'),
            controller: 'PeopleListCtrl',
            link: {
                pre: listDirective.pre.bind(listDirective),
                post: listDirective.postCallback(function (scope, iElem) {
                    iElem.on('click', '.message', function (event) {
                        scope = angular.element(event.target).scope();
                        DataBus.emit('/message/user', {
                            to: [scope.Activity]
                        });
                    });
                })
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map