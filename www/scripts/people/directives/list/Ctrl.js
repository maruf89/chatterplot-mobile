/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
var ListCtrl_1 = require('root/people/directives/ListCtrl');
require('../../../app').controller('PeopleListCtrl', [
    '$q',
    '$scope',
    'Activities',
    'DataBus',
    'Util',
    'User',
    'Maps',
    'localStorageService',
    'AdSlot',
    'People',
    ListCtrl_1.PeopleListCtrl,
]);
//# sourceMappingURL=Ctrl.js.map