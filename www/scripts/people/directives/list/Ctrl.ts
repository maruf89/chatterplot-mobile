/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

import {PeopleListCtrl} from 'root/people/directives/ListCtrl';

require('../../../app').controller('PeopleListCtrl', [
    '$q',
    '$scope',
    'Activities',
    'DataBus',
    'Util',
    'User',
    'Maps',
    'localStorageService',
    'AdSlot',
    'People',
    PeopleListCtrl,
]);
