/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import App = require('../app');

var userPermissions = null,
    sitePermissions = null,
    _ready = false,
    _pending = [],

    /**
     * The function that checks whether a user is capable of said action
     *
     * @private
     * @param  {string} check  action
     * @param  {number} role   permissions integer
     * @return {number}        An integer > 0 if the user is capable
     */

    _checkPermission = function (check, role) {
        return sitePermissions[check] & role;
    },

    Permissions = function ($q) {
        this._$q = $q;

        sitePermissions = window.CP.Settings.permissions;

        if (Object.freeze) {
            sitePermissions = Object.freeze(sitePermissions);
        }
    };




Permissions.prototype = _.extend(Permissions.prototype, {
    /**
     * Tests user's permissions to see if they're capable of the action
     *
     * @param  {array} capability  Array of strings representing actions
     * @param  {number} user       User's permissions
     * @return {Boolean}           Whether the user can
     */
    can: function (capability, user) {
        if (user == null) {
            user = userPermissions;
        }
        if (typeof user !== 'number') {
            throw new Error(
                'Undefined user permissions. Either define the user permissions via #defineUserPermissions or add as second parameter'
            );
        }
        if (typeof capability === 'string') {
            if (!sitePermissions[capability]) {
                throw new Error("Invalid capability: " + capability);
            }
            return _checkPermission(capability, user);
        }
        return _.every(capability, function (cap) {
            return _checkPermission(cap, user);
        });
    },

    /**
     * Same as the above but called as a promise.
     * Can be called before authentication and will store the pending requests until
     * the user has been authenticated and has something to work with
     *
     * @param  {array} capability  Array of strings representing actions
     * @return {Promise}           Result of promise contains a {Boolean}
     */
    canPromise: function (capabilities) {
        var deferred, method;

        if (_ready) {
            method = this.can(capabilities) ? 'when' : 'reject';
            return this._$q[method]();
        } else {
            deferred = this._$q.defer();
            _pending.push({
                capabilities: capabilities,
                deferred: deferred
            });

            return deferred.promise;
        }
    },

    /**
     * Caches the user's permissions
     *
     * @param  {number} permissions
     */
    defineUserPermissions: function (permissions) {
        userPermissions = +permissions;
        _ready = true;
        return this._runPending();
    },

    /**
     * Runs an pending calls
     */
    _runPending: function () {
        return _.each(_pending, function (run) {
            var method = this.can(run.capabilities) ? 'resolve' :
                'reject';
            return run.deferred[method]();
        }, this);
    }
});

Permissions.$inject = ['$q'];

App.service('Permissions', Permissions);
