/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';
var FB, SERVICE_NAME, oauthMethods, self;

self = null;

SERVICE_NAME = 'facebook';

FB = function (Facebook, $http, $window, $q, locale, DataBus, SocketIo, Auth,
    ipCookie) {
    self = this;
    this._Facebook = Facebook;
    this._$http = $http;
    this._$window = $window;
    this._$q = $q;
    this._locale = locale;
    this._SocketIo = SocketIo;
    this._Auth = Auth;
    this._ipCookie = ipCookie;
    this.statusWaiting = [];

    this._loadDefer = [];
    var onLoaded = this._onLoaded.bind(this);

    window.fbAsyncInit = function () {
        if (!window.FB || !window.FB.init) {
            return setTimeout(window.fbAsyncInit, 50);
        }
        window.FB.init({
            appId: '407597439382292',
            version: 'v2.4',
            status: true,
            cookie: true,
            xfbml: true
        });

        window.FB.Event.subscribe('auth.statusChange', function (response) {
            var fn, results;
            self._Facebook.setState(response);
            results = [];
            while (self.statusWaiting[0]) {
                fn = self.statusWaiting.shift();
                results.push(fn());
            }
            return results;
        });

        _.defer(onLoaded);
    };

    // Facebook init script 
    (function (d, s, id) {
        var fjs, js;
        fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/" + (self._locale.getLocale()
            .replace('-', '_')) + "/sdk.js";
        return fjs.parentNode.insertBefore(js, fjs);
    })(document, 'script', 'facebook-jssdk');
    DataBus.on('/site/unauthenticated/init', this.checkIfLoggedIn.bind(this));
    return this;
};

oauthMethods = {
    connect: 'connectURL',
    verify: 'verifyURL'
};

FB.prototype = _.extend(FB.prototype, {
    _onLoaded: function () {
        if (this._loadDefer.length) {
            _.each(this._loadDefer, function (deferred) {
                deferred.resolve();
            });
        }

        this._loadDefer = null;
        this.onLoaded = this._$q.when;
    },

    onLoaded: function () {
        var deferred = this._$q.defer();

        this._loadDefer.push(deferred);

        return deferred.promise;
    },

    checkIfLoggedIn: function () {

        // If we logged out then we do not want to force login again 
        var state;
        if (this._ipCookie('disableSocialConnect')) {
            return false;
        }
        if (!window.FB || !window.FB.init) {
            return setTimeout(this.checkIfLoggedIn.bind(this), 50);
        }
        self = this;
        if (!(state = this._Facebook.state)) {
            return this.statusWaiting.push(this.checkIfLoggedIn.bind(
                this));
        }
        if (state.status !== 'connected') {
            return false;
        }
        return self._SocketIo.onEmitSock('/auth/serviceToken', {
                service: SERVICE_NAME,
                data: state.authResponse
            })
            .then(function (response) {

                // Lastly try to authenticate with the token if we got one 
                if (response.token) {
                    self._Auth.setOnAuthHook($.noop, true);
                    return self._Auth.authenticateWithToken(response);
                }
            });
    },
    share: function (vars) {
        var height, leftPosition, topPosition, width, windowFeatures;
        width = 550;
        height = 500;
        leftPosition = (this._$window.innerWidth / 2) - ((width / 2) +
            10);
        topPosition = (this._$window.innerHeight / 2) - ((height / 2) +
            50);
        windowFeatures = 'status=no,height=' + height + ',width=' +
            width + ',resizable=yes,left=' + leftPosition + ',top=' +
            topPosition + ',screenX=' + leftPosition + ',screenY=' +
            topPosition +
            ',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no';
        return window.open('https://www.facebook.com/sharer.php?u=' +
            encodeURIComponent(vars.url) + '&t=' +
            encodeURIComponent(vars.text), 'sharer', windowFeatures);
    },

    getLoginStatus: function () {
        return this._Facebook.getLoginStatus();
    },

    login: function () {
        return this._Facebook.login();
    },

    logout: function () {
        this._Facebook.logout();
        return this._$http.post('/logout/facebook');
    },

    unsubscribe: function () {
        return this._Facebook.unsubscribe();
    },

    getInfo: function (callback) {
        return window.FB.api("/" + this.id, callback);
    },

    query: function (method, queryObj, callback) {
        return window.FB.api({
            method: method,
            queries: queryObj
        }, callback);
    },

    connect: function () {
        return this._oauthCall('connect');
    },

    verify: function () {
        return this._oauthCall('verify');
    },

    _oauthCall: function (method) {
        var deferred = this._$q.defer(),
            callback = function (err, service) {
                if (err) {
                    return deferred.reject(service);
                }
                return deferred.resolve(service);
            },
            oauthMethod = oauthMethods[method],
            oauthURL = CP.Settings.facebook[oauthMethod]
                .replace('redirect_uri=.', 'state=' + CP.site.prefix + '&redirect_uri=https://' + CP.site.prefix + '.');

        window.onFacebookAuth = callback;
        this._$window.open(oauthURL, 'AuthenticateFacebook', 'width=600, height=600');

        return deferred.promise;
    }
});

FB.$inject = [
    'Facebook',
    '$http',
    '$window',
    '$q',
    'locale',
    'DataBus',
    'SocketIo',
    'Auth',
    'ipCookie'
];

require('../app.js').service('FB', FB);
