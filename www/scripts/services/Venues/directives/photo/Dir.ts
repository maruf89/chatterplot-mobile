/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../../app').directive('venuePhoto', [
    'Venues',
    function (Venues) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                venue:      '=?',   // {object} a full google venue object
                width:      '@',    // {number}
                height:     '@',    // {number=} if not set will use width
            },
            template: '<div class="v-photo"><span class="img" /></div>',
            link: {
                pre: function (scope, iElem) {
                    scope.height = scope.height || scope.width;

                    Venues.getCacheFormat(scope.venue, true)
                        .then(function (venueObj:cp.venue.IVenueObj) {
                            var photoSrc = Venues.getPhotoSrc(venueObj.service, scope.height, scope.width);
                            iElem.find('.img').css('background-image', 'url(' + photoSrc + ')');
                        });
                }
            }
        };
    }
]);
