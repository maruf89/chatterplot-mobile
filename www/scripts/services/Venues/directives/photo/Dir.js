/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app').directive('venuePhoto', [
    'Venues',
    function (Venues) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                venue: '=?',
                width: '@',
                height: '@',
            },
            template: '<div class="v-photo"><span class="img" /></div>',
            link: {
                pre: function (scope, iElem) {
                    scope.height = scope.height || scope.width;
                    Venues.getCacheFormat(scope.venue, true)
                        .then(function (venueObj) {
                        var photoSrc = Venues.getPhotoSrc(venueObj.service, scope.height, scope.width);
                        iElem.find('.img').css('background-image', 'url(' + photoSrc + ')');
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map