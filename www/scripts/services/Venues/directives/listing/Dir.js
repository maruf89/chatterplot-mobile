/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var photoSize = 125, 
// Sometimes google returns a photo and it's not a square it will pixelate
// the photo so we request a bigger image in case a dimension is too small
photoReqSize = Math.round(photoSize * 1.5 * window.devicePixelRatio);
require('../../../../app.js').directive('venuesListing', [
    'Venues', '$q', '$templateCache', 'Maps',
    function (Venues, $q, $templateCache, Maps) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                venue: '=?',
                remove: '&',
                venueSrc: '=?',
                noMap: '@',
                watchVar: '=?',
            },
            template: $templateCache.get('views/services/Venues/directives/listing/Template.html'),
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var photo, vars = {
                        isHome: true,
                        isLoaded: false,
                        showInfo: false,
                        gmapUrl: null,
                        showRemove: !!iAttrs.remove,
                        venue: null,
                    }, setPhoto = function (venue) {
                        var photoSrc = Venues.getPhotoSrc(venue, photoReqSize, photoReqSize);
                        iElem.find('.img').css('background-image', 'url(' + photoSrc + ')');
                    }, getVenue = function () {
                        var promise = scope.venueSrc ? $q.when(scope.venueSrc) : Venues.getPlace(scope.venue);
                        // Fetch the location from Google
                        promise.then(function (venue) {
                            vars.venue = venue;
                            vars.isHome = venue.types && venue.types.indexOf('establishment') === -1;
                            vars.isLoaded = true;
                            var name = vars.isHome ? '' : venue.name;
                            vars.gmapUrl = Venues.addressStringToUrl('google', name, venue.formatted_address);
                            setPhoto(venue);
                            scope.$root.safeDigest(scope);
                        });
                    };
                    scope.vars = vars;
                    getVenue();
                    if (iAttrs.watchVar) {
                        scope.$watch('watchVar', function (post, prev) {
                            if (post && post !== prev) {
                                getVenue();
                            }
                        });
                    }
                },
                post: function (scope, iElem) {
                    var vars = scope.vars, info, hideCheck = function (event, force) {
                        info = info || iElem.find('.more-info')[0];
                        if (force === true ||
                            (vars.showInfo &&
                                info !== event.target &&
                                !info.contains(event.target))) {
                            vars.showInfo = !vars.showInfo;
                            scope.$root.safeDigest(scope);
                        }
                        return true;
                    };
                    if (!scope.noMap) {
                        iElem.on('click', '.loc-center', function () {
                            var instance = Maps.getInstance(), root;
                            if (instance && (root = instance.rootObject())) {
                                root.setCenter(scope.vars.venue.geometry.location);
                                root.setZoom(15);
                            }
                        });
                    }
                    CP.Cache.$document.on('click', hideCheck);
                    iElem.on('click', '.more-info', function () {
                        vars.showInfo = !vars.showInfo;
                        scope.$root.safeDigest(scope);
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map