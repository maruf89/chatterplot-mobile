/// <reference path="../../../../typings/libraries/browserify.d.ts" />
/// <reference path="../../../../typings/libraries/lodash.d.ts" />
'use strict';
var features = require('./features.json');
require('../../app.js').factory('NewFeature', [
    'User', 'SocketIo', 'btfModal', '$templateCache',
    function (User, SocketIo, btfModal, $templateCache) {
        /**
         * @ngdoc factory
         * @class
         * @classdesc Facilitates showing feature updates to the user
         */
        var NewFeature = function (featureName, data) {
            this.featureName = featureName;
            this.options = features[featureName];
            this.data = data || {};
        };
        NewFeature.prototype = _.extend(NewFeature.prototype, {
            /**
             * Checks whether a feature is applicable to a user
             * @param {boolean} openIfFound - if true and applicable, will activate the feature
             * @returns {boolean}
             */
            check: function (openIfFound) {
                // If not logged in, exit
                if (!User.userID) {
                    return false;
                }
                var features = User.data.features;
                // If not applicable -> quit
                if (!_.isArray(features) || features.indexOf(this.featureName) === -1) {
                    return false;
                }
                // applicable
                if (openIfFound) {
                    this.activate();
                }
                return true;
            },
            /**
             * @description Builds the modal if it doesn't exist yet
             * @returns {btfModal}
             * @private
             */
            _getModal: function () {
                if (this.modal) {
                    return this.modal;
                }
                var options = {
                    controller: this.options.controller
                };
                if (this.options.templateUrl) {
                    options.templateUrl = this.options.templateUrl;
                }
                else if (this.options.templateCache) {
                    options.template = $templateCache.get(this.options.templateCache);
                }
                else {
                    options.template = this.options.template;
                }
                return this.modal = btfModal(options);
            },
            /**
             * @description Activates the feature
             */
            activate: function () {
                var modal = this._getModal();
                if (modal.active()) {
                    return false;
                }
                modal.activate({
                    modal: {
                        deactivate: this.deactivate.bind(this, this.data.onCancel, true)
                    },
                    vars: this.data.vars,
                    onSuccess: this.deactivate.bind(this, this.data.onSuccess, true)
                });
            },
            /**
             * @description deactivates the featured modal & removes the feature
             * @param {function=} callback - if passed will be called
             * @param {boolean=} remove - whether to remove feature from the user in the DB
             */
            deactivate: function (callback, remove) {
                if (typeof callback === 'function') {
                    callback();
                }
                this.modal.active() && this.modal.deactivate();
                remove && this._removeFeature();
            },
            /**
             * @descriptions makes an update request to the server to remove this feature
             * @private
             */
            _removeFeature: function () {
                SocketIo.onEmitSock('/user/feature/seen', this.featureName).catch(function (err) {
                    console.log(err);
                    throw new Error('Error updating a feature as seen');
                })
                    .then(function () {
                    // on success remove the feature locally
                    var index = User.data.features.indexOf(this.featureName);
                    if (index !== -1) {
                        User.data.features.splice(index, 1);
                    }
                }.bind(this));
            }
        });
        return NewFeature;
    }
]);
//# sourceMappingURL=Service.js.map