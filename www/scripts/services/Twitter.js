/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
var App = require('../app');
'use strict';
var Twitter, baseUrl, self = null;
baseUrl = 'https://twitter.com/intent/tweet?via=Chatterplot&url=';
Twitter = function () {
    self = this;
    _.each(arguments, function (val, index) {
        self[Twitter.$inject[index]] = val;
    });
    (function (d, s, id) {
        var fjs, js, p;
        js = void 0;
        fjs = d.getElementsByTagName(s)[0];
        p = (/^http:/.test(d.location) ? 'http' : 'https');
        if (!d.getElementById(id)) {
            js = d.createElement(s);
            js.id = id;
            js.src = p + '://platform.twitter.com/widgets.js';
            fjs.parentNode.insertBefore(js, fjs);
        }
    })(document, 'script', 'twitter-wjs');
    return self;
};
Twitter.prototype = _.extend(Twitter.prototype, {
    share: function (vars) {
        var height, leftPosition, topPosition, width, windowFeatures;
        width = 550;
        height = 500;
        leftPosition = (this.$window.innerWidth / 2) - ((width / 2) + 10);
        topPosition = (this.$window.innerHeight / 2) - ((height / 2) +
            50);
        windowFeatures = 'status=no,height=' + height + ',width=' +
            width + ',resizable=yes,left=' + leftPosition + ',top=' +
            topPosition + ',screenX=' + leftPosition + ',screenY=' +
            topPosition +
            ',toolbar=no,menubar=no,scrollbars=no,location=no,directories=no';
        this.$window.open(baseUrl + encodeURIComponent(vars.url) +
            '&text=' + encodeURIComponent(vars.text), 'sharer', windowFeatures);
    }
});
Twitter.$inject = ['$window'];
App.service('Twitter', Twitter);
//# sourceMappingURL=Twitter.js.map