/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var App = require('../app');
var LinkedIn, oauthMethods;
LinkedIn = function ($http) {
    var self;
    self = this;
    self.ready = false;
    _.each(arguments, function (val, index) {
        self[LinkedIn.$inject[index]] = val;
    });
    return self;
};
oauthMethods = {
    connect: 'connectURL',
    verify: 'verifyURL'
};
LinkedIn.prototype = _.extend(LinkedIn.prototype, {
    onLoad: function () {
        return this.ready = true;
    },
    connect: function () {
        return this._oauthCall('connect');
    },
    verify: function () {
        return this._oauthCall('verify');
    },
    _oauthCall: function (method) {
        var deferred = this.$q.defer(), callback = function (err, service) {
            if (err) {
                return deferred.reject(service);
            }
            return deferred.resolve(service);
        }, oauthMethod = oauthMethods[method], oauthURL = CP.Settings.linkedin[oauthMethod]
            .replace(/&state=[^&]*/, '')
            .replace('redirect_uri=.', 'state=' + CP.site.prefix + '&redirect_uri=https://' + CP.site.prefix + '.');
        window.onLinkedinAuth = callback;
        this.$window.open(oauthURL, 'AuthenticateLinkedin', 'width=600, height=600');
        return deferred.promise;
    }
});
LinkedIn.$inject = ['$http', '$q', '$window'];
App.service('LinkedIn', LinkedIn);
//# sourceMappingURL=LinkedIn.js.map