/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var instance = null,

    slots = {
        tandemsList: [{
            index: 1,
            template: 'views/services/AdSlot/templates/addSelfSpeakerList/Template.html',
            expr: function (listLength:number) {
                // Only show for unauthenticated users if 1 item on the Activity search page
                return listLength === 1 &&
                    !instance._Auth.isAuthenticated() &&
                    /^activity\.search/.test(instance._$state.$current);
            }
        }, {
            index: 2,
            template: 'views/services/AdSlot/templates/addSelfSpeakerList/Template.html',
            expr: function (listLength:number) {
                // Only run for unauthenticated users on the Activity search page
                return !instance._Auth.isAuthenticated() &&
                    /^activity\.search/.test(instance._$state.$current);
            }
        }]
    },

AdSlot = function (Auth, $state) {
    this._Auth = Auth;
    this._$state = $state;

    instance = this;
};

AdSlot.prototype.getSlots = function (name) {
    return slots[name];
};

AdSlot.$inject = [
    'Auth',
    '$state',
];

require('../../app.js').service('AdSlot', AdSlot);
