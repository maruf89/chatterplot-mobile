/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {Util} from 'root/services/util';
import {Language} from 'root/services/util/language';

/**
 * @description A light weight scroll to utility which has LOTS of room for improvement
 * @name Util#$scrollTo
 * @param {object} $elem - The element we're scrolling to
 * @param {object} $parent - The closest parent with scroll enabled (default:$('.pane-wrapper-scroll'))
 * @param {object=} opts
 * @param {number} opts.padding
 */
Util.prototype.$scrollTo = function ($elem:JQuery, $parent:any, opts:{padding?:number}) {
    opts = opts || { padding: 0 };

    $elem = $elem instanceof jQuery ? $elem : $($elem);

    var bounding = $elem[0].getBoundingClientRect(),
        winHeight = CP.Cache.$window.height(),
        scroller:any = this._$scroll.$getByHandle('scroll'),
        scrollX:number = bounding.bottom - winHeight + opts.padding;

    scroller.scrollBy(0, scrollX, true);
};

/**
 * @description this is a filter function for Twitter's typeahead library. It returns a function that
 * builds a list of results to autocomplete depending on what the user has entered
 * @name Util~languages#ttSubstringMatcher
 * @member {function}
 * @param {array<string>} strs - array of possible words to complete (the word bank)
 * @param {array<string>} matches - a reference to the suggestions
 * @param {array<string>} exclude - values to exclude
 * @param {number} [langOptionLimit=matches.length] - maximum number of results to show
 * @param {number} [indexOffset=0] - how much to offset the ID index of the selected value
 *      this is needed because arrays are 0-indexed and sometimes are results need to be 1-base indexed
 * @returns {function} - returns a function that is called each keystroke
 */
Language.ttSubstringMatcher = function (
    strs,
    matches,
    exclude,
    langOptionLimit,
    indexOffset
) {
    if (langOptionLimit == null) {
        langOptionLimit = matches.length;
    }
    if (indexOffset == null) {
        indexOffset = 0;
    }

    var allCache;

    /**
     * @param {string} q - the value of the inputted text
     * @param {function} sync - inner callback from the typeahead module that MUST be called with the matches
     * to finish processing
     */
    return function (q) {

        // A optimization to trigger default search
        var substrRegex;
        if (q === '…') {
            return [];
        }

        if (q === '' && allCache) {
            return allCache;
        }

        // regex used to determine if a string contains the substring `q`
        substrRegex = new RegExp(q, "i");

        // empty the array
        matches.length = 0;

        // iterate through the pool of strings and for any string that
        // contains the substring `q`, add it to the `matches` array
        _.each(strs, function (str, index) {

            // the typeahead jQuery plugin expects suggestions to a
            // JavaScript object, refer to typeahead docs for more info
            if (substrRegex.test(str)) {
                if (~_.indexOf(exclude, str)) {
                    return true;
                }

                // The languages are already sorted by their id's in ascending order
                // starting from 1, so we need to make up for array's 0-base indexing
                if (matches.push({
                        id: index + indexOffset,
                        name: str,
                    }) === langOptionLimit
                ) {
                    // if the number of items in the array is our limit, then stop looping
                    return false;
                }
            }
            return true;
        });

        if (q === '') {
            allCache = _.clone(matches);
        }

        return matches;
    };
};

require('../app').service('Util', [
    'localStorageService',
    '$q',
    '$rootScope',
    'locale',
    '$location',
    '$state',
    '$stateParams',
    '$http',
    '$ionicScrollDelegate',
    Util,
]);
