/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * @class
 * @classdesc handles everything on the front end with Cloudinary and image generation stuff
 */
var Cloudinary = require('root/services/Cloudinary'), commonCloudinary = require('common/util/cloudinary'), imageSizes = require('../config/imageSizes.json');
commonCloudinary.SIZE_PRESETS.coverPhoto = {
    width: imageSizes.coverPhoto.sm.width,
    height: imageSizes.coverPhoto.sm.height,
    fill: true,
};
require('../app')
    .service('Cloudinary', [
    'SocketIo',
    Cloudinary.service,
])
    .directive('cloudImg', [
    'Cloudinary',
    Cloudinary.directive,
]);
//# sourceMappingURL=Cloudinary.js.map