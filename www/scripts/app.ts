/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * Stores all route objects to be loaded
 * @type {Array}
 */
require('./chatterplot.templates');
require('front/modules/Facebook');
require('front/modules/miscDirectives');
require('front/modules/SocketIo');
require('front/modules/ui.router.history');
require('front/modules/ResponsiveImage');

var misc = require('front/modules/misc.js'),
    supportedLocales = require('common/config/locales'),
    routeMap = _.extend(CP.site.routes, { path: 'm/' }),
    routeConfig = [
        require('./config/routes/landingRoute')(routeMap),            // Home pages
        require('./config/routes/pStandaloneRoute')(routeMap),        // Private Standalone
        require('root/config/routes/accountRoute')(routeMap),         // User (login/signup)
        //require('root/config/routes/emailRoute')(routeMap),           // Email
        require('root/config/routes/standaloneRoute')(routeMap),      // About, Terms, Privacy Policy Pages
        require('root/config/routes/mapRoute')(routeMap),      // About, Terms, Privacy Policy Pages
    ];

module.exports = angular.module('Chatterplot', ['ionic', 'angulartics',
    'angulartics.google.analytics', 'ngAnimate', 'ngMessages', 'ipCookie',
    'ngSanitize', 'ui.router', 'ui.router.stateHelper', 'ui.router.history', 'LocalStorageModule',
    'uiGmapgoogle-maps', 'btford.modal', 'maruf89.SocketIo',
    'FacebookProvider', 'ngLocalize', 'ionic-datepicker',
    'chatterplot.miscDirectives', 'chatterplot.templates', 'maruf89.ResponsiveImage', 'ion-autocomplete',
])
    .value('localeConf', {
        basePath: 'languages',
        defaultLocale: 'en-US',
        sharedDictionary: 'common',
        fileExtension: '.lang.json?v=' + CP.deployVersion,
        persistSelection: true,
        cookieName: 'COOKIE_LOCALE_LANG',
        observableAttrs: new RegExp('^data-(?!ng-|i18n)'),
        delimiter: '||'
    })
    .value('localeSupported', supportedLocales.list)
    .value('localeFallbacks', supportedLocales.defaultMap)
    .config([
        '$stateProvider', '$locationProvider', '$urlRouterProvider',
        'stateHelperProvider', 'localStorageServiceProvider',
        '$provide', 'uiGmapGoogleMapApiProvider',
        'SocketIoProvider', 'ResponsiveImageProvider',
        function ($stateProvider, $locationProvider, $urlRouterProvider,
             stateHelperProvider, localStorageServiceProvider,
             $provide, GoogleMapApi, SocketIo, ResponsiveImageProvider
        ) {
            localStorageServiceProvider.setPrefix('cp');

            // If Internet Explorer Or iPhone then disable the socket timeout reconnect attempt
            if (misc.isIphone()) {
                SocketIo.setTimeout(false);
            }

            ResponsiveImageProvider.setResponsiveSource(require('./config/imageSizes.json'));

            // !Load Routes
            _.each(routeConfig, function (route) {
                return stateHelperProvider.setNestedState(route);
            });
            $urlRouterProvider.otherwise('/404');
            $urlRouterProvider.deferIntercept(true);

            GoogleMapApi.configure({
                key: CP.Settings.google.apiKey,
                v: '3.21',
                libraries: 'places',
                language: CP.locale.substr(0,2),
            });
            $locationProvider.html5Mode(true);
            $locationProvider.hashPrefix('!');

            // Workaround for bug #1404 - ngForm name not interpolating
            // https://github.com/angular/angular.js/issues/1404
            // Source: http://plnkr.co/edit/hSMzWC?p=preview
            $provide.decorator("ngModelDirective", function ($delegate) {
                var controller, ngModel;
                ngModel = $delegate[0];
                controller = ngModel.controller;
                ngModel.controller = [
                    "$scope", "$element", "$attrs", "$injector",
                    function (scope, element, attrs, $injector) {
                        var $interpolate;
                        $interpolate = $injector.get("$interpolate");
                        attrs.$set("name", $interpolate(attrs.name || "")(scope));
                        return $injector.invoke(controller, this, {
                            $scope: scope,
                            $element: element,
                            $attrs: attrs
                        });
                    }
                ];
                return $delegate;
            });
            $provide.decorator("formDirective", function ($delegate) {
                var controller, form;
                form = $delegate[0];
                controller = form.controller;
                form.controller = [
                    "$scope", "$element", "$attrs", "$injector",
                    function (scope, element, attrs, $injector) {
                        var $interpolate;
                        $interpolate = $injector.get("$interpolate");
                        attrs.$set("name", $interpolate(attrs.name || attrs.ngForm || "")(scope));
                        return $injector.invoke(controller, this, {
                            $scope: scope,
                            $element: element,
                            $attrs: attrs
                        });
                    }
                ];
                return $delegate;
            });

            $provide.decorator('$state', function ($delegate, $rootScope) {
                $rootScope.$on('$stateChangeStart', function (event, state, params) {
                    $delegate.next = state;
                    $delegate.toParams = params;
                });

                return $delegate;
            });
        }
    ]);