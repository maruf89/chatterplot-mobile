/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @namespace ddLayout1
 * @description a wrapper directive that shows/hides content when on/off clicked
 */
'use strict';
require('../../../app').directive('ddLayout1', ['$templateCache', function ($templateCache) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/structure/layouts/ddLayout1/Template.html'),
            scope: {
                showVariable: '=',
                showCancel: '@',
                triggerId: '@'
            },
            transclude: true,
            link: function (scope, iElem) {
                scope.vars = {
                    hideCheck: function (event, force) {
                        if (force === true ||
                            (scope.showVariable &&
                                iElem[0] !== event.target &&
                                !iElem[0].contains(event.target))) {
                            if (event.target && scope.triggerId &&
                                event.target.id === scope.triggerId) {
                                return true;
                            }
                            scope.showVariable = !scope.showVariable;
                            scope.$root.safeDigest(scope);
                            event.stopImmediatePropagation();
                        }
                        return true;
                    }
                };
                CP.Cache.$document.on('click', scope.vars.hideCheck);
                return scope.$on('$destroy', function () {
                    return CP.Cache.$document.off('click', scope.vars.hideCheck);
                });
            }
        };
    }]);
//# sourceMappingURL=Dir.js.map