/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var primary = [
    {
        key: "activities.MY_ACTIVITIES",
        sref: "map.dashboard.myActivities",
        auth: true,
        icon: "icon-group",
    },
    {
        key: "common.MESSAGES",
        sref: "pStandalone.messages.list",
        auth: true,
        icon: "icon-mail",
    },
    {
        key: "common.SEARCH",
        sref: "map.activity.search.default",
        icon: "icon-search",
    },
    {
        key: "common.SETTINGS",
        sref: "standalone.settings",
        icon: "icon-cog",
        auth: true,
    },
    {
        key: "common.LOGOUT",
        sref: "account.deauth",
        auth: true,
        icon: "icon-forward",
    },
], secondary = [
    {
        key: "common.SIGNUP",
        sref: "standalone.addYourself",
        auth: false,
    },
    {
        key: "common.LOGIN",
        sref: "account.auth({page:'login'})",
        auth: false,
    },
    {
        key: "standalone.PAGES-CONTACT",
        sref: "standalone.contact"
    },
    {
        key: "standalone.PAGES-DONATE",
        sref: "standalone.donate"
    },
    {
        key: "standalone.PAGES-FAQ",
        sref: "standalone.FAQ"
    },
], filterMenu = function (menu, authVal) {
    return _.filter(menu, function (val) {
        return typeof val.auth !== 'boolean' || val.auth === authVal;
    });
};
require('../../app').directive('siteSideMenu', [
    '$rootScope', '$templateCache', '$ionicSideMenuDelegate', 'Config', 'DataBus', 'User',
    function ($rootScope, $templateCache, $ionicSideMenuDelegate, Config, DataBus, User) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            transclude: true,
            template: $templateCache.get('views/structure/sideMenu/Template.html'),
            link: {
                pre: function (scope) {
                    var setMenu = function (loggedIn) {
                        scope.vars = {
                            primary: filterMenu(primary, loggedIn),
                            secondary: filterMenu(secondary, loggedIn),
                        };
                        scope.$root.safeDigest(scope);
                    };
                    DataBus.on('/site/authChange', setMenu);
                    setMenu(false);
                    scope.$on('$destroy', function () {
                        DataBus.removeListener('/site/authChange', setMenu);
                    });
                },
                post: function (scope, iElem) {
                    var loadNameNAvatar = function (doLoad) {
                        if (!doLoad) {
                            return;
                        }
                        iElem.find('#sideMenuHead')[0].innerText = User.data.firstName;
                    }, closeMenuListener = $rootScope.$on('$locationChangeSuccess', function () {
                        $ionicSideMenuDelegate.toggleRight(false);
                    });
                    DataBus.on('/site/authChange', loadNameNAvatar);
                    scope.$on('$destroy', function () {
                        DataBus.removeListener('/site/authChange', loadNameNAvatar);
                        closeMenuListener();
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map