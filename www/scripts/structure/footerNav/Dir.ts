/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app.js').directive('siteFooterNav', [
    'Config', '$state', '$templateCache',
    function (Config, $state, $templateCache) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            transclude: true,
            template: $templateCache.get('views/structure/footerNav/Template.html'),
            link: {
                pre: function (scope) {
                    scope.vars = {
                        routes: Config.routes,
                    };
                }
            }
        };
    }
]);
