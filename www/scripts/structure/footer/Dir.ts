/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app.js').directive('siteFooter', [
    '$templateCache', 'Config', 'DataBus', 'feedbackModal', 'contactModal', 'Auth',
    function ($templateCache, Config, DataBus, feedbackModal, contactModal, Auth) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            transclude: true,
            template: $templateCache.get('views/structure/footer/Template.html'),
            link: {
                pre: function (scope) {
                    scope.Config = Config;
                    scope.routes = Config.routes;

                    var modals = {
                        feedback: feedbackModal,
                        contact: contactModal
                    };

                    scope.vars = {
                        modal: function (what) {
                            return modals[what].activate({
                                modal: {
                                    deactivate: modals[
                                        what].deactivate
                                }
                            });
                        },
                        logout: function () {
                            return Auth.deauthenticate(null, 'user');
                        }
                    };
                }
            }
        };
    }
]);
