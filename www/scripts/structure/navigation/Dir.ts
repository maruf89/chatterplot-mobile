/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * @ngdoc directive
 * @module navigationDirective
 *
 * @require Config
 * @require Auth
 * @require $state
 * @require eventHostModal
 * @require User
 * @require $q
 * @require locale
 */
'use strict';

require('../../app').directive('siteNav', [
    'Config', '$state', '$ionicSideMenuDelegate', '$templateCache',
    function (Config, $state, $ionicSideMenuDelegate, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/structure/navigation/Template.html'),
            link: function (scope, iElem) {
                scope.vars = {
                    goHome: function () {
                        return $state.go(Config.routes.onLogout);
                    },
                    toggleRightMenu: function () {
                        $ionicSideMenuDelegate.toggleRight();
                    },
                };

                // Needed by other parts of the site for sticky nav
                CP.Global.vars.navHeight = iElem.outerHeight();
            }
        };
    }
]);
