/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * @module quickSearchDirective
 *
 * @requires Util
 * @requires Venues
 * @requires locale
 * @requires $q
 * @requires $templateCache
 * @requires Activities
 */
'use strict';

require('../../../../app').directive('quickSearch', [
    'Util', 'Venues', 'locale', '$q', '$compile', '$templateCache', 'Activities', '$analytics', 'DataBus', 'Maps',
    function (
        Util:any,
        Venues:cp.venue.IService,
        locale:any,
        $q:ng.IQService,
        $compile:ng.ICompileService,
        $templateCache:ng.ITemplateCacheService,
        Activities:cp.activities.IService,
        $analytics:Angulartics.IAnalyticsService,
        DataBus:any,
        Maps:cp.maps.IService
    ) {

        // preload
        locale.ready('languages');
        return {
            restrict: 'E',
            replace: true,
            scope: {},
            template: $templateCache.get('views/structure/navigation/directives/quickSearch/Template.html'),
            link: {
                pre: function (scope) {
                    scope.vars = {
                        focus: false,
                        showBox: false,
                        passiveLoc: false, // whether the location was set non-manually
                        filter: {
                            language: [],
                            type: 'tandems',
                            location: null,
                        },
                        langComplete: {},
                        location: {
                            "default": null,
                            retrievingDefault: null,
                            value: null,
                            force: true,
                        }
                    };
                },
                post: function (scope, iElem) {
                    var vars = scope.vars,
                        $subhead = $('#subNav'),

                        /**
                         * @description shows/hides the subheader and updates the variable for whether it's active
                         * @param val
                         */
                        toggleSubhead = function (val?:boolean):void {
                            // invert the boolean if it's passed so true = show & false = hide
                            vars.focus = val = typeof val === 'boolean' ? !val : true;
                            $subhead.toggleClass('hidden', val);
                        },

                        buildDefaultLocation = function (location) {
                            return Util.geo.latlng2Address(location)
                                .then(function (places) {
                                    vars.location.retrievingDefault = false;
                                    if (!places[0]) {
                                        return;
                                    }

                                    var address:cp.venue.IVenueFull = Venues.scrapePlace(places[0], 'google');
                                    vars.filter.location = address;

                                    if (address.city) {
                                        vars.location["default"] = `${address.city}, ${address.country}`;
                                        vars.location.force = false;

                                        // 12 is the city/postal zoom level
                                        vars.filter.location.coords.zoom = 12;
                                    }
                                });
                        },

                        // Function that hides the popout modal if it's not selected
                        hideCheck = function (event, force) {
                            if (force !== true &&
                                (!vars.focus ||
                                    iElem[0] === event.target ||
                                    event.target === document.body ||
                                    iElem[0].contains(event.target) ||
                                    !document.body.contains(event.target)
                                )
                            ) {
                                return true;
                            }

                            // hide the window
                            toggleSubhead(false);
                        },

                        getLangObj = function (langIDs:number[]):ng.IPromise<any> {
                            if (!langIDs || !langIDs.length) {
                                langIDs = [0];
                            }

                            return Util.language.expand(Util.language.arrayToObject(langIDs), langIDs);
                        },

                        onSearchUpdate = function (options:cp.activities.ISearchOptionsOpts) {
                            getLangObj(options.languages).then(function (data) {
                                vars.getLanguage(data[0].languageID, data[0].name, true);
                                hideCheck(null, true);
                            });
                        };

                    CP.Cache.$document.on('click', hideCheck);

                    DataBus.on('/search/refresh', onSearchUpdate);

                    vars.onFocus = function () {
                        if (!vars.location["default"] && !vars.location.retrievingDefault) {
                            vars.location.retrievingDefault = true;
                            Util.geo.getMyLocation().then(buildDefaultLocation);
                        }

                        scope.$root.safeDigest(scope);
                    };

                    vars.onLangChange = function (langID, name, notManual) {
                        vars.getLanguage.apply(vars, arguments);

                        vars.somethingChanged();
                    };

                    vars.getLanguage = function (langID, name, notManual) {
                        toggleSubhead(true);

                        this.filter.language = [langID];

                        if (!notManual) {
                            $analytics.eventTrack('click', {
                                category: 'search',
                                label: 'quickSearch: language selected'
                            });
                        }

                        // Important that we return false here to stop autocomplete
                        return true;
                    };

                    vars.invalidateSearch = function (field) {
                        return this.filter[field] = null;
                    };

                    vars.addLoc = function (place, passive) {
                        this.filter.location = place;
                        this.passiveLoc = passive;
                        vars.somethingChanged();
                        scope.$root.safeDigest(scope);
                    };

                    vars.somethingChanged = function () {
                        // Let anyone who wants know when it happens
                        DataBus.emit('/search/updated', {
                            // can't be called twice for the same change
                            search: _.once(vars.submitSearch.bind(vars)),
                        });
                    };

                    vars.submitSearch = function ():ng.IPromise<any> {
                        var coords,
                            search:any = {},
                            filter = this.filter,
                            hasLang:boolean = !!filter.language.length,
                            hasLoc:boolean = !!filter.location,
                            specificity:cp.venue.ISpecificity | number,
                            applyLoc = function () {
                                specificity = vars.passiveLoc ? 12 : Venues.scrapeSpecificity(filter.location, 'google');

                                if (!(coords = filter.location.coords)) {
                                    coords = Venues.getCoords(filter.location, 'google');
                                }

                                // Don't zoom in too far
                                coords.zoom = specificity.zoom;

                                search.location = { coords: coords };
                            },
                            promise:ng.IPromise<any>;


                        if (hasLang) {
                            search.languages = filter.language;
                        }

                        if (hasLoc) {
                            applyLoc();
                        } else {
                            promise = Util.geo.getMyLocation().then(function (location) {
                                location.passiveLoc = true;
                                filter.location = { coords: Maps.normalizeCoords(location, true) };
                                applyLoc();
                                return true;
                            });
                        }

                        promise = promise || $q.when();

                        return promise.then(function () {
                            $analytics.eventTrack('submit', {
                                category: 'search',
                                label: 'quickSearch'
                            });

                            return Activities.search(search, filter.type, 'language');
                        });
                    };

                    scope.$root.$on('$locationChangeSuccess', function () {
                        toggleSubhead();
                    });

                    //* Doesn't ever get removed so unnecessary
                    //scope.$on('$destroy', function () {
                    //    CP.Cache.$document.off('click', hideCheck);
                    //});
                }
            }
        };
    }
]);
