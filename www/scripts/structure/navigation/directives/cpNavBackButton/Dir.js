/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../../app').directive('cpNavBackButton', [
    '$templateCache',
    'Util',
    function ($templateCache, Util) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/structure/navigation/directives/cpNavBackButton/Template.html'),
            link: function (scope, iElem) {
                var $logo = iElem.children('.nav-logo'), $back = iElem.children('.nav-back'), hasBack = false, updateVisibility = function (showBack) {
                    $logo.toggleClass('hidden', showBack);
                    $back.toggleClass('hidden', !showBack);
                };
                scope.$root.$on('$locationChangeSuccess', function () {
                    _.defer(function () {
                        var curHasBack = Util.location.canShowBack();
                        if (hasBack !== curHasBack) {
                            updateVisibility(curHasBack);
                        }
                        hasBack = curHasBack;
                    });
                });
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map