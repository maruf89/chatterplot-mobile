/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../../app').directive('cpNavBackButton', [
    '$templateCache',
    'Util',
    function (
        $templateCache:ng.ITemplateCacheService,
        Util:any
    ) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/structure/navigation/directives/cpNavBackButton/Template.html'),
            link: function (scope:cp.IScope, iElem:JQuery) {
                var $logo:JQuery = iElem.children('.nav-logo'),
                    $back:JQuery = iElem.children('.nav-back'),

                    hasBack:boolean = false,

                    updateVisibility = function (showBack:boolean) {
                        $logo.toggleClass('hidden', showBack);
                        $back.toggleClass('hidden', !showBack);
                    };

                scope.$root.$on('$locationChangeSuccess', function () {
                    _.defer(function () {
                        var curHasBack:boolean = Util.location.canShowBack();

                        if (hasBack !== curHasBack) {
                            updateVisibility(curHasBack);
                        }

                        hasBack = curHasBack;
                    });
                });
            }
        }
    }
]);
