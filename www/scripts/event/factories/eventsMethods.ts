/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var groupSort = require('common/util/groupSort'),
    listUtil = require('common/front/util/list'),

attendingOptions = {},

_attendingOptions = {
    'true': 'IM_GOING',
    'false': 'IM_NOT_GOING'
},

waitingOptions = {},

_waitingOptions = {
    'true': 'IM_WAITING',
    'false': 'LEAVE_WAITLIST'
},

translated = false,

_translateFn = function (locale) {
    if (!translated) {
        locale.ready('activities')
            .then(function () {
                _.each(_attendingOptions, function (val, key) {
                    attendingOptions[key] = locale.getString('activities.' + val);
                });

                _.each(_waitingOptions, function (val, key) {
                    waitingOptions[key] = locale.getString('activities.' + val);
                });
            });
    }
};

require('../../app').factory('eventsMethods', [
    'DataBus',
    '$q',
    'Events',
    'Auth',
    'Util',
    '$rootScope',
    'locale',
    'User',
    function (Bus, $q, Events, Auth, Util, $rootScope, locale, User) {
        _translateFn(locale);

        var EventsMethods = function (opts, groups, eventsList) {
            if (!(groups)) {
                throw new Error('EventsMethod requires an groups {array} parameter');
            }

            this.eventGroups = groups;
            this.eventsList = eventsList || [];
            this.opts = opts;
            this.hooks = this.opts.hooks || {};
        };

        EventsMethods.prototype = _.extend(EventsMethods.prototype, {
            eventFromCurrent: function (eID) {
                if (!this.eventsList.length && this.eventGroups.length) {
                    this.eventsList = _.flatten(this.eventGroups);
                }

                var event,
                    i = 0,
                    ref = this.eventsList,
                    len = ref.length;

                for (;i < len; i++) {
                    event = ref[i];
                    if (p(eID) === p(event.eID)) {
                        return event;
                    }
                }
                return false;
            },

            /**
             * @description formats an array of events into groups
             * @param {array<object>} events
             * @returns {array<array>}
             */
            format: function (events) {
                // group the items in a hashed array
                var grouped = groupSort.formatItems(events, {
                    groupBy: 'date',
                    onEachFn: Events.format.bind(Events)
                });

                // next add alternate odd/even classes to each item
                listUtil.alternateGroupsArray(grouped, events.length);

                return grouped;
            },

            /**
             * @description adds events to own list
             *
             * @param {array<object>} events
             * @returns {number} how many events are in the current list
             */
            addEvents: function (events) {
                return [].push.apply(this.eventsList, events);
            },

            /**
             * @description either accepts the passed in events, or uses the current and rebuilds the groups
             *
             * @param {array<object>} events
             * @returns {array<array>}
             */
            rebuildList: function (events) {
                events = events || this.eventsList;
                return this.eventGroups = this.format(events);
            },

            /**
             * @description empties both the eventsList and eventGroups and replaces them
             *
             * @param {array<array>} groups - already formatted
             */
            replaceGroups: function (groups, groupsToReplace) {
                if (!groups) {
                    groups = this.eventGroups;
                }
                this.eventsList.length = 0;
                groups.length = 0;

                this.addEvents(_.flatten(groupsToReplace));
                [].push.apply(groups, groupsToReplace);
                return groups;
            },

            /**
             * @description combines to groups of preformatted event array groups
             *
             * @param {array<array>} groups
             * @param {array<array>} groupsToAdd
             * @returns {array<array>} - 1 combined groups array
             */
            stitchGroups: function (groups, groupsToAdd) {
                // add the new events to our list
                this.addEvents(_.flatten(groups));

                // lastly safely combine both arrays and return as one
                return groupSort.stitchGroups(groups, groupsToAdd, 'date');
            },

            attendingOptions: attendingOptions,
            waitingOptions: waitingOptions,

            /**
             * @description updates a
             * @param event
             * @param attending
             * @param asWaitlist
             * @returns {Promise.<T>}
             */
            toggleAttending: function (event, attending, asWaitlist) {
                var onAuth,
                    self = this,
                    eID = event.eID;

                // Force user to sign up/log in before they can do that
                if (!Auth.isAuthenticated()) {
                    // On authentication recall this method with the same arguments
                    onAuth = self.toggleAttending.bind(self, event, attending, asWaitlist);
                    return Auth.loginRequired(null, null).then(onAuth);
                }

                return Events.updateAttendance(event, attending, asWaitlist)
                    .then(function (response) {
                        response.vars.asWaitlist = asWaitlist;

                        if (event = self.eventFromCurrent(eID)) {
                            $rootScope.safeApply(function () {
                                return self._processRSVP(
                                    event,
                                    attending,
                                    response.vars
                                );
                            });
                        }
                        return Bus.emit('yapServerResponse', response);
                    }, function (err) {
                        return Bus.emit('yapServerResponse', err);
                    });
            },

            /**
             * @description complex process of figuring out whether to:
             *                      - ADD a user to the goingList
             *                      - REMOVE a user from the goingList
             *                      - ADD a user to the waitList
             *                      - REMOVE a user from the waitList
             * @param {object} event
             * @param {boolean} attending
             * @param {object} response - response from the server
             * @param {boolean} response.waitList - whether the user was added to the wait list
             * @returns {*}
             * @private
             */
            _processRSVP: function (event, attending, response) {
                var arr, waitList,
                    uAttending = User.data.attending;

                if (event.imWaiting = response.waitList) {
                    // Wait List Stuff

                    event.goingCount = event.maxGoing;
                    event.capGoing = true;
                    event.imAttending = false;

                    if (attending) {
                        // Add to wait list

                        uAttending.eventsWaitList.push(event.eID);
                        if (waitList = event.waitList) {
                            waitList.push($rootScope.userID);
                        } else {
                            event.waitList = [$rootScope.userID];
                        }
                    } else {
                        // Remove from wait list

                        event.imWaiting = false;
                        Util.arrRemove(event.waitList, $rootScope.userID);
                        Util.arrRemove(uAttending.eventsWaitList, event.eID);
                    }
                } else {
                    // Non-waitlist Stuff

                    if (attending) {
                        // User Attendance +

                        if (!response.asWaitList) {
                            // If not wait listed increment the counter of active going

                            event.goingCount++;
                        }

                        event.goingList.push($rootScope.userID);
                        uAttending.events.push(event.eID);
                    } else {
                        // User Attendance -

                        if (response.fromGoing) {
                            // If user wasn't on the wait list decrement the visible going #

                            event.goingCount--;
                        }

                        arr = response.fromGoing ? event.goingList : event.waitList;
                        Util.arrRemove(arr, $rootScope.userID);
                        Util.arrRemove(uAttending.events, event.eID);
                    }

                    event.imAttending = !!attending;
                    event.capGoing = false;
                }
            },

            /**
             * @deprecated
             * @param event
             * @returns {Promise.<T>}
             */
            publishEvent: function (event) {
                return Events.publish(event)
                    .then(function (response) {
                        $rootScope.safeApply(function () {
                            return event.published = true;
                        });
                        return Bus.emit('yapServerResponse', response);
                    }, function (err) {
                        return Bus.emit('yapServerResponse', err);
                    });
            },

            reformatAll: function () {
                _.each(this.eventsList, function (event) {
                    Events.format(event, true);
                });

                return this.eventsList;
            },

            loadEvent: function (eIDs, type) {
                var self = this;
                return Events.get({
                    eIDs: eIDs,
                    type: type
                })
                .then(function (events) {
                    self.addEvents(events);
                    return self.rebuildList();
                });
            }
        });

        return EventsMethods;
    }
]);
