/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var _requiredFields = [
    'when', 'name', 'languages', 'description', {
        field: 'venue',
        children: ['vID', 'coords']
    }
];
/**
 * @class
 * @class Handles all CRUD event functionality
 */
var Events = (function () {
    function Events(_$q, _$state, _Request, _SocketIo, _Util, _User, _Permissions, _DataBus, _Config, _Venues, Helper, EventMap) {
        this._$q = _$q;
        this._$state = _$state;
        this._Request = _Request;
        this._SocketIo = _SocketIo;
        this._Util = _Util;
        this._User = _User;
        this._Permissions = _Permissions;
        this._DataBus = _DataBus;
        this._Config = _Config;
        this._Venues = _Venues;
        this.helper = Helper.get(this);
        this.map = new EventMap(this);
        this.cache = {};
        this.eventCache = {};
        this._DataBus.on('/site/authChange', this.reformatEvents.bind(this));
    }
    /**
     * @description Sends the event to the server to be created/updated
     * @name Events#create
     * @param  {object} eventObj - Event details
     * @return {Promise} Promise will return the entire saved event object
     */
    Events.prototype.create = function (eventObj, place) {
        var context = this, missingField = this._Util.lacksFields(_requiredFields, eventObj), sendObj = {
            indexType: eventObj.indexType,
            event: eventObj
        };
        // Unless the passed in eventObject has all the required fields, throw an error 
        if (missingField) {
            return this._$q.reject({
                error: 'missing field',
                missing: missingField
            });
        }
        return this._SocketIo.onEmitSock('/event/create', sendObj).then(function (newEvent) {
            newEvent.venueDetails = place;
            newEvent.stillIndexing = true;
            return context.format(newEvent, true);
        });
    };
    /**
     * @description Sends an update request to the server
     * @name Events#update
     * @param {object} data          Must contain:
     * @param {number} data.eID      the event id
     * @param {object} data.updates  object containing only the fields to update
     * @param {string} data.type     type of event: event|listing
     * @return {Promise<boolean>}
     */
    Events.prototype.update = function (data) {
        var cache = this.cache;
        if (typeof data.eID !== 'number' && !_.isPlainObject(data.updates)) {
            throw new Error('Invalid arguments passed to Event.update - expecting `eID` and `updates` props');
        }
        return this._SocketIo.onEmitSock('/event/update', data).then(function (arg) {
            // Update the cached event with the changes & reformat it
            this.format(_.extend(this.cache[data.eID], data.updates), true);
            return arg;
        }.bind(this));
    };
    /**
     * @description does a search for events
     *
     * @name Events#fetch
     * @param {object} opts
     * @param {boolean} opts.markerSearch - if true will return a markers array on the return object
     * @returns {Promise<object>} returnObj
     * @returns {array<object>} returnObj.events
     * @returns {array<object>=} returnObj.markers
     */
    Events.prototype.fetch = function (opts) {
        var context = this;
        return this._Request.onEmitSock('/event/fetch', opts, true).then(function (response) {
            _.each(response.events, context.format, context);
            return response;
        });
    };
    /**
     * @description gets either a single or multiple events from the server
     *
     * @name Events#get
     * @returns {Promise<array>}
     */
    Events.prototype.get = function (data) {
        var req = {}, preCached, filtered, checkCache;
        if (_.isArray(data)) {
            checkCache = true;
            // assume it's an array of objects defining both `eID` and `type`
            req = { evTypes: data };
        }
        else if (data.eID) {
            if (CP.Settings.isDev) {
                throw new Error('This way of calling is deprecated…');
            }
            req = {
                eIDs: [data.eID],
                withVenues: data.withVenues || data.venues,
                _source: data._source,
            };
        }
        else if (!data.eIDs && !_.isArray(data.evTypes)) {
            throw new Error('Events#get expects an array as the first parameter or object with eIDs');
        }
        else {
            checkCache = true;
            req = data;
        }
        if (checkCache) {
            preCached = [];
            filtered = _.reduce(req.evTypes, function (arr, evType) {
                var cached = this.cache[evType.eID];
                // Here we ensure the cached item has all the required parts
                if (cached &&
                    (!req.venues || cached.venueDetails) &&
                    (!req.host || cached.host) &&
                    (!req.description || cached.description)) {
                    preCached.push(cached);
                }
                else {
                    arr.push(evType);
                }
                return arr;
            }, [], this);
            if (!filtered.length) {
                return this._$q.when(preCached);
            }
        }
        return this._Request.onEmitSock('/event/get', req).then(function (events) {
            // format/cache the new events
            _.each(events, this.format, this);
            // If we got some events from the venue, upon receiving the results from the server
            // add the preexisting events
            if (preCached && preCached.length) {
                events = preCached.concat(events);
            }
            return events;
        }.bind(this));
    };
    /**
     * Extends/updates an event object with additional attributes parsing the
     * current ones
     *
     * @iterable
     * @param {object} event - the event object to extend
     * @param {boolean=} force - whether to format format this object
     * @return {object}
     */
    Events.prototype.format = function (event, force) {
        var date, endDate, going, radius;
        // include anything the previous event may have had
        if (this.cache[event.eID]) {
            event = _.extend(this.cache[event.eID], event);
        }
        if (event.formatted && force !== true) {
            return event;
        }
        // As an activity, it will need to know what kind of activitiy it is
        event.activityType = 'event';
        // unique ID for this
        event.activityID = "ev-" + event.eID;
        event.vars = {};
        date = this._Config.format.date.toObj('basic_date_time_no_millis', event.when);
        event.date = this._Config.format.date.display(date, 'longDate');
        event.time = this._Config.format.date.displayTime(date);
        event.whenISO = date.toISOString();
        if (event.venueDetails) {
            event.formattedAddress = this._Util.geo.format.address(event.venueDetails);
        }
        if (event.until) {
            endDate = this._Config.format.date.toObj('basic_date_time_no_millis', event.until);
            event.endDate = this._Config.format.date.display(endDate, 'longDate');
            event.endTime = this._Config.format.date.displayTime(endDate);
            event.untilISO = endDate.toISOString();
        }
        event.uiSref = 'map.activity.' + event.indexType + 's.single({ eID:' + event.eID + '})';
        event.href = this._$state.href('map.activity.' + event.indexType + 's.single', {
            eID: event.eID
        });
        event.canonicalURL = CP.site.name + event.href;
        if (!event.host && event.creator && this._User[event.creator]) {
            event.host = this._User[event.creator];
        }
        if (!event.stillIndexing) {
            if (going = event.goingList) {
                event.goingList = _.uniq(going);
            }
            else {
                going = event.goingList = [];
            }
            event.goingCount = going.length || 0;
            event.displayLocation = event.locationDisplayName;
            event.gmapLink = this._Venues.addressObjToUrl('google', event.venue);
        }
        // Calls _eventFormat || _listingFormat
        this['_' + event.indexType + 'Format'](event);
        if ((radius = event.venue.rsvpOnlyRadius) && !this.amIAttending(event.eID)) {
            event.venue.hidden = true;
            event.__V = _.cloneDeep(event.venue);
            event.venue.coords = this._Util.geo.rndPtWithin(event.venue.coords, radius);
        }
        this._formatLoginable(event);
        event.formatted = true;
        // Save the event in the general event/listing cache
        this.cache[event.eID] = event;
        return event;
    };
    Events.prototype._eventFormat = function (event) {
        if (!event.stillIndexing) {
            event.imWaiting = !event.imAttending &&
                event.waitList &&
                _.indexOf(event.waitList, this._User.userID) > -1;
        }
        if (event.host) {
            event.host.fullName = event.host.firstName + "  " + event.host.lastName;
            if (!event.aboutHost) {
                event.aboutHost = event.host.personal;
            }
        }
        // waitlist
        if (event.goingCount >= event.maxGoing) {
            event.capGoing = true;
        }
        // save the event in the event cache
        this.eventCache[event.eID] = event;
    };
    Events.prototype._listingFormat = function (event) { };
    /**
     * @description updates values pertaining to user's logged in state
     *
     * @param {object} event
     * @private
     */
    Events.prototype._formatLoginable = function (event) {
        var ID = this._User.userID;
        event.imHosting = ID && !!(ID === event.creator || event.stillIndexing);
        event.imAttending = ID && _.indexOf(event.goingList || [], ID) > -1;
        event.vars.attending = event.imAttending || event.imWaiting ? 'true' : 'false';
    };
    /**
     * @description reformats all cached events and updates values pertaining to user's logged in state
     */
    Events.prototype.reformatEvents = function () {
        _.each(this.cache, this._formatLoginable, this);
    };
    /**
     * @description updates both the event and the current user as either attending/not attending an event
     * @name Events#updateAttendance
     * @param {object} event
     * @param {number} event.eID - events ID
     * @param {string} event.type - one of (event|listing)
     * @param {boolean} attending - true if attending | false if not
     * @param {boolean} asWaitList - whether to add the user to the wait list
     *                               TODO: this should be handled by the backend
     * @returns {Promise}
     */
    Events.prototype.updateAttendance = function (event, attending, asWaitList) {
        var data = {
            eID: event.eID,
            attending: !!attending,
            type: event.indexType || 'event',
            asWaitlist: asWaitList,
        };
        return this._SocketIo.onEmitSock('/event/updateAttendance', data);
    };
    /**
     * @description marks an event as published and makes it viewable to the public
     * @name Events#publish
     * @param {object} event
     * @param {number} event.eID - events ID
     * @param {string} event.type - one of (event|listing)
     * @param {string} type - index type
     * @returns {Promise}
     */
    Events.prototype.publish = function (event, type) {
        type = type || event.indexType || 'event';
        if (!(this._User.userID === event.creator && this._Permissions.can(['PUBLISH_EVENT']))) {
            return this._$q.reject({
                response: 400,
                type: 'updates.ERROR-INVALID_PERMISSIONS'
            });
        }
        this._DataBus.emit('progressLoader', {
            start: true
        });
        return this._SocketIo.onEmitSock('/event/publish', {
            eID: event.eID,
            type: type
        })["finally"](function () {
            return this._DataBus.emit('progressLoader');
        }.bind(this));
    };
    /**
     * @description sends a request to delete an event from the server
     * @name Events#delete
     * @param {object} event
     * @param {number} event.eID - events ID
     * @param {string} event.type - one of (event|listing)
     * @param {string=} event.notifyMessage - if passed will be sent to the attendees of the event
     * @returns {Promise}
     */
    Events.prototype["delete"] = function (event) {
        return this._SocketIo.onEmitSock('/event/delete', event).then(function (arg) {
            delete this.cache[event.eID];
            return arg;
        }.bind(this));
    };
    /**
     * @description Checks whether a user is part of an events going list
     * @name Events#amIAttending
     * @param  {number=} eID - event ID
     * @return {boolean} - whether the current user is attending said event
     */
    Events.prototype.amIAttending = function (eID) {
        return this._User.data.attending && this._User.data.attending.events.indexOf(p(eID)) > -1;
    };
    Events.prototype.view = function (eID) {
        console.log('view event…');
        return this._$q.when({ completed: true });
    };
    Events.prototype.parseSortRequests = function (action, items) {
        return this._$q.when(items);
    };
    /**
     * @description Notify users
     *
     * @param {object} data
     * @param {array<number>} data.users - array of userIDs to notify
     * @param {string} data.message - what to send
     * @param {string} data.emailTemplate - what type of email template to use
     * @returns {Promise<boolean>}
     */
    Events.prototype.notifyUsers = function (data) {
        if (!data.eID || !data.type) {
            throw new Error('missing fields in Events.notifyUsers');
        }
        return this._SocketIo.onEmitSock('/event/authNotify', data);
    };
    return Events;
})();
Events.$inject = [
    '$q',
    '$state',
    'Request',
    'SocketIo',
    'Util',
    'User',
    'Permissions',
    'DataBus',
    'Config',
    'Venues',
    'EventHelper',
    'EventMap',
];
require('../app.js').service('Events', Events);
//# sourceMappingURL=Service.js.map