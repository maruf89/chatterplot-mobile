/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var MODULE_TYPE:string = 'event',
    MODULE_NAME:string = MODULE_TYPE + 'List',

    _defaultSearch = {
        venues: true,
        what: MODULE_TYPE,
    };

import {AbstractDirective} from 'root/search/AbstractListDir';


require('../../../app').directive(MODULE_NAME, [
    'DataBus', '$compile', '$analytics', '$q', 'Util', '$templateCache', '$ionicScrollDelegate',
    function (
        DataBus:any,
        $compile:ng.ICompileService,
        $analytics:any,
        $q:ng.IQService,
        Util:any,
        $templateCache:ng.ITemplateCacheService,
        $ionicScrollDelegate:any
    ) {
        var listDirective:cp.activities.IListDirective = new AbstractDirective({
                moduleName: MODULE_NAME,
                moduleType: MODULE_TYPE,
                itemKeyID: 'eID',
            },
            _defaultSearch,
            DataBus,
            $analytics,
            $compile,
            $q,
            Util,
            $ionicScrollDelegate
        );

        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                options: '=?',
                onRefresh: '=?',
                zeroDataDirective: '@',
                stack: '=?',
                group: '=?',
            },
            template: $templateCache.get('views/event/directives/list/Template.html'),
            controller: 'EventListCtrl',
            link: {
                pre: listDirective.pre.bind(listDirective),

                post: listDirective.postCallback(function (scope) {
                    // If a user logs out, reset events to defaults
                    var authReformatFn = function () {
                        _.defer(function () {
                            scope.methods.reformatAll();
                            scope.$root.safeDigest(scope);
                        });
                    };

                    DataBus.on('/site/authChange', authReformatFn);

                    scope.$on('$destroy', function () {
                        DataBus.removeListener('/site/authChange', authReformatFn);
                    });
                })
            }
        };
    }
]);
