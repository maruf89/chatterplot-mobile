/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('myOwnEvents', [
    'User', '$templateCache',
    function (User, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                id: '@',
                size: '@',
                filterTypes: '@',
                showMore: '@',
                options: '=?',
                onRefresh: '=?',
                zeroDataDirective: '@',
                stack: '=?'
            },
            template: $templateCache.get('views/event/directives/myOwnEvents/Template.html'),
            link: {
                pre: function (scope) {
                    var _filterTypes = scope.filterTypes ? scope.filterTypes.split(',') : ['attending'];
                    scope.vars = {
                        searchOptions: {
                            size: 10,
                            filterTypes: ['current'].concat(_filterTypes)
                        }
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map