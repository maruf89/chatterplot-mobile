/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var stackName = 'eventSingle';
var EventSingleCtrl = (function () {
    function EventSingleCtrl(_$scope, _$window, _$stateParams, _$sce, _DataBus, _Util, _Events, _eventsMethods, _Activities, _Maps, _modalConfirm, _locale, _Meta, _User, _Modal, Datum) {
        this._$scope = _$scope;
        this._$window = _$window;
        this._$stateParams = _$stateParams;
        this._$sce = _$sce;
        this._DataBus = _DataBus;
        this._Util = _Util;
        this._Events = _Events;
        this._eventsMethods = _eventsMethods;
        this._Activities = _Activities;
        this._Maps = _Maps;
        this._modalConfirm = _modalConfirm;
        this._locale = _locale;
        this._Meta = _Meta;
        this._User = _User;
        this._Modal = _Modal;
        this.Datum = Datum;
        this.data = {
            trustedDescription: null
        };
        this.init();
        // Map Stack Init 
        this.mapInit();
        var context = this, listActions = {
            'single_attendEvent': function () {
                var objArgs = _$stateParams;
                return context.methods.toggleAttending({ eID: Datum.eID }, true, objArgs && objArgs.waitList).then(function () {
                    return _Util.location.stripQP(true);
                });
            }
        }, attendanceChange = this.attendanceChange.bind(this);
        _$scope.$watch('Event.data.imAttending', attendanceChange);
        _$scope.$watch('Event.data.imWaiting', attendanceChange);
        // Run any saved functions
        if (listActions[_$stateParams.action]) {
            listActions[_$stateParams.action](this._$stateParams);
        }
        // unbind listener upon death 
        _$scope.$on('$destroy', function () {
            _Activities.map.destroyStack();
            if (context._infoWindow && context._infoWindow.close) {
                context._infoWindow.close();
            }
        });
        this._$scope.$root.mustBeLoadedIn(3000);
    }
    EventSingleCtrl.prototype.init = function () {
        //this.data.showBack = this._Util.location.isBack();
        this.data.trustedDescription = this._$sce.trustAsHtml(this.Datum.description);
        //this.editPhoto = null;
        this.meta = {
            name: this._locale.getString('events.NEW_EVENT', {
                event_name: this.Datum.name
            }),
            description: this._Meta.stripHTML(this.Datum.description),
            image: this.Datum.hasPhoto ? this._Meta.image(this.Datum.eID, 'events') : '',
            hostImg: this.Datum.indexType === 'event' && this._User.format.photoSrc(this.Datum.host),
        };
        this.methods = new this._eventsMethods({
            hooks: {
                toggleAttendingReject: function (eID, waitList) {
                    return {
                        action: 'single_attendEvent',
                        eID: eID,
                        args: "waitList:" + waitList
                    };
                }
            }
        }, [this.Datum]);
    };
    EventSingleCtrl.prototype.mapInit = function () {
        this._Activities.map.initStack(stackName);
        this._Activities.map.setMarkers([this.Datum], {
            classType: 'event',
            replace: true,
            identifier: stackName
        });
        this._Maps.getInstance().center(15);
        // Load the address window 
        this.buildWindow();
    };
    EventSingleCtrl.prototype.goBack = function () {
        return this._$window.history.back();
    };
    EventSingleCtrl.prototype.attendanceChange = function (attending, prev) {
        if (typeof attending === 'boolean' && attending !== prev) {
            this.Datum.vars.attending = String(attending);
            this._Activities.updateMarkers(attending);
            if (this.Datum.venue.rsvpOnlyRadius) {
                this.Datum.__V = this.Datum.__V || _.cloneDeep(this.Datum.venue);
                if (attending) {
                    this.Datum.venue = _.cloneDeep(this.Datum.__V);
                }
                else {
                    this.Datum.venue.coords = this.Datum.marker.coords;
                }
                this.Datum.venue.hidden = !attending;
            }
            _.defer(this.buildWindow.bind(this));
        }
    };
    /**
     * @descriptions opens a modal which upon entering a message and submitting
     * sends it to the backend to notify all event attendees
     *
     * @returns {IPromise<object>}
     */
    EventSingleCtrl.prototype.notifyAttendees = function () {
        var Modal = this._Modal, DataBus = this._DataBus;
        return Modal.activate({
            templateUrl: 'views/event/templates/NotifyAttendeesTemplate.html',
            container: '.map-pane',
        })
            .then(function (vars) {
            return this._Events.notifyUsers({
                eID: this.Datum.eID,
                type: this.Datum.indexType,
                emailKey: 'NOTIFY_ATTENDEES',
                excludeCreator: true,
                data: {
                    message: vars.message,
                }
            });
        }.bind(this))
            .catch(function (err) {
            throw DataBus.emit('yapServerResponse', err);
        })
            .then(function (response) {
            Modal.deactivate();
            DataBus.emit('yapServerResponse', response);
            return true;
        });
    };
    return EventSingleCtrl;
})();
/**
 * Builds the google maps address window for this event
 */
EventSingleCtrl.prototype.buildWindow = _.debounce(function () {
    if (this._$scope.$root.size.xs) {
        return;
    }
    var context = this, instance = this._Maps.getInstance();
    if (this._infoWindow && this._infoWindow.close) {
        this._infoWindow.close();
    }
    this._Activities.map.buildWindow(this._Events.map.formatWindow.call(this._Events, this.Datum, { showDirections: true }))
        .then(function (_infoWindow) {
        context._infoWindow = _infoWindow;
        context._infoWindow.open(instance.rootObject());
    });
}, 100);
EventSingleCtrl.$inject = [
    '$scope',
    '$window',
    '$stateParams',
    '$sce',
    'DataBus',
    'Util',
    'Events',
    'eventsMethods',
    'Activities',
    'Maps',
    'modalConfirm',
    'locale',
    'Meta',
    'User',
    'ModalService',
    'Datum',
];
require('../../app.js').controller('EventSingleCtrl', EventSingleCtrl);
//# sourceMappingURL=Ctrl.js.map