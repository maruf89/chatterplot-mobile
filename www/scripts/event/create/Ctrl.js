///// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
//
//'use strict';
//
///**
// * TODO:    -    Fix loading in an event which has end date/times set
// */
//var EventCreateCtrl, _autosaveInterval, _baseState, _extractUntilDate,
//    _formatEventObject, _getLanguages, _parseEditObject;
//
//_autosaveInterval = 33 * 1000;
//
//_baseState = 'map.dashboard.eventCreate';
//
//
//// Return 0 to represent all languages
//
//_getLanguages = function (vars) {
//    if (vars.allLanguages) {
//        return [0];
//    } else {
//        return _.filter(_.pluck(vars.languages, 'languageID'), _.identity);
//    }
//};
//
//_parseEditObject = function (Event) {
//    var date = this.Config.format.date.toObj('basic_date_time_no_millis', Event.when, false).toDate(),
//        endDate = Event.until ? this.Config.format.date.toObj('basic_date_time_no_millis', Event.until, false).toDate() : null,
//        skillLength = Event.levels.length;
//
//    return this.Util.language.expand(null, Event.languages)
//        .then(function (languages) {
//            return {
//                vars: {
//                    eventAsListing: Event.indexType === 'listing',
//                    regRequired: !!Event.registrationURL,
//                    event: {
//                        date: date,
//                        time: date,
//                        endDate: endDate,
//                        endTime: endDate
//                    },
//                    rsvpOnlyRadius: !!Event.venue.rsvpOnlyRadius,
//                    showDateEnd: !!endDate,
//                    location: _.clone(Event.venue),
//                    locationClone: _.clone(Event.venue),
//                    languages: languages,
//                    allLanguages: Event.languages.length && Event.languages[
//                        0] === 0,
//                    skill: {
//                        from: {
//                            level: Event.levels[0]
//                        },
//                        to: {
//                            level: Event.levels[skillLength - 1]
//                        }
//                    },
//                    allSkills: skillLength === 6,
//                    priceFree: !Event.priceString,
//                    validSteps: [1, 1, 1],
//                    maxGoing: !!Event.maxGoing
//                },
//                newEvent: _.cloneDeep(Event)
//            };
//        });
//};
//
//_extractUntilDate = function (self, startTime) {
//    if (!self.vars.showDateEnd || (!self.vars.event.endDate && !self.vars.event.endTime)) {
//        return null;
//    }
//
//    var date = self.vars.event.endDate || new Date(),
//        untilTime = self.vars.event.endTime ? self.vars.event.endTime.split('|') : null;
//
//    // The date is automatically +1 than the end date, so if both are set then we have everything
//    if (self.vars.event.endDate && untilTime) {
//        return self.Config.format.date.toString('basic_date_time_no_millis', [
//            self.vars.event.endDate, untilTime
//        ]);
//    }
//
//    // Check if the end time < start time, if so than increment the date by +1
//    if (untilTime) {
//        if (+untilTime[0] < +startTime[0] || (+untilTime[0] === +startTime[0] &&
//                +untilTime[1] <= +startTime[1])) {
//            date.setDate(self.vars.event.date.getDate() + 1);
//        } else {
//            date.setDate(self.vars.event.date.getDate());
//        }
//    }
//
//    return self.Config.format.date.toString('basic_date_time_no_millis', [date, untilTime]);
//};
//
//_formatEventObject = function () {
//    var Event = this.newEvent,
//
//        // Only trigger a venue update if it changed
//        promise = this.vars.locationChange ? this.Venues.save([this.vars.location])
//                                                :
//                                             this.$q.when([this.EditEvent.venue]);
//
//    return promise.then(function (locArray) {
//        // set the venue id to the location
//        var ref, results,
//            loc = locArray[0];
//
//        Event.indexType = this.vars.eventAsListing ? 'listing' : 'event';
//        Event.venue.vID = loc.vID;
//        Event.venue.coords = this.vars.location.coords;
//
//        var venue = loc.vID ? loc : loc[Event.venue.vID],
//            time = this.vars.event.time.split('|');
//
//        Event.when = this.Config.format.date.toString(
//            'basic_date_time_no_millis',
//            [this.vars.event.date, time, venue.tz]
//        );
//
//        Event.until = _extractUntilDate(this, time);
//        Event.languages = _getLanguages(this.vars);
//        Event.levels = (function () {
//            results = [];
//            for (var j = ref = this.vars.skill.from.level, ref1 =
//                    this.vars.skill.to.level; ref <= ref1 ? j <=
//                ref1 : j >= ref1; ref <= ref1 ? j++ : j--) {
//                results.push(j);
//            }
//            return results;
//        }).call(this);
//
//        if (!this.vars.regRequired && Event.registrationURL) {
//            Event.registrationURL = null;
//            try {
//                delete Event.registrationURL;
//            } catch (e) {}
//        }
//        if (this.vars.eventAsListing || !this.vars.maxGoing) {
//            Event.maxGoing = null;
//            try {
//                delete Event.maxGoing;
//            } catch (e) {}
//        }
//        if (this.vars.eventAsListing) {
//            Event.aboutHost = null;
//            try {
//                delete Event.aboutHost;
//            } catch (e) {}
//        }
//        if (!Event.paymentInfo && Event.paymentInfo) {
//            Event.paymentInfo = null;
//            try {
//                delete Event.paymentInfo;
//            } catch (e) {}
//        }
//        if (!this.vars.rsvpOnlyRadius) {
//            Event.venue.rsvpOnlyRadius = null;
//        }
//        return Event;
//    }.bind(this));
//};
//
//EventCreateCtrl = function () {
//    var stateChangeListener,
//        self = this;
//
//    _.each(arguments, function (val, index) {
//        this[EventCreateCtrl.$inject[index]] = val;
//    }, this);
//
//    // Fields that go right into the final event as is
//    this.newEvent = {
//        name: null,
//        locationDisplayName: null,
//        description: null,
//        aboutHost: this.User.data.personal,
//        paymentInfo: null,
//        venue: {
//            vID: null,
//            coords: {}
//        }
//    };
//
//    // Represents variables that should be stored and hold fields
//
//    // that need to be formatted to an event object
//    this.vars = {
//        eventAsListing: null,
//        regRequired: null,
//        event: {
//            date: null,
//            datePlus1: null,
//            time: null
//        },
//        showDateEnd: null,
//        location: null,
//        languages: [],
//        allLanguages: false,
//        allSkills: true,
//        skill: {
//            from: {
//                level: 1
//            },
//            to: {
//                level: 6
//            }
//        },
//        currentStep: null,
//        validSteps: [],
//        locationChange: null,
//        priceFree: null,
//        disableVenue: false,
//        rsvpOnlyRadius: null
//    };
//
//    // Represents temporary variables that should not be saved
//
//    // to local storage
//    this.$scope.misc = {
//        currentForm: null,
//        edit: !!this.EditEvent,
//        title: this.EditEvent ? 'events.EDIT' : 'events.CREATE_EVENT',
//        hideContent: false,
//        showDelete: null,
//        showUpdate: null,
//        processing: null,
//        updateLocField: 0
//    };
//    this.steps = [{
//        route: 'basicInfo',
//        title: 'events.BASIC_INFO'
//    }, {
//        route: 'languages',
//        title: 'events.LANGUAGES'
//    }, {
//        route: 'details',
//        title: 'common.DETAILS'
//    }];
//    this.mapSet = {};
//
//    // A new map markers layer
//    this.Maps.getInstance()
//        .selectSet('createEvent', this.mapSet);
//    this._loadCurrent()["finally"](function () {
//        self.currentState();
//
//        // If the end date is added, we need to be sure to add the end date to start at +1 the start
//        self.$scope.$watch('Create.vars.event.date', function (post, prev) {
//            if ((self.vars.showDateEnd && post !== prev) || !
//                self.vars.event.datePlus1) {
//                return self._setDatePlus1();
//            }
//        });
//        self.$scope.$watch('Create.vars.showDateEnd', function (post) {
//            if (!post) {
//                return self.vars.event.endDate = self.vars.event
//                    .endTime = null;
//            }
//        });
//        return self._autoSave();
//    });
//    stateChangeListener = this.$scope.$root.$on('$stateChangeStart', this._onStateChange
//        .bind(this));
//    this.$scope.$on('$destroy', function () {
//        var instance = self.Maps.getInstance();
//
//        stateChangeListener();
//
//        if (instance) {
//            instance.deselectSet();
//        }
//
//        if (self.autoSaveTimer) {
//            return self.$interval.cancel(self.autoSaveTimer);
//        }
//    });
//    return this;
//};
//
//EventCreateCtrl.prototype = _.extend(EventCreateCtrl.prototype, {
//
//    // Sets the date to +1 so that we do not end up with end dates before start dates
//    _setDatePlus1: function () {
//        var start;
//        if (!((start = this.vars.event.date) instanceof Date)) {
//            return false;
//        }
//        this.vars.event.datePlus1 = new Date();
//        return this.vars.event.datePlus1.setDate(this.vars.event.date.getDate() +
//            1);
//    },
//
//    /**
//     * Called an route change start - If leaving the create event flow without hitting cancel
//     * it will delete any saved data
//     *
//     * @param  {object} event  not needed
//     * @param  {object} to     to state
//     */
//    _onStateChange: function (event, to) {
//        if (!/create\.[a-zA-Z]+$/.test(to.name)) {
//            return this._clearEvent();
//        }
//    },
//
//    /**
//     * Loads any persistent event data to prefill the forms
//     *
//     * @return {Promise}
//     */
//    _loadCurrent: function () {
//        var deferred, event, promise, self;
//        self = this;
//        deferred = this.$q.defer();
//        promise = null;
//        event = this.localStorageService.get('event.create');
//
//        // If we have an edit event
//        if (this.EditEvent) {
//            if (event && event.newEvent.eID === this.EditEvent.eID) {
//
//                // If we were in the middle of editing an event load local storage
//                promise = this.$q.when(event);
//            } else {
//
//                // Otherwise parse the event into something we can use
//                promise = _parseEditObject.call(this, this.EditEvent);
//            }
//        } else if (event) {
//            promise = this.$q.when(event);
//        } else {
//            return this.$q.reject();
//        }
//        promise.then(function (saved) {
//            var instance, vars;
//            self.newEvent = saved.newEvent;
//            vars = self.vars = _.merge(self.vars, saved.vars);
//            if (vars.event.date) {
//                vars.event.date = new Date(vars.event.date);
//                self.$scope.$root.safeApply(function () {
//                    return self._setDatePlus1();
//                });
//            }
//            if (vars.event.endDate) {
//                vars.event.endDate = new Date(vars.event.endDate);
//            }
//            if (vars.location && (instance = self.Maps.getInstance())) {
//                instance.createMarker(vars.location.coords, null, true).center(17);
//            }
//            if (vars.rsvpOnlyRadius) {
//                self.triggerRadius();
//            }
//            return deferred.resolve();
//        });
//        return deferred.promise;
//    },
//
//    /**
//     * Gets the current state index based on url
//     */
//    currentState: function () {
//
//        // Go to first slide if we landed on the parent
//        var curState, index, state, step;
//        if ((curState = this.$state.current.name) === _baseState) {
//            curState = _baseState + ".basicInfo";
//            this.$state.transitionTo(curState, null, {
//                location: true,
//                inherit: true,
//                notify: true
//            });
//        }
//        step = _.isArray(state = curState.match(/[^.]+$/)) ? state[0] :
//            '';
//        index = null;
//
//        // Get the index by matching it against the available steps
//        _.each(this.steps, function (_step, _index) {
//            if (step === _step.route) {
//                index = _index;
//                return false;
//            }
//        });
//        if (this.disableStepAccessible(index)) {
//            index = this.getNextAvailStep();
//            this.$state.go(_baseState + "." + this.steps[index].route);
//        }
//        this.vars.currentStep = index;
//        this.steps[index].active = this.steps[index].accessible = true;
//        return this.updateAccessibleSteps();
//    },
//
//    setStepForm: function (Form) {
//        var self;
//        self = this;
//        this.$scope.misc.currentForm = Form;
//
//        // unlisten to any previous watches
//        if (this.$scope.misc.formListener) {
//            this.$scope.misc.formListener();
//            this.$scope.misc.formListener = null;
//        }
//        return this.$scope.misc.formListener = this.$scope.$watch(
//            'misc.currentForm.$valid',
//            function (post, prev) {
//                if (post !== prev) {
//                    return self.$scope.$root.safeApply(function () {
//                        var nextStep;
//                        self.updateCurrentValidity();
//
//                        // Make next step appear as accessible
//                        if (nextStep = self.steps[self.vars.currentStep +
//                                1]) {
//                            return nextStep.accessible =
//                                post;
//                        }
//                    });
//                }
//            });
//    },
//
//    /**
//     * Updates the classes of the steps that are accessible
//     */
//    updateAccessibleSteps: function () {
//        var i, j, ref,
//            results = [];
//
//        for (i = j = 0, ref = this.steps.length - 1; 0 <= ref ? j <= ref :
//            j >= ref; i = 0 <= ref ? ++j : --j
//        ) {
//            this.steps[i].accessible = true;
//            if (!this.vars.validSteps[i]) {
//                break;
//            } else {
//                results.push(void 0);
//            }
//        }
//        return results;
//    },
//
//    /**
//     * Returns the next available step
//     *
//     * @return {number}  index of the step
//     */
//    getNextAvailStep: function () {
//        var i, j, ref;
//        for (i = j = 0, ref = this.steps.length - 1; 0 <= ref ? j <= ref :
//            j >= ref; i = 0 <= ref ? ++j : --j) {
//            if (!this.vars.validSteps[i]) {
//                return i;
//            }
//        }
//    },
//
//    /**
//     * Whether to restrict going forward to the next step or not
//     *
//     * If update is passed, will also function as a "Go To Slide" method
//     *
//     * @param  {number}  index   The index we're trying to get to
//     * @param  {Boolean} update  Whether to go to the next state also
//     * @return {Boolean}         A truthy value forbids progressing
//     */
//    disableStepAccessible: function (index, update) {
//
//        // Update current step if Form
//        var i, j, ref;
//        this.updateCurrentValidity();
//
//        // Disallow if any of the steps before our desired step are invalid
//        if (index) {
//            for (i = j = 0, ref = index - 1; 0 <= ref ? j <= ref : j >=
//                ref; i = 0 <= ref ? ++j : --j) {
//                if (!this.vars.validSteps[i]) {
//                    return true;
//                }
//            }
//        }
//        if (update) {
//            this._autoSave();
//            this.vars.currentStep = index;
//            this._storeEvent();
//            this.updateAccessibleSteps();
//        }
//        return false;
//    },
//
//    /**
//     * Inverts disableStep accessible
//     *
//     * @param  {number}  index   The index we're trying to get to
//     * @return {Boolean}         If progressing
//     */
//    goTo: function (index) {
//        if (!this.disableStepAccessible(index, true)) {
//            this.$state.go('^.' + this.steps[index].route);
//            return true;
//        }
//        return false;
//    },
//    updateCurrentValidity: function () {
//        return this.$scope.misc.currentForm && (this.vars.validSteps[
//                this.vars.currentStep] = this.$scope.misc.currentForm
//            .$valid);
//    },
//
//    /**
//     * Proceed to next step in the create event process
//     */
//    "continue": function () {
//        if (!this.disableStepAccessible(this.vars.currentStep + 1, true)) {
//            return this.$state.go('^.' + this.steps[this.vars.currentStep]
//                .route);
//        }
//    },
//
//    /**
//     * GoogleLocation directive on select callback
//     *
//     * @param  {object} location location data
//     * @param  {string} service  only service now is 'google'
//     */
//    locationSelect: function (location, service, ignoreMarker) {
//        this.vars.locationChange = true;
//        this.vars.location = this.Venues.scrapePlace(location, service);
//        this.vars.disableVenue = false;
//
//        if (!ignoreMarker) {
//            return this.setMarker(this.vars.location.coords);
//        }
//    },
//
//    setMarker: function (coords) {
//        var instance = this.Maps.getInstance();
//        if (instance) {
//            return this.$scope.safeApply(function () {
//                return instance.createMarker(coords, 'createEvent', true).center(15);
//            });
//        }
//    },
//
//    triggerRadius: function () {
//        var coords, curCenter, defaultRadius, location,
//            self = this;
//
//        defaultRadius = 500;
//        location = this.vars.location;
//        coords = location.coords;
//        this.setMarker(coords, this.vars.rsvpOnlyRadius);
//
//        if (!this.vars.rsvpOnlyRadius) {
//            this.mapSet.circle = null;
//            this.mapSet.markers = this.mapSet._markers;
//            this.mapSet._markers = null;
//            return;
//        }
//
//        curCenter = (coords.lat.toFixed(6)) + "," + (coords.lon.toFixed(6));
//        this.newEvent.venue.rsvpOnlyRadius = this.newEvent.venue.rsvpOnlyRadius || defaultRadius;
//
//        self.mapSet.circle = _.extend({
//            center: self.Maps.normalizeCoords(location.coords),
//            radius: self.newEvent.venue.rsvpOnlyRadius,
//            events: {
//                radius_changed: _.debounce(function (vars) {
//                    var latlng;
//                    if ((latlng = vars.getCenter()
//                            .toUrlValue()) !== curCenter) {
//                        self.vars.disableVenue = true;
//                        self.Util.geo.latlng2Address(
//                                latlng)
//                            .then(function (places) {
//                                if (!places[0]) {
//                                    return;
//                                }
//                                ++self.$scope.misc.updateLocField;
//                                self.locationSelect(
//                                    places[0],
//                                    'google',
//                                    true);
//
//                                // Update the coords of the hidden marker
//                                return self.mapSet._markers[
//                                        0].coords =
//                                    self.Maps.normalizeCoords(
//                                        self.vars.location
//                                        .coords);
//                            });
//                    }
//                    return self.newEvent.venue.rsvpOnlyRadius =
//                        vars.getRadius();
//                }, 100)
//            }
//        }, self.Maps.circle.editable);
//
//        self.mapSet._markers = self.mapSet.markers;
//        self.mapSet.markers = [];
//
//        return this.Maps.getInstance().updateScope();
//    },
//
//    /**
//     * Watches the skill levels and updates the checkbox based on the expr
//     */
//    onSkillChange: function () {
//        var skill;
//        skill = this.vars.skill;
//        return this.vars.allSkills = skill.from.level === 1 && skill.to.level ===
//            6;
//    },
//
//    /**
//     * Clicking on the all skills checkbox updates the skill level directives
//     */
//    toggleAllSkills: function () {
//        var skill;
//        if (this.vars.allSkills) {
//            skill = this.vars.skill;
//            skill.from.level = 1;
//            return skill.to.level = 6;
//        }
//    },
//    cancelEvent: function () {
//        this._clearEvent();
//        this.$state.go(this.Config.routes.myActivities, null, {
//            inherit: false
//        });
//        return null;
//    },
//
//    /**
//     * Stores the current event data in local storage
//     */
//    _storeEvent: function () {
//        return this.localStorageService.set('event.create', {
//            newEvent: this.newEvent,
//            vars: this.vars
//        });
//    },
//    _clearEvent: function () {
//        return this.localStorageService.remove('event.create');
//    },
//
//    /**
//     * Elasticsearch doesn't instantly index new documents, so we need to store it
//     * temporarily in memory
//     *
//     * TODO: build in  an expiration that automatically deletes expired data
//     *
//     * @param  {object} event  event data to store
//     * @param  {number} eID    eventID
//     */
//    _tempEvent: function (event, venue, eID) {
//
//        // set just in case it's not there
//        event.eID = eID;
//        this.localStorageService.set('event.indexing.eID', eID);
//        this.localStorageService.set('event.indexing.data', event);
//        this.localStorageService.set('event.indexing.venue', venue);
//        return this.localStorageService.set('event.indexing.expire', (
//                new Date())
//            .getTime() + 60 * 5 * 1000);
//    },
//
//    /**
//     * Initiates the autosave.
//     * Subsequent calls reset the interval
//     */
//    _autoSave: function () {
//        var self;
//        self = this;
//        if (this.autoSaveTimer) {
//            this.$interval.cancel(this.autoSaveTimer);
//        }
//
//        // Set autosave
//        return this.autoSaveTimer = this.$interval(function () {
//            return self._storeEvent();
//        }, _autosaveInterval);
//    },
//
//    /**
//     * Prepares the event data for the server
//     */
//    createEvent: function () {
//        var self = this,
//            newEvent = null;
//
//        this.$scope.misc.processing = true;
//        this.DataBus.emit('progressLoader', { start: true });
//        return _formatEventObject.call(this)
//            .then(function (_newEvent) {
//                newEvent = _newEvent;
//                newEvent.created = self.Config.format.date.toString('basic_date_time_no_millis');
//                return self.Events.create(_newEvent);
//            })
//            .then(function (eID) {
//                return self.onFinish(newEvent, eID);
//            })["catch"](function (err) {
//                self.$scope.misc.processing = false;
//                return self.DataBus.emit('serverResponse', err);
//            });
//    },
//
//    /**
//     * Formats the event for updating. If there are changed fields
//     * will follow up with a `Do you want to notify the attendees` screen
//     */
//    updateEvent: function () {
//        var eID, eventUpdates, self;
//        self = this;
//        eID = this.EditEvent.eID;
//        eventUpdates = null;
//        return _formatEventObject.call(this)
//            .then(function (newEvent) {
//                eventUpdates = self.Util.objectDifferences(self.EditEvent,
//                    newEvent);
//
//                // If there's nothing to update then same as cancel
//                if (!_.keys(eventUpdates, newEvent)
//                    .length) {
//                    throw self.cancelEvent();
//                }
//                eventUpdates.updated = self.Config.format.date.toString(
//                    'basic_date_time_no_millis');
//                return self.updateMessage({
//                    eID: eID,
//                    updates: eventUpdates
//                }, newEvent);
//            });
//    },
//
//    /**
//     * Screen that a user sees upon updating an event that gives him the
//     * option to notify users with an optional message
//     *
//     * @param  {object} updateObj - object ready for insertion as is
//     * @return {Promise}
//     */
//    updateMessage: function (updateObj, newEvent) {
//        var self;
//        self = this;
//        this.$scope.misc.hideContent = this.$scope.misc.showNotify =
//            true;
//        return this.updateAction = function (notify) {
//            self.$scope.misc.processing = true;
//            self.DataBus.emit('progressLoader', {
//                start: true
//            });
//            updateObj.type = self.vars.eventAsListing ? 'listing' :
//                'event';
//            if (notify) {
//                updateObj.notify = true;
//                if (self.$scope.misc.notifyMessage) {
//                    updateObj.notifyMessage = self.$scope.misc.notifyMessage;
//                }
//            }
//            return self.Events.update(updateObj)
//                .then(function () {
//                    return self.onFinish(newEvent, updateObj.eID);
//                }, function (err) {
//                    if (err) {
//                        return self.DataBus.emit(
//                            'serverResponse', err);
//                    }
//                })["finally"](function () {
//                    self.updateAction = $.noop;
//                    return self.$scope.misc.processing = false;
//                });
//        };
//    },
//
//    /**
//     * @description Since we can't easily use promises with user button clicks we have to
//     * store the `resolve` callback in a closure variable that's defined in #updateMessage
//     *
//     * @type {Function}
//     */
//    updateAction: $.noop,
//
//    onFinish: function (newEvent, eID) {
//        this._clearEvent();
//        this._tempEvent(newEvent, this.vars.location, eID);
//        this.newEvent = this.vars = {};
//        this.$state.go(this.Config.routes.myActivities, {
//            eID: eID,
//            fromStore: true,
//        });
//    },
//
//    /**
//     * @description Trigger's a confirmation modal which upon confirming
//     * will trigger the event to be deleted
//     */
//    noWaitDelete: function (notify) {
//        var self = this,
//            response:any = null,
//            deleteObject:any = {
//                eID: this.newEvent.eID,
//                notify: true,
//                type: this.newEvent.indexType,
//                notifyMessage: null,
//            };
//
//        if (notify && this.$scope.misc.notifyMessage) {
//            deleteObject.notifyMessage = this.$scope.misc.notifyMessage;
//        }
//
//        return this.Events["delete"](deleteObject)
//            .then(function (_response) {
//                return response = _response;
//            })
//            .catch(function (err) {
//                if (err.status === 404) {
//                    return response = {
//                        type: "updates.ERROR-NO_RESULTS",
//                        response: 404
//                    }
//                }
//
//                return response = err;
//            })
//            .finally(function () {
//                // show notification
//                self.DataBus.emit('yapServerResponse', response);
//
//                // clear the event from localstorage
//                self._clearEvent();
//
//                // remove the event from cache
//                self.Events.deleteIndexedEvent(self.newEvent.eID);
//
//                // go to activities
//                self.$state.go(self.Config.routes.myActivities, { hide: self.newEvent.eID });
//            });
//    }
//});
//
//EventCreateCtrl.$inject = [
//    '$scope',
//    '$state',
//    'DataBus',
//    'Config',
//    'locale',
//    'Venues',
//    'Util',
//    'Events',
//    'localStorageService',
//    '$interval',
//    '$q',
//    'Maps',
//    'EditEvent',
//    'User',
//];
//
//require('../../app.js').controller('EventCreateCtrl', EventCreateCtrl);
//# sourceMappingURL=Ctrl.js.map