/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var tagsList = require('root/config/paidTags');
var PaidTagsCtrl = (function () {
    function PaidTagsCtrl(_$analytics, $state, User, Util) {
        this._$analytics = _$analytics;
        this._maxTags = 4;
        this._priceVal = tagsList.tagsPrice; // $ dollars
        this.data = {
            tags: _.map(tagsList.tags, function (val, key) {
                return _.extend({
                    selected: false,
                    key: key,
                }, val);
            }),
            price: 0,
            validated: false,
            feedbackUrl: $state.href('standalone.feedback', {
                redirect: 'dashboard.profile.user',
                action: Util.encodeActionArgs({ userID: User.userID })
            }),
        };
        // limit how many tagsh shown
        this.data.tags.length = this._maxTags;
        this._sendAnalytic = _.debounce(this.__sendAnalytic.bind(this), 301);
    }
    PaidTagsCtrl.prototype.valClicked = function (obj) {
        this.valChanged = true;
        this._sendAnalytic(obj);
    };
    PaidTagsCtrl.prototype.__sendAnalytic = function (obj) {
        var addRemove = obj.selected ? 'add' : 'remove';
        this._$analytics.eventTrack('click', {
            category: 'acquisition',
            label: "validation: tags: " + obj.key + ": " + addRemove,
        });
    };
    PaidTagsCtrl.prototype._calculateVal = function (priceVal) {
        return _.reduce(this.data.tags, function (number, obj) {
            if (obj.selected) {
                number += priceVal || p(obj.value);
            }
            return number;
        }, 0);
    };
    PaidTagsCtrl.prototype.toPrice = function () {
        if (this.valChanged) {
            this.data.price = this._calculateVal(this._priceVal);
            this.hasSelected = !!this.data.price;
        }
        return this.data.price.toFixed(2);
    };
    PaidTagsCtrl.prototype.validate = function () {
        if (!this.data.price) {
            return;
        }
        this.data.validated = true;
        this._$analytics.eventTrack('click', {
            category: 'acquisition',
            label: 'validation: tags: PayPal',
            value: this._calculateVal(),
        });
    };
    return PaidTagsCtrl;
})();
PaidTagsCtrl.$inject = ['$analytics', '$state', 'User', 'Util'];
require('../../../app').controller('PaidTagsCtrl', PaidTagsCtrl);
//# sourceMappingURL=Ctrl.js.map