/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var _userEdits = {
        fullName: function (fName:string, self:any):{firstName:string; lastName?:string} {
            var fullName:string,
                parts:string[];

            self.user.fullName = fullName = $.trim(fName);
            parts = fullName.split(' ');

            if (parts.length === 1) {
                return {
                    firstName: parts[0]
                };
            }

            return {
                lastName: parts.pop(),
                firstName: parts.join(' ')
            };
        },

        details: function (val, self) {
            return {
                details: _.merge(self._editClone.details, val)
            };
        }
    },

    _advanced = {
        onModalDeactivate: $.noop
    };

import events = require('../../../../common/front/modules/EventEmitter');

/**
 * @ngdoc factory
 * @class
 * @classdesc Creates a Profile user instance with methods to edit the user
 *
 * @param {object}  user                           the current user object
 * @param {Object=} advanced                       Hooks
 * @param advanced {Function} onModalDeactivate
 * @returns {Profile}
 */
class Profile extends events.EventEmitter implements cp.user.IProfile {
    public isSelf:boolean;
    public options:any;
    private _editClone:cp.user.IUser;
    private _advanced:any;
    private _modals:any;
    private _emitUpdate:(userID:number, updates:any) => void;
    private _formatBind:() => void;
    private _activeModal:any;

    constructor(
        private _User:any,
        private _DataBus:NodeJS.EventEmitter,
        private _$rootScope:cp.IRootScopeService,
        private _locale:any,
        private _Util:any,
        private _ProfilePhoto:any,
        private _$analytics:Angulartics.IAnalyticsService,
        private _$q:ng.IQService,
        profileEditBasic:any,
        editLanguagesModal:any,
        public user:cp.user.IUser,
        advanced?:any
    ) {
        super();

        this._advanced = typeof advanced === 'object' ? _.defaults(advanced, _advanced) : _advanced;
        this._init();

        this._modals = {
            basic: profileEditBasic,
            languages: editLanguagesModal
        };

        this._format();

        this._formatBind = this._format.bind(this);
        _DataBus.on('/site/authChange', this._formatBind);
    }

    private _init():void {
        this._locale.ready('common').then(function ():void {
            this.options = {
                genders: {
                    'MALE': this._locale.getString('common.MALE'),
                    'FEMALE': this._locale.getString('common.FEMALE'),
                    'OTHER': this._locale.getString('common.OTHER')
                },
                age: {
                    min: 18,
                    max: 90,
                    get: function () {
                        return this._User.format.birthday2Age(this.user.details.birthday);
                    }.bind(this)
                },
                saveLanguage: this._saveLanguages.bind(this)
            };
        }.bind(this));

        // Create a relay
        this._emitUpdate = function (userID, updates) {
            if (userID === this.user.userID) {
                return this.emit('/updated', updates);
            }
        }.bind(this);

        this._User.on('/updated', this._emitUpdate);
    }

    private _format():void {
        // On auth change, if we are looking at our own profile, than use the same object reference
        if (this._User.userID === this.user.userID) {
            this.user = this._User.data;
        }

        var user:cp.user.IUser = this.user;
        user.fullName = this._User.format.name(user);
        user.userID = p(user.userID);
        this._editClone = this.isSelf = null;

        // Doesn't belong here - belongs in ProfileCtrl
        this._$rootScope.pageClasses = (this.isSelf = this.isMe()) ? '' : 'hide-dash';

        user.details = user.details || {};
        this.emit('/ready', this.isSelf);
    }

    public isMe(forceCheck?:boolean):boolean {
        if (forceCheck || !this.isSelf) {
            this.isSelf = this._User.userID === this.user.userID;
        }

        return this.isSelf;
    }

    /**
     * Notifies the message directive to open a message window to contact this user
     */
    public messageUser():void {
        this._DataBus.emit('/message/user', { to: [this.user] });
    }

    public uploadPhoto(userID:number, opts:any):ng.IPromise<{picture:cp.user.IPicture}> {
        return this._ProfilePhoto.activate(userID, opts);
    }

    public editSection(section:string):void {
        var modal = this._modals[section];

        // if the modal doesn't exist
        if (!modal) {
            // Assume it's a togglable module
            return this._DataBus.emit("/profile/" + section + "/toggle", true);
        }

        this._$analytics.eventTrack('click', {
            category: 'profile',
            label: 'edit profile: edit modal ' + section
        });

        this._activeModal = modal;
        _.defer(function () {
            modal.activate({
                profile: {
                    deactivate: this._deactivateModal.bind(this),
                    save: this._saveModal.bind(this)
                },
                user: this._editClone = _.cloneDeep(this.user),
                options: this.options
            }, null, true);
        }.bind(this));
    }

    public save(updateObj:any):ng.IPromise<cp.user.IUser> {
        this._DataBus.emit('progressLoader', { start: true });

        return this._User.update(updateObj).then(function () {
            this._$analytics.eventTrack('click', {
                category: 'profile',
                label: 'edit profile: saved'
            });

            this._$rootScope.safeApply(function () {
                _.extend(this.user, updateObj);
                this._DataBus.emit('progressLoader');
            }.bind(this));

            return this.user;
        }.bind(this));
    }

    private _saveModal():ng.IPromise<cp.user.IUser> {
        var changes:any = this._Util.objectDifferences(this.user, this._editClone),
            updateObj:any = {},
            send:boolean = false;

        _.each(changes, function (val, key) {
            send = true;
            if (_userEdits[key]) {
                return _.merge(updateObj, _userEdits[key](val, this));
            } else {
                return updateObj[key] = val;
            }
        }, this);

        this._activeModal.deactivate();

        if (!send) {
            return this._$q.reject();
        }

        return this.save(updateObj);
    }

    private _deactivateModal():void {
        var deactivate:() => void = this._advanced.onModalDeactivate;

        if (typeof deactivate === 'function') {
            deactivate();
        }

        this._activeModal.deactivate();
    }

    /**
     * Triggered on Edit Languages Modal save
     *
     * @param  {object} languages  Elasticsearch compatible languages data
     * @param  {object} complete   Same as above but includes the languages display names
     */
    private _saveLanguages(languages:cp.user.ILanguage[], complete:cp.user.ILanguage[]):ng.IPromise<any> {
        this._Util.language.save2Storage(complete);
        return this.save({ languages: languages })
            .then(this._activeModal.deactivate.bind(this._activeModal));
    }

    public destroy():void {
        var modal = this._activeModal;

        this._DataBus.removeListener('/site/authChange', this._formatBind);
        this._User.removeListener('/updated', this._emitUpdate);

        if (modal && modal.deactivate) {
            modal.deactivate();
        }
    }
}

require('../app.js').factory('Profile', [
    'User',
    'DataBus',
    '$rootScope',
    'locale',
    'Util',
    'ProfilePhoto',
    '$analytics',
    '$q',
    'profileEditBasic',
    'editLanguagesModal',
    function (
        User:any,
        DataBus:any,
        $rootScope:cp.IRootScopeService,
        locale:any,
        Util:any,
        ProfilePhoto:any,
        $analytics:Angulartics.IAnalyticsService,
        $q:ng.IQService,
        profileEditBasic:any,
        editLanguagesModal:any
    ) {
        return {
            create: function (user:cp.user.IUser, advanced?:any):cp.user.IProfile {
                return new Profile(
                    User,
                    DataBus,
                    $rootScope,
                    locale,
                    Util,
                    ProfilePhoto,
                    $analytics,
                    $q,
                    profileEditBasic,
                    editLanguagesModal,
                    user,
                    advanced
                )
            }
        }
    }
]);
