/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('sectionContact', [
    '$q', '$templateCache', 'Interaction', 'User', 'Auth',
    function ($q, $templateCache, Interaction, User, Auth) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/sectionContact/Template.html'),
            scope: {
                onSave: '&',
                source: '='
            },
            link: {
                pre: function (scope) {
                    scope.chat = {
                        messageID: [User.userID, scope.source.userID].sort().join('|'),
                        msgID: false,
                        message: null,
                        processing: null,
                        send: function () {
                            var context = this;
                            if (!Auth.isAuthenticated()) {
                                // On authentication redirect here & recall this method with the same arguments
                                return Auth.isLoggedIn({
                                    redirectBack: true,
                                    message: "auth.MODAL_BAR_EVENT_ATTND"
                                }).then(function () {
                                    context.send();
                                });
                            }
                            this.processing = true;
                            return Interaction.send({
                                to: [scope.source.userID],
                                message: this.message,
                            })
                                .then(function (msgID) {
                                context.message = '';
                                context.msgID = msgID;
                            })
                                .finally(function () {
                                context.processing = false;
                                scope.$root.safeDigest(scope);
                            });
                        }
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map