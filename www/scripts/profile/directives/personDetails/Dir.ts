/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * @ngdoc directive
 * @namespace personDetails
 *
 * @requires $templateCache
 * @requires User
 * @requires Util
 */
require('../../../app').directive('personDetails', [
    '$templateCache', 'User', 'Util',
    function ($templateCache, User, Util) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/personDetails/Template.html'),
            scope: {
                user: '=',
                category: '@'
            },
            link: {
                pre: function (scope, iElem) {

                    // format the age from their birthday & build a basic location
                    var age, location,
                        ageLoc = [];

                    if (age = User.format.birthday2Age(scope.user.details && scope.user.details.birthday)) {
                        ageLoc.push(age);
                    }
                    if (location = Util.geo.format.address(scope.user, ['city', 'country'], ', ')) {
                        ageLoc.push(location);
                    }

                    // we do this manually so that we don't have to ng-bind-html which seems like overkill to achieve bullets
                    iElem.find('.display-row').html(ageLoc.join('&#32;&#149;&#32;'));
                }
            }
        };
    }
]);
