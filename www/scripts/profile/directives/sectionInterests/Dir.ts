/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var interests = require('./interests.json'),

    translated = {},

    isTranslated = false,

    maxQuestions = 5;

require('../../../app').directive('sectionInterests', [
    'localStorageService', 'locale', 'DataBus', '$analytics',
    '$templateCache', 'Util',
    function (locStorage, locale, DataBus, $analytics, $templateCache, Util) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get(
                'views/profile/directives/sectionInterests/Template.html'
            ),
            scope: {
                onSave: '&',
                source: '=',
                defaultOpen: '=?'
            },
            link: {
                pre: function (scope) {
                    var availableOptions, vars;
                    vars = scope.vars = {
                        activeEdit: scope.defaultOpen || false,
                        newQuestion: null,
                        clone: _.clone(scope.source),
                        options: translated,
                        availableOptions: {},
                        maxQuestions: false,

                        // Checks to see if the translation keys have been translated yet
                        translateFn: function () {
                            if (!isTranslated) {
                                return locale.ready(interests.source).then(function () {
                                    _.each(interests.topics, function (topic) {
                                        return translated[topic] = locale.getString(
                                            interests.source + "." +
                                            interests.prefix + topic
                                        );
                                    });

                                    isTranslated = true;
                                    return availableOptions();
                                });
                            }
                            return availableOptions();
                        }
                    };

                    // Build our available options from 'all questions' - 'already used questions'
                    availableOptions = function () {

                        // If our source isn't the right format, make it and open all available options
                        if (!_.isArray(vars.clone)) {
                            vars.clone = [];
                            return vars.availableOptions = vars.options;
                        }

                        // If we reached our question max, hide the additional options
                        if (vars.clone.length >= maxQuestions) {
                            return vars.maxQuestions = true;
                        }

                        // Show the question selector if hidden
                        vars.maxQuestions = false;
                        vars.availableOptions = {};
                        return _.each(vars.options, function (val, key) {
                            if (_.every(vars.clone, function (obj) {
                                    return obj.question !== key;
                                })
                            ) {
                                vars.availableOptions[key] =val;
                            }
                            return true;
                        });
                    };

                    // On new question change, append it to the users available questions
                    scope.$watch('vars.newQuestion', function (post, prev) {
                        if (post && prev !== post) {
                            vars.clone.push({
                                question: post,
                                answer: ''
                            });
                            vars.newQuestion = null;
                            return availableOptions();
                        }
                    });
                    return vars.translateFn();
                },
                post: function (scope, iElem) {
                    var vars = scope.vars,
                        toggleTrigger = function (toggle) {
                            var $hilite = iElem.closest('.can-hilite');
                            Util.$hilite($hilite);

                            // If toggle is boolean, will set edit to that, otherwise toggles
                            if ((vars.activeEdit = typeof toggle ===
                                                   'boolean' ? toggle : !vars.activeEdit
                                )) {
                                _.defer(function () {
                                    Util.$scrollTo(iElem, null, {
                                        padding: 30
                                    });
                                    return iElem.find('.input')
                                        .focus();
                                });
                                return scope.$root.safeDigest(scope);
                            }
                        };
                    vars.deleteRow = function (topic) {
                        return _.each(vars.clone, function (obj,
                                                            index) {
                            if (topic.question === obj.question) {
                                vars.clone.splice(index, 1);
                                vars.availableOptions[obj.question] =
                                    vars.options[obj.question];
                                return false;
                            }
                        });
                    };
                    vars.save = function () {
                        var update = {
                            interests: vars.clone
                        };
                        return scope.onSave({
                            updates: update
                        }).then(function () {
                            vars.activeEdit = false;
                            return $analytics.eventTrack(
                                'click', {
                                    category: 'profile',
                                    label: 'edit profile: interests'
                                });
                        });
                    };
                    DataBus.on('/profile/interest/toggle', toggleTrigger);
                    return scope.$on('$destroy', function () {
                        return DataBus.removeListener(
                            '/profile/interest/toggle',
                            toggleTrigger
                        );
                    });
                }
            }
        };
    }
]);
