/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var imageSizes = require('../../../config/imageSizes.json');

import {AvatarCtrl} from 'front/sections/profile/directives/avatar';

require('../../../app').directive('avatar', [
    'DataBus', '$templateCache', 'User',
    function (DataBus, $templateCache, User) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/profile/directives/avatar/Template.html'),
            scope: {
                user: '=?',
                size: '@',
                opts: '=?',
                editable: '@',
                doWatch: '@',
            },
            controller: [
                'User',
                'Config',
                '$compile',
                '$scope',
                'ProfilePhoto',
                AvatarCtrl,
            ],
            transclude: true,
            controllerAs: 'viewModel',
            link: {
                pre: function (scope, iElem, iAttrs, Avatar) {
                    Avatar.init({
                        imageSizes: imageSizes,
                        user: scope.user,
                        size: scope.size,
                        opts: scope.opts,
                        iElem: iElem,
                    });

                    var genderFn = function () {
                            var genderWatch;
                            if (!genderListener && vars.editable) {
                                genderWatch = scope.gender ? 'gender' : 'user.details.gender';
                                return genderListener = scope.$watch(
                                    genderWatch,
                                    function (post, prev) {
                                        Avatar.updateGender(post);
                                        if (!Avatar.photo[Avatar.size] && post !== prev) {
                                            Avatar.setAvatar();
                                        }
                                    });
                            }
                        },

                        onAuthChange,
                        vars,
                        genderListener = null;

                    scope.vars = vars = {
                        editable: !!scope.editable && Avatar.isSelf(),
                        showEdit: false,
                    };

                    if (scope.editable || scope.doWatch) {
                        onAuthChange = function (auth) {
                            if (!auth && genderListener) {
                                genderListener();
                            }
                            vars.editable = !!scope.editable && Avatar.isSelf();
                            genderFn();

                            if (auth && !scope.user) {
                                Avatar.setAvatar();
                            }
                        };
                        DataBus.on('/site/authChange', onAuthChange);
                        scope.$on('$destroy', function () {
                            return DataBus.removeListener('/site/authChange', onAuthChange);
                        });
                    }

                    genderFn();
                    Avatar.setAvatar();
                },

                post: function (scope, iElem, iAttrs, Avatar) {
                    var photoUpdateCheck;

                    if (scope.editable || scope.doWatch) {
                        photoUpdateCheck = function (userID, updates) {
                            if (Avatar.isSelf(userID) && 'picture' in updates) {
                                Avatar.photo = updates.picture || {};
                                Avatar.photoID = userID;
                                Avatar.setAvatar(true);
                            }
                        };

                        User.on('/updated', photoUpdateCheck);
                        scope.$on('$destroy', function () {
                            User.removeListener('/updated', photoUpdateCheck);
                        });
                    }
                }
            }
        };
    }
]);
