/// <reference path="../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var _userEdits = {
    fullName: function (fName, self) {
        var fullName, parts;
        self.user.fullName = fullName = $.trim(fName);
        parts = fullName.split(' ');
        if (parts.length === 1) {
            return {
                firstName: parts[0]
            };
        }
        return {
            lastName: parts.pop(),
            firstName: parts.join(' ')
        };
    },
    details: function (val, self) {
        return {
            details: _.merge(self._editClone.details, val)
        };
    }
}, _advanced = {
    onModalDeactivate: $.noop
};
var events = require('../../../../common/front/modules/EventEmitter');
/**
 * @ngdoc factory
 * @class
 * @classdesc Creates a Profile user instance with methods to edit the user
 *
 * @param {object}  user                           the current user object
 * @param {Object=} advanced                       Hooks
 * @param advanced {Function} onModalDeactivate
 * @returns {Profile}
 */
var Profile = (function (_super) {
    __extends(Profile, _super);
    function Profile(_User, _DataBus, _$rootScope, _locale, _Util, _ProfilePhoto, _$analytics, _$q, profileEditBasic, editLanguagesModal, user, advanced) {
        _super.call(this);
        this._User = _User;
        this._DataBus = _DataBus;
        this._$rootScope = _$rootScope;
        this._locale = _locale;
        this._Util = _Util;
        this._ProfilePhoto = _ProfilePhoto;
        this._$analytics = _$analytics;
        this._$q = _$q;
        this.user = user;
        this._advanced = typeof advanced === 'object' ? _.defaults(advanced, _advanced) : _advanced;
        this._init();
        this._modals = {
            basic: profileEditBasic,
            languages: editLanguagesModal
        };
        this._format();
        this._formatBind = this._format.bind(this);
        _DataBus.on('/site/authChange', this._formatBind);
    }
    Profile.prototype._init = function () {
        this._locale.ready('common').then(function () {
            this.options = {
                genders: {
                    'MALE': this._locale.getString('common.MALE'),
                    'FEMALE': this._locale.getString('common.FEMALE'),
                    'OTHER': this._locale.getString('common.OTHER')
                },
                age: {
                    min: 18,
                    max: 90,
                    get: function () {
                        return this._User.format.birthday2Age(this.user.details.birthday);
                    }.bind(this)
                },
                saveLanguage: this._saveLanguages.bind(this)
            };
        }.bind(this));
        // Create a relay
        this._emitUpdate = function (userID, updates) {
            if (userID === this.user.userID) {
                return this.emit('/updated', updates);
            }
        }.bind(this);
        this._User.on('/updated', this._emitUpdate);
    };
    Profile.prototype._format = function () {
        // On auth change, if we are looking at our own profile, than use the same object reference
        if (this._User.userID === this.user.userID) {
            this.user = this._User.data;
        }
        var user = this.user;
        user.fullName = this._User.format.name(user);
        user.userID = p(user.userID);
        this._editClone = this.isSelf = null;
        // Doesn't belong here - belongs in ProfileCtrl
        this._$rootScope.pageClasses = (this.isSelf = this.isMe()) ? '' : 'hide-dash';
        user.details = user.details || {};
        this.emit('/ready', this.isSelf);
    };
    Profile.prototype.isMe = function (forceCheck) {
        if (forceCheck || !this.isSelf) {
            this.isSelf = this._User.userID === this.user.userID;
        }
        return this.isSelf;
    };
    /**
     * Notifies the message directive to open a message window to contact this user
     */
    Profile.prototype.messageUser = function () {
        this._DataBus.emit('/message/user', { to: [this.user] });
    };
    Profile.prototype.uploadPhoto = function (userID, opts) {
        return this._ProfilePhoto.activate(userID, opts);
    };
    Profile.prototype.editSection = function (section) {
        var modal = this._modals[section];
        // if the modal doesn't exist
        if (!modal) {
            // Assume it's a togglable module
            return this._DataBus.emit("/profile/" + section + "/toggle", true);
        }
        this._$analytics.eventTrack('click', {
            category: 'profile',
            label: 'edit profile: edit modal ' + section
        });
        this._activeModal = modal;
        _.defer(function () {
            modal.activate({
                profile: {
                    deactivate: this._deactivateModal.bind(this),
                    save: this._saveModal.bind(this)
                },
                user: this._editClone = _.cloneDeep(this.user),
                options: this.options
            }, null, true);
        }.bind(this));
    };
    Profile.prototype.save = function (updateObj) {
        this._DataBus.emit('progressLoader', { start: true });
        return this._User.update(updateObj).then(function () {
            this._$analytics.eventTrack('click', {
                category: 'profile',
                label: 'edit profile: saved'
            });
            this._$rootScope.safeApply(function () {
                _.extend(this.user, updateObj);
                this._DataBus.emit('progressLoader');
            }.bind(this));
            return this.user;
        }.bind(this));
    };
    Profile.prototype._saveModal = function () {
        var changes = this._Util.objectDifferences(this.user, this._editClone), updateObj = {}, send = false;
        _.each(changes, function (val, key) {
            send = true;
            if (_userEdits[key]) {
                return _.merge(updateObj, _userEdits[key](val, this));
            }
            else {
                return updateObj[key] = val;
            }
        }, this);
        this._activeModal.deactivate();
        if (!send) {
            return this._$q.reject();
        }
        return this.save(updateObj);
    };
    Profile.prototype._deactivateModal = function () {
        var deactivate = this._advanced.onModalDeactivate;
        if (typeof deactivate === 'function') {
            deactivate();
        }
        this._activeModal.deactivate();
    };
    /**
     * Triggered on Edit Languages Modal save
     *
     * @param  {object} languages  Elasticsearch compatible languages data
     * @param  {object} complete   Same as above but includes the languages display names
     */
    Profile.prototype._saveLanguages = function (languages, complete) {
        this._Util.language.save2Storage(complete);
        return this.save({ languages: languages })
            .then(this._activeModal.deactivate.bind(this._activeModal));
    };
    Profile.prototype.destroy = function () {
        var modal = this._activeModal;
        this._DataBus.removeListener('/site/authChange', this._formatBind);
        this._User.removeListener('/updated', this._emitUpdate);
        if (modal && modal.deactivate) {
            modal.deactivate();
        }
    };
    return Profile;
})(events.EventEmitter);
require('../app.js').factory('Profile', [
    'User',
    'DataBus',
    '$rootScope',
    'locale',
    'Util',
    'ProfilePhoto',
    '$analytics',
    '$q',
    'profileEditBasic',
    'editLanguagesModal',
    function (User, DataBus, $rootScope, locale, Util, ProfilePhoto, $analytics, $q, profileEditBasic, editLanguagesModal) {
        return {
            create: function (user, advanced) {
                return new Profile(User, DataBus, $rootScope, locale, Util, ProfilePhoto, $analytics, $q, profileEditBasic, editLanguagesModal, user, advanced);
            }
        };
    }
]);
//# sourceMappingURL=Factory.js.map