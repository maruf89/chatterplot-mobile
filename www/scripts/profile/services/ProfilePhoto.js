/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var ProfilePhoto = function (modalConfirm, User, $q, locale) {
    this._modalConfirm = modalConfirm;
    this._User = User;
    this._$q = $q;
    this._locale = locale;
};
ProfilePhoto.prototype = _.extend(ProfilePhoto.prototype, {
    /**
     * Initiates the upload profile photo modal flow
     *
     * @param {number} userID - This is used to generate the publicID/path to the uploaded picture
     * @param {object} opts
     * @param {boolean} opts.showCTA - whether to show the orange section
     * @returns {Promise}
     */
    activate: function (userID, opts) {
        var deferred = this._$q.defer();
        if (!this._User.userID) {
            throw new Error('ProfilePhoto#activate cannot be called while the user is not authenticated');
        }
        userID = userID || this._User.userID;
        if (!CP.production) {
            userID = CP.site.prefix + "/" + userID;
        }
        //this._photoUploadFlow.activate({
        //    uploadPreset: 'profile',
        //    onSuccess: this.uploadSuccess.bind(this, deferred),
        //    userID: userID,
        //    opts: opts,
        //    modal: {
        //        deactivate: this.uploadCancel.bind(this, deferred)
        //    },
        //});
        return deferred.promise;
    },
    /**
     * Callback for when a user cancels out of the photo upload flow
     *
     * @callback
     */
    //uploadCancel: function (deferred) {
    //    this._photoUploadFlow.deactivate();
    //    return deferred.reject();
    //},
    /**
     * Upon successful upload response from the image host (Cloudinary)
     *
     * @callback
     */
    //uploadSuccess: function (deferred, uploadResponse) {
    //    var savePhotos = {
    //        hasPhoto: true,
    //        service: null,
    //        id: null
    //    };
    //
    //    this._photoUploadFlow.deactivate();
    //
    //    return this._User.update({ picture: savePhotos })
    //        .then(deferred.resolve.bind(deferred, savePhotos))
    //        ["catch"](deferred.reject);
    //},
    /**
     * Removes the profile photo of the current user
     */
    remove: function () {
        if (!this._User.userID) {
            throw new Error('ProfilePhoto#remove cannot be called while the user is not authenticated');
        }
        return this._locale.ready('common')
            .then(function () {
            var values = {
                title: this._locale.getString('common.R_U_SURE'),
                confirmText: this._locale.getString('common.REMOVE'),
                cancelClass: 'btn-white',
                confirmClass: 'btn-red'
            };
            return this._modalConfirm.activate(values);
        }.bind(this))
            .then(function (close) {
            return this._User.update({ picture: null })["finally"](close);
        }.bind(this));
    }
});
ProfilePhoto.$inject = [
    'modalConfirm',
    'User',
    '$q',
    'locale',
];
require('../../app').service('ProfilePhoto', ProfilePhoto);
//# sourceMappingURL=ProfilePhoto.js.map