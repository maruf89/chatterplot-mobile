/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * @description checks whether the current user has any (non-temporary) locations set
 * @param {(null|array)} locs
 * @returns {boolean}
 * @private
 */
var _hasLocationsCheck = function (locs) {
        var length;
        return _.isArray(locs) && (length = locs.length) && (length > 1 || locs[0].type !== 'temp');
    },

    /**
     * The name given to the map stack layer
     * @type {string}
     */
    setName = 'user';

/**
 * @ngdoc controller
 * @class
 * @classdesc Handles view only as well and supports edit profile capabilities through the {@link Profile} factory
 */
class ProfileCtrl implements cp.user.IProfileCtrl {
    private _stateAction:any = {};
    private _user:cp.user.IUser;
    private _mapSet:any;

    public profile:cp.user.IProfile;
    public mapInstance:cp.map.IMapCtrl;
    public meta:any;

    /**
     * @deprecated
     */
    public vars:any;
    public data:any;

    constructor(
        private _$scope:cp.user.IProfileCtrlScope,
        private _Util:any,
        private _Maps:any,
        private _Meta:any,
        private _$stateParams:ng.ui.IStateParamsService,
        private _User:any,
        private _$analytics:Angulartics.IAnalyticsService,
        private _Modal:cp.modal.IService,
        CurUser:cp.user.IUser,
        Auth:any,
        Profile:cp.user.IProfileFactory,
        DataBus:NodeJS.EventEmitter,
        $ionicScrollDelegate:any
) {
        _$scope.profile = this.profile = Profile.create(CurUser);

        $ionicScrollDelegate.scrollTop(false);

        this._user = _$scope.user = this.profile.user;

        this._initData();

        this.profile.on('/updated', this._onProfileUpdate.bind(this));

        var toggleUserProfile = this._toggleProfChecks.bind(this);
        Auth.on('/authChange', toggleUserProfile);

        _$scope.$on('$destroy', function (instance) {
            DataBus.emit('/nav/subfooter/change');

            this.profile.destroy();
            Auth.removeListener('/authChange', toggleUserProfile);

            if (instance = this.mapInstance) {
                return instance.deselectSet();
            }
        }.bind(this));
    }

    private _initData():void {
        var _action:string = this._$stateParams['action'];

        this._mapSet = {};

        this.mapInstance = this._Maps.getInstance();
        this.mapInstance.selectSet(setName, this._mapSet);
        this.mapInstance.clearMarkers();

        // Save any state param actions
        if (_action) {
            this._stateAction = this._Util.parseActionArgs(_action);
        }

        this.vars = this.data = {
            parts: {
                personal: !!this._user.personal,
                interests: _.isArray(this._user.interests) && this._user.interests.length,
                locations: _hasLocationsCheck(this._user.locations),
                availability: false,
                contact: !!this._user.settings.notifs.messaging.acceptAll
            },
            userPicture: this._User.format.photoSrc(this._user, 'default'),
        };

        this._toggleProfChecks();

        this._setTempLoc();

        this.meta = {
            name: this._user.fullName,
            description: this._user.personal,
            canonical: this._Meta.canonical(),
            picture: this.vars.userPicture,
        };

        this._$scope.$root.mustBeLoadedIn(2500);
    }

    /**
     * @description checks whether the profile completion should be loaded for a user
     * @returns {*}
     * @private
     */
    private _toggleProfChecks():void {
        var isMe:boolean = this.profile.isMe();

        if (isMe &&
            this._User.userID
        ) {
            if (this._stateAction.firstTime) {
                this._firstTimeLoad();
            }
        }
    }

    private _firstTimeLoad():void {
        if (CP.releases.indexOf('paidTags') !== -1) {
            this._Modal.activate({
                templateUrl: 'views/profile/modals/paidTags/Template.html',
                'class': 'paid-tag-cont',
            });
        }
    }

    private _onProfileUpdate(updates) {
        if ('picture' in updates) {
            this.data.userPicture = !!updates.picture && this._User.format.photoSrc(this._user, 'default');
            this._$scope.$root.safeDigest(this._$scope);
        }
    }

    public editSection(event:any):void {
        var section = event.target.getAttribute('data-section');

        if (!section) {
            return;
        }

        this.profile.editSection(section);
    }

    private _setTempLoc():void {
        var temp,
            instance:cp.map.IMapCtrl = this.mapInstance,
            curSet:cp.map.IMarkerSet,
            coords,
            defaultCircle;

        // Check that we have a temporary location
        if (!_.isArray(this._user.locations) ||
            !this._user.locations.length ||
            (temp = this._user.locations[0]).type !== 'temp'
        ) {
            return;
        }

        curSet = instance.getSet(setName);
        coords = this._Maps.normalizeCoords(temp.coords);
        defaultCircle = this._Maps.circle.nonEditable;

        instance.loaded().then(function () {
            curSet.circle = _.extend({
                center: coords,
                radius: 5000
            }, defaultCircle);

            _.defer(instance.centerMap.bind(instance, coords, 12));
        });
    }
}

ProfileCtrl.$inject = ['$scope',
    'Util',
    'Maps',
    'Meta',
    '$stateParams',
    'User',
    '$analytics',
    'ModalService',
    'CurUser',
    'Auth',
    'Profile',
    'DataBus',
    '$ionicScrollDelegate',
];

require('../app').controller('ProfileCtrl', ProfileCtrl);
