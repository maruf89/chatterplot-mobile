/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Ctrl_1 = require('front/sections/search/Ctrl');
Ctrl_1.setDefaultSize(5, 8);
Ctrl_1.setMarkerSize(20);
var SearchCtrl = (function (_super) {
    __extends(SearchCtrl, _super);
    function SearchCtrl($scope, Util, DataBus, Activities, Maps, $state) {
        _super.call(this, $scope, Util, DataBus, Activities, Maps, $state);
        var pageClass = 'page-results';
        $scope.$root.addPageClass(pageClass);
        $scope.$on('$destroy', function () {
            $scope.$root.removePageClass(pageClass);
        });
    }
    return SearchCtrl;
})(Ctrl_1.ActivitySearchCtrl);
exports.SearchCtrl = SearchCtrl;
require('../../app').controller('ActivitySearchCtrl', [
    '$scope',
    'Util',
    'DataBus',
    'Activities',
    'Maps',
    '$state',
    SearchCtrl,
]);
//# sourceMappingURL=Ctrl.js.map