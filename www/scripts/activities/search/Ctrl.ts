/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {setDefaultSize, setMarkerSize, ActivitySearchCtrl} from 'front/sections/search/Ctrl';

setDefaultSize(5, 8);
setMarkerSize(20);

export class SearchCtrl extends ActivitySearchCtrl {
    constructor(
        $scope:cp.IScope,
        Util:any,
        DataBus:NodeJS.EventEmitter,
        Activities:any,
        Maps:any,
        $state:ng.ui.IStateService
    ) {
        super($scope, Util, DataBus, Activities, Maps, $state);

        var pageClass = 'page-results';

        $scope.$root.addPageClass(pageClass);

        $scope.$on('$destroy', function () {
            $scope.$root.removePageClass(pageClass);
        });
    }
}

require('../../app').controller('ActivitySearchCtrl', [
    '$scope',
    'Util',
    'DataBus',
    'Activities',
    'Maps',
    '$state',
    SearchCtrl,
]);
