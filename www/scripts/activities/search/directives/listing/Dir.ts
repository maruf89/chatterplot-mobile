/// <reference path="../../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../../app').directive('activityListing', [
    'Maps',
    '$templateCache',
    require('root/search/directives/listing')
]);
