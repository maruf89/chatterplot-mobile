/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />
/// <reference path="../../../../typings/node/node.d.ts" />

'use strict';

var _classKey, _closeMarkerWindows, _openMarkerWindow,
    markerWindows;

import events = require('common/front/modules/EventEmitter');

/**
 * For each class of activity we match the
 * activity class to it's identifier
 *
 * @type {object}
 */
_classKey = {
    'event': {
        _id: 'eID',
        location: function (event) {
            return (event._source || event).venue;
        },
        service: 'Events',
        disableHide: 'amIAttending',
        icon: 'xs',
        window: true, // whether to show a gmap window
        events: {
            click: function (marker, Map) {
                Map.getInstance().centerMap(marker.position, marker.map.getZoom() + 1);
            }
        }
    },
    'people': {
        _id: 'userID',
        location: function (user) {
            var locations = (user._source || user).locations;
            return locations && locations[0];
        },
        service: 'People',
        icon: 'xs',
        window: false,
        events: {
            click: null
        }
    },
    'request': {
        _id: 'reqID',
        location: function (request) {
            return (request._source || request).venue;
        },
        service: 'Interaction',
        icon: 'xs',
        window: true,
        events: {
            click: function (marker, Map) {
                Map.getInstance().centerMap(marker.position, marker.map.getZoom() + 1);
            }
        }
    }
};

markerWindows = [];

_closeMarkerWindows = function () {
    return _.each(markerWindows, function (infoWindow:any) {
        infoWindow.close();
        infoWindow.$content.removeClass('appear');
        return infoWindow.triggerClose = false;
    });
};

_openMarkerWindow = function (infoWindow:any, map:any, marker:any) {
    _closeMarkerWindows();
    infoWindow.$content.removeClass('appear');
    infoWindow.open(map, marker);
    markerWindows.push(infoWindow);

    _.defer(function () {
        infoWindow.$content.addClass('appear');
    });
};


/**
 * @class
 * @classdesc handles all overlapping functionality for Events, Tandems and Teachers
 */
class Activities extends events.EventEmitter implements cp.activities.IService {
    public map:ActivityMap;

    constructor(
        private Events:any,
        private Tandems:any,
        private Interaction:cp.interaction.IService,
        private _$state:ng.ui.IStateService,
        private _$rootScope:cp.IRootScopeService,
        private _Config:any,
        private _Util:any,
        private _DataBus:NodeJS.EventEmitter,
        private _Maps:any,
        private _$templateCache:ng.ITemplateCacheService,
        private _$compile:ng.ICompileService,
        private _$q:ng.IQService
    ) {
        super();
        _DataBus.on('/site/authChange', this.updateMarkers.bind(this));

        this.map = new ActivityMap(
            this.MapEvFactory.bind(this),
            Events,
            Tandems,
            _Maps,
            _Config,
            _Util,
            _$rootScope,
            _$templateCache,
            _$compile,
            _$q
        );
    }

    /**
     * @description initiates a new map search - if not on a search page it will go to one
     * otherwise it will refresh the current search pages results
     * @name Activities#search
     * @param {object} search - see {@ActivitySearchCtrl} for parameters
     */
    search(search:cp.activities.ISearchOptionsOpts, type:string, group:string):void {
        var searchObj;

        if (this._$state.is(this._Config.routes.search)) {
            this.emit('/search', {
                search: search,
                type: type,
                group: group,
            });
        } else {
            searchObj = this._Util.location.encodeParams({
                search: search
            });

            if (type) {
                searchObj.type = type;
            }

            if (group) {
                searchObj.group = group;
            }

            this._$state.go(this._Config.routes.search, searchObj);
        }
    }

    /**
     * @description This runs on auth/deauth and updates hidden map markers to either
     * show/hide depending on if they can see the event
     *
     * @name Activities#updateMarkers
     * @param {boolean} authenticated - whether to show/hide
     */
    public updateMarkers(authenticated:boolean):void {
        var hidden, set,
            instance = this._Maps.getInstance(),
            set = instance && instance.currentSet;

        if (!set) {
            return;
        }

        hidden = set.hidden;

        // If there are no hidden markers to update then we don't need to do anything
        if (!_.isArray(hidden) || !hidden.length) {
            return;
        }

        _.each(hidden, function (marker:any) {
            var _class, coords, disableCheckFn;

            if (!authenticated) {
                // user logged out
                // if markers were hidden, they stay hidden
                if (marker.hidden) {
                    return true;
                } else {
                    marker.__C = marker.coords;
                    coords = this._Util.geo.rndPtWithin(marker.coords, marker.radius);
                    marker.hidden = true;
                    marker.icon = this._Config.map.icon.hidden;
                    marker.window = null;
                }
            } else {
                // logged in
                _class = _classKey[marker._class];
                disableCheckFn = _class.disableHide;

                // if this method call returns true then use unrandomized location
                if (disableCheckFn && this[_class.service][disableCheckFn](marker._id)) {
                    coords = marker.__C || marker.coords;
                    marker.hidden = false;
                    marker.icon = this._Config.map.icon.xs;
                    marker.window = null;
                }
            }

            if (coords) {
                return marker.coords = this._Maps.normalizeCoords(coords);
            }
        }, this);

        // update the map
        instance.updateScope()
    }

    MapEvFactory() {
        var Events = ActivityMarkerEvents;
        return {
            click: Events.click.bind(this),
            mouseover: Events.mouseover.bind(this),
            mouseout: Events.mouseout.bind(this),
            clusterClick: Events.clusterClick.bind(this)
        };
    }
}

class ActivityMap {
    constructor(private _MapEvFactory:any,
                private Events:any,
                private Tandems:any,
                private _Maps:any,
                private _Config:any,
                private _Util:any,
                private _$rootScope:cp.IRootScopeService,
                private _$templateCache:ng.ITemplateCacheService,
                private _$compile:ng.ICompileService,
                private _$q:ng.IQService
    ) {}

    /**
     * @description Adds a new stack to the map marker sets
     * Allows for switching between different map icon layers
     * @name Activities.map#initStack
     * @param  {String=} identifier  The stacks name
     * @param  {Object=} opts        Options to pass to the map controller object
     * @param  {Boolean} defaults    Whether to extend the opts object with the default events
     * @return {Boolean}
     */
    initStack(identifier:string, opts?:any, defaults?:any):void {
        var instance;

        if (identifier == null) {
            identifier = 'activity';
        }

        if (opts === void 0) {
            opts = { events: this._MapEvFactory() };
        } else if (_.isPlainObject(opts) && defaults) {
            opts.events = this._MapEvFactory();
        }

        instance = this._Maps.getInstance();
        instance && instance.selectSet(identifier, opts);
    }

    /**
     * @description a proxy for {@link MapCtrl.deselectSet)
         * @name Activities.map#destroyStack
         * @proxy
     * @deprecated in favore of MapCtrl.deselectSet()
         */
    destroyStack() {
        var instance = this._Maps.getInstance();
        return instance && instance.deselectSet();
    }

    /**
     * @description Adds an Array of activities onto the map
     * @name Activities.map#setMarkers
     * @param {array<object>} activities - array of activities
     * @param {object} opts
     * @param {string} opts.classType - event|people|request (tandem & teacher go under people)
     * @param {boolean} opts.replace - whether to remove the existing markers
     * @param {string=} opts.identifier - map set layer key object identifier (default: 'activity')
     * @returns {Promise}
     */
    setMarkers(activities, opts) {
        var identifier = opts.identifier || 'activity',
            instance = this._Maps.getInstance(),
            length = activities.length,
            set = instance.getSet(identifier),
            markers,
            deferred;

        if (!_.isArray(set.hidden)) {
            set.hidden = [];
        }

        if (opts.replace || !length) {
            instance.clearMarkers(identifier);
            if (!length) {
                return this._$q.when();
            }
        }

        markers = _.reduce(activities, this.formatMarker.bind(this, opts.classType, set.hidden, opts.marker), []);
        instance.pushMarkers(markers, identifier);

        deferred = this._$q.defer();

        // wait until the current stack has executed the new markers to resolve
        _.defer(deferred.resolve);

        return deferred.promise;
    }

    /**
     * @description Formats an activity object for the map
     * @name Activities.map#formatMarker
     * @param  {string} classType
     * @param  {array} hidden      reference to an array where to store hidden icons
     * @param  {array<object>} itemArray - the array passed as a part of Array.reduce
     * @param  {object} activity - the current iteration item
     * @return <nothing>            modifies the passed in object
     * @callback
     */
    formatMarker(classType:string, hidden:any[], opts, itemArray, activity):cp.action.IMarker[] {
        classType = classType || activity.activityType;

        var disableCheckFn, marker,
            _class = _classKey[classType],
            loc = _class.location(activity),
            coords = loc && loc.coords;

        // continue if no coords (could be an advertisement slot loaded from history)
        if (!coords) {
            return itemArray;
        }

        activity.marker = marker = {
            id: null,
            _id: activity._id || activity[_class._id],
            _class: classType,
            _type: activity._type || activity.type || classType,
            icon: this._Config.map.icon[_class.icon],
            radius: loc.rsvpOnlyRadius
        };

        if (opts) {
            if (opts.icon) {
                marker.icon = opts.icon(marker) || marker.icon;
            }
        }

        if (loc.hidden) {
            marker.icon = this._Config.map.icon.hidden;
            marker.hidden = true;
            marker.__C = activity.__V ? activity.__V.coords : {};
            hidden.push(marker);
        } else if (loc.rsvpOnlyRadius) {
            disableCheckFn = _class.disableHide;
            hidden.push(marker);

            // if this method call returns true then don't randomize the location
            if (!this[_class.service][disableCheckFn](marker._id)) {
                marker.icon = this._Config.map.icon.hidden;
                marker.__C = coords;
                coords = this._Util.geo.rndPtWithin(coords, loc.rsvpOnlyRadius);
                loc.hidden = marker.hidden = true;
            }
        }

        marker.coords = this._Maps.normalizeCoords(coords);
        marker.id = ActivityMap.generateMarkerID(marker);
        itemArray.push(marker);

        return itemArray;
    }

    /**
     * @description builds a google maps marker ID for the angular google maps API using the type of activity
     * @name Activities.map#generateMarkerID
     */
    public static generateMarkerID(marker:any):string {
        return marker._class + "-" + marker._id;
    }

    /**
     * @description opens a google maps info window
     * @name Activities.map#buildWindow
     */
    buildWindow(opts, marker) {
        return this._Maps.onReady()
            .then(function () {
                var infoOptions, infoWindow,
                    $content = this._$templateCache.get(opts.template),
                    $scope = this._$rootScope.$new(true);

                _.extend($scope, opts.scope);
                $content = this._$compile($content)($scope);
                $content.on('click', '.close-window', function () {
                    if (infoWindow) {
                        return infoWindow.close();
                    }
                });

                infoOptions = {
                    content: $content[0],
                    disableAutoPan: true,
                    maxWidth: 0,
                    infoBoxClearance: new google.maps.Size(1, 1),
                    pixelOffset: new google.maps.Size(-125, - 44),
                    closeBoxURL: "",
                    alignBottom: true,
                    boxClass: 'window',
                    pane: 'floatPane'
                };

                if (!marker) {
                    infoOptions.position = new google.maps.LatLng(
                        opts.coords.latitude, opts.coords.longitude
                    );
                }

                infoWindow = new InfoBox(infoOptions);
                infoWindow.$content = $content;
                return infoWindow;
            }.bind(this));
    }
}

var ActivityMarkerEvents = {
    click: function (marker) {
        this._DataBus.emit("/map/" + marker.model._class + "/click", marker);

        var clickFn = _classKey[marker.model._class].events.click;

        clickFn && clickFn(marker, this._Maps);
    },

    mouseover: function (mapMarker, eventName, marker) {
        var Service = this[_classKey[marker._class].service],
            promise = null,
            infoWindow = mapMarker.window;

        if (!_classKey[mapMarker.model._class].window) {
            return false;
        }

        if (infoWindow) {
            promise = this._$q.when(mapMarker.window);
            infoWindow.triggerClose = false;
            if (infoWindow.timeout) {
                clearTimeout(infoWindow.timeout);
            }
        } else {
            promise = this._$q.when(Service.map.getWindow.call(Service, marker))
                .then(function (activity) {
                    return this.map.buildWindow(Service.map.formatWindow.call(Service, activity), mapMarker);
                }.bind(this));
        }
        return promise.then(function (infoWindow) {
            mapMarker.window = infoWindow;

            // Check if infoWindow isOpen - hack: check if the map property is set
            // http://stackoverflow.com/questions/12410062/check-if-infowindow-is-opened-google-maps-v3
            if (infoWindow.getMap()) {
                return false;
            }

            return _openMarkerWindow(infoWindow, this._Maps.getInstance().rootObject(), mapMarker);
        }.bind(this));
    },

    clusterClick: function (cluster, clusterModel):void {
        this._DataBus.emit("/map/" + clusterModel[0]._class + "/clusterClick", arguments);
    },

    mouseout: function (mapMarker:any):void {
        var infoWindow:any = mapMarker.window;

        if (infoWindow) {
            if (infoWindow.timeout) {
                clearTimeout(infoWindow.timeout);
            }
            infoWindow.triggerClose = true;
            infoWindow.timeout = setTimeout(function () {
                if (infoWindow.triggerClose) {
                    infoWindow.close();
                    infoWindow.$content.removeClass('appear');
                }
                infoWindow.triggerClose = false;
            }, 500);
        }
    }
};

Activities.$inject = [
    'Events',
    'People',
    'Interaction',
    '$state',
    '$rootScope',
    'Config',
    'Util',
    'DataBus',
    'Maps',
    '$templateCache',
    '$compile',
    '$q',
];

require('../app').service('Activities', Activities);
