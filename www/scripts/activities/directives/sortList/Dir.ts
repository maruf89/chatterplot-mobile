/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {SortListDirective} from 'root/activities/directives/SortList';

require('../../../app').directive('sortList', [
    '$templateCache',
    'Interaction',
    'Events',
    'People',
    'Venues',
    'DataBus',
    'Util',
    '$analytics',
    SortListDirective,
]);
