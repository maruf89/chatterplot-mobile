/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';
var SearchAbstract, _filterDefaults, _searchText, _searchTextTypes;

_filterDefaults = {
    location: {
        coords: {}
    },
    languages: []
};

_searchTextTypes = {
    events: 'events.SEARCH',
    speakers: 'tandem.SPEAKER_SEARCH',
    tandems: 'tandem.SEARCH',
    teachers: 'teacher.SEARCH'
};

_searchText = {
    language: false,
    name: {
        events: 'activities.PLACEHOLDER-EVENT_TEXT_SEARCH',
        tandems: 'activities.PLACEHOLDER-USER_TEXT_SEARCH'
    }
};

module.exports = [
    'locale', 'Venues', '$scope', 'Util',
    SearchAbstract = <cp.activities.IDirectiveSearchCtrl>(function () {
        function SearchAbstract(
            locale:any,
            Venues:any,
            $scope:cp.IScope,
            Util:any
        ) {
            if (typeof this.types !== 'string') {
                throw new Error('activitySearch must have types provided');
            }

            this._locale = locale;
            this._Venues = Venues;
            this._$scope = $scope;
            this._Util = Util;

            var self = this,
                filters = _.cloneDeep(_filterDefaults),
                searchGroups,
                vars;

            if (this.data) {
                filters = Util.defaultsDeep(filters, this.data);
            }

            // Set default search group if none set 
            if (!_.isArray(this.groups)) {
                this.groups = ['language'];
            } else if (this.groups.length > 1) {

                // Otherwise build the groups 
                searchGroups = [];
                locale.ready('activities').then(function () {
                    _.each(self.groups, function (group:string) {
                        return searchGroups.push({
                            display: locale.getString('activities.SEARCH_BY_' + group.toUpperCase()),
                            name: group
                        });
                    });

                    return $scope.$root.safeDigest($scope);
                });
            }

            this.vars = vars = {
                groups: searchGroups,
                locationSet: null,
                displayLocation: {},
                locationUpdate: 0,
                filter: filters,
                curLang: typeof this.syncWatch !== 'undefined' && {},
                types: _.map(this.types.split(','), function (type:string):{type:string; caps:string} {
                    // quick fix with translation key changes
                    var tl;

                    if (type === 'tandems') tl = 'TANDEM_PARTNERS';

                    return {
                        type: type,
                        caps: (tl || type.toUpperCase())
                    };
                }),
                oneType: null,
                searchText: null
            };

            // If we are only searching 1 type - template will hide the search 
            vars.oneType = vars.types.length === 1;


            this._bindListeners();

            // combination of both 
            if (searchGroups) {
                this.currentGroupType = this.currentGroup + "-" + this.currentType;
                $scope.$watch('Search.currentGroupType', this.groupTypeChange.bind(this));
            }

            if (vars.curLang) {
                $scope.$watch('Search.syncWatch', function (post, prev) {
                    if (post !== prev) {
                        if (this.data) {
                            vars.filters = Util.defaultsDeep(filters, this.data);
                        }

                        vars.curLang = this.data.languages;
                        this._setVariables();

                    }
                }.bind(this));
            }

            this._setVariables();
            this._setSearchLocation();
        }

        SearchAbstract.prototype._setVariables = function () {
            // set a defaults if none given
            this.currentGroup = this.currentGroup || this.groups[0];
            this.currentType = this.currentType || this.vars.types[0].type;

            // Whether to show the search form
            if ((this.vars.searchText = !!_searchText[this.currentGroup])) {
                this.updatePlaceholder();
            }
        };

        SearchAbstract.prototype._bindListeners = function () {
            var self = this,
                scope = this._$scope;

            // If we have more than 1 type, watch when they change 
            if (this.currentType && !this.vars.oneType && this.currentGroup) {
                scope.$watch('Search.currentType', function (post, prev) {
                    if (post && post !== prev) {
                        self.currentGroupType = self.currentGroup + '-' + post;
                    }
                });
            }

            // If we have more than 1 possible group watch that as well 
            if (this.currentGroup && _.isArray(this.groups) && this.groups.length > 1) {
                scope.$watch('Search.currentGroup', function (post,
                    prev) {
                    if (post && post !== prev) {
                        self.currentGroupType = post + '-' + self.currentType;
                    }
                });
            }
        };

        /**
         * @description previously the languageID was updated by reference in the langComplete directive but
         * that was proving to ONLY work when inspecting the instance, and pass no language other times so
         * this was added to make sure the
         * @param {number} languageID
         */
        SearchAbstract.prototype.languageSet = function (languageID:number):void {
            this.vars.filter.languages = [languageID];
            this.vars.update('language');
        };

        SearchAbstract.prototype.locationUnset = function () {
            this.vars.filter.location.coords = {};
            return this.vars.update('location');
        };

        SearchAbstract.prototype.locationSet = function (location, type, passive) {
            var loc,
                curLoc = this.vars.filter.location,
                preZoom = curLoc && curLoc.coords && curLoc.coords.zoom;

            if (type === 'GOOGLE') {
                loc = this._Venues.scrapePlace(location, type, !passive);
                this.vars.filter.location.coords = loc.coords;
            } else if (type === 'CURRENT') {
                this.vars.filter.location.coords = {
                    lat: location.latitude,
                    lon: location.longitude,
                    zoom: 15
                };
            }

            // If the location was triggered by the 'Get Current Location (get-cur-loc)'
            if (passive) {
                // then update the input text to reflect the current loc
                this.vars.displayLocation = loc;
                this.vars.displayLocation.address = null;
                this.vars.locationUpdate++;
                this.vars.zoomLevel = this.vars.filter.location.coords.zoom = preZoom || 12;
                this._$scope.$root.safeDigest(this._$scope);
            } else {
                // otherwise it was a user interaction - trigger search
                this.vars.update('location');
            }
        };

        SearchAbstract.prototype.groupTypeChange = function (post, prev) {
            var ref;
            if (post === prev) {
                return false;
            }
            ref = post.split('-'), this.currentGroup = ref[0], this.currentType = ref[1];
            if ((this.vars.searchText = !!_searchText[this.currentGroup])) {
                return this.updatePlaceholder();
            } else {
                return this._$scope.$root.safeDigest(this._$scope);
            }
        };

        SearchAbstract.prototype.updatePlaceholder = function () {
            var self = this,

                // get the placeholder text key
                string = _searchText[this.currentGroup][this.currentType];

            // update the placeholder text 
            return this._locale.ready(this._locale.getPath(string))
                .then(function () {
                    self.vars.searchPlaceholder = self._locale.getString(string);
                    return self._$scope.$root.safeDigest(self._$scope);
                });
        };

        SearchAbstract.prototype.manualSearch = function () {

            // onSubmit expects a boolean as the first param for whether to query w/ text 
            return this.vars.onSubmit(!!this.vars.filter.query);
        };

        SearchAbstract.prototype._setSearchLocation = function () {
            var loc = this.vars.filter.location,
                coords;

            if (!loc || !(coords = loc.coords) || !coords.lat) {
                return false;
            }

            this._Util.geo.latlng2Address(coords).then(function (location) {
                if (!location || !location.length) { return false; }

                this.locationSet(location[0], 'GOOGLE', true);
            }.bind(this));
        };

        return SearchAbstract;
    })()
];
