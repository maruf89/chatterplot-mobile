/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
/**
 * @ngdoc directive
 * @module activitySearch2Directive
 * @restrict E
 * @requires Activities
 * @requires locale
 * @requires $templateCache
 */
'use strict';
var SearchCtrl = require('../SearchAbstract');
require('../../../app.js').directive('activitySearch', [
    'Activities', 'locale', '$templateCache', 'Util',
    function (Activities, locale, $templateCache, Util) {
        // Preload this for Ctrl#displayLanguage in `post:`
        locale.getPath('languages');
        return {
            restrict: 'E',
            replace: true,
            scope: {
                currentType: '=?',
                currentGroup: '=?',
                types: '@',
                onSearch: '&',
                forceSearch: '@',
                grabLocation: '@',
                allIcons: '@',
                data: '=?',
                groups: '=?',
                syncWatch: '=?',
                jumpOnFirstInput: '@',
            },
            template: $templateCache.get('views/activities/directives/search/Template.html'),
            controller: SearchCtrl,
            controllerAs: 'Search',
            bindToController: true,
            link: {
                pre: function (scope, iElem, iAttrs, Ctrl) {
                    Ctrl.vars.idPrefix = 'activitySearch-' + scope.$id;
                },
                post: function (scope, iElem, iAttrs, Ctrl) {
                    var vars = Ctrl.vars, filters = vars.filter, 
                    // the key to listen on to jump to next input
                    jumpInputListener = 'language';
                    Ctrl.displayLanguage = function () {
                        return locale.getString(Util.language.getI18nKey(filters.languages && filters.languages[0] || 0));
                    };
                    Ctrl.displayLocation = function () {
                        var location = vars.displayLocation;
                        if (location.city || location.state ||
                            location.country) {
                            // do something
                            return Util.geo.format.address(vars.displayLocation, ['city', 'state', 'country'], ', ');
                        }
                        else {
                            return locale.getString('common.SEARCHING');
                        }
                    };
                    /**
                     * @description triggers that something in the seacrh has been updated
                     * @param {string} source - what triggered the change??? (language|location)
                     */
                    vars.update = function (source) {
                        _.defer(function () {
                            if (!Ctrl.forceSearch) {
                                // If we don't have manual search enabled, then update on any change
                                this.onSubmit();
                            }
                            else if (source === jumpInputListener && Ctrl.jumpOnFirstInput) {
                                // Otherwise disable auto search
                                // see if input completion jump is enabled and the source is the first input field
                                iElem.find('.location input').focus();
                            }
                        }.bind(this));
                    };
                    vars.onSubmit = function (nameSearch) {
                        if (!nameSearch) {
                            vars.filter.query = null;
                        }
                        return Ctrl.onSearch({
                            type: Ctrl.currentType,
                            group: Ctrl.currentGroup,
                            filters: vars.filter
                        });
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map