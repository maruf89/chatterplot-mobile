/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var regionsSource = require('front/sections/landing/config/regions.json');
require('../../../app.js').directive('lsRegions', [
    '$state', '$templateCache',
    function ($state, $templateCache) {
        return {
            restrict: 'E',
            scope: {},
            replace: true,
            template: $templateCache.get('views/landing/sections/regions/Template.html'),
            link: {
                pre: function (scope) {
                    // get the current browser size
                    scope.vars = {
                        regionData: regionsSource,
                        regions: _.map(regionsSource.blocks.slice(0, 3), function (region) {
                            // Define a link for each image
                            region.href = $state.href('map.activity.search.language', {
                                type: 'tandem-partners',
                                language: region.lang.trim().toLocaleLowerCase(),
                            });
                            return region;
                        })
                    };
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map