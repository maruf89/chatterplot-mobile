/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app.js').directive('landingSectionSplash', [
    'Activities', '$templateCache',
    function(Activities:cp.activities.IService, $templateCache:ng.ITemplateCacheService) {
        return {
            restrict: 'E',
            replace: true,
            scope: true,
            template: $templateCache.get('views/landing/sections/splash/Template.html'),
        };
    }
]);

