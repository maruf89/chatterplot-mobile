/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

import {AddYourself} from 'root/landing/directives/AddYourself';

require('../../../app').directive('addYourself', [
    '$templateCache',
    AddYourself,
]);
