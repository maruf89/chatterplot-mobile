/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
/**
 * @class
 */
var AddYourselfCtrl = function ($scope, $analytics, localStorageService, Venues, AuthenticateModal) {
    this._$scope = $scope;
    this._$analytics = $analytics;
    this._locStorage = localStorageService;
    this._Venues = Venues;
    this._AuthenticateModal = AuthenticateModal;
    this.data = {
        isRequired: true,
        "native": null,
        learning: [],
        location: null,
        locationChange: 0 // {number} a $watch variable
    };
};
AddYourselfCtrl.prototype = _.extend(AddYourselfCtrl.prototype, {
    locationUpdate: function (location) {
        this.data.location = location;
        this.data.locationChange++;
        _.defer(function () {
            this._$scope.$root.safeDigest(this._$scope);
        }.bind(this));
    },
    submitForm: function () {
        var signupData = {}, learning = _.map(this.data.learning, function (languageID) {
            return {
                languageID: languageID,
                learning: true
            };
        }), location = this._Venues.scrapePlace(this.data.location, 'google', 2);
        signupData.languages = _.reduce(this.data["native"], function (arr, next) {
            var noHave = true;
            _.each(learning, function (lang) {
                if (lang.languageID === next.languageID) {
                    lang.level = 7;
                    return noHave = false;
                }
            });
            if (noHave) {
                arr.push({
                    languageID: next.languageID,
                    level: 7
                });
            }
            return arr;
        }, []).concat(learning);
        location.primary = true;
        return this._Venues.save(location, true)
            .then(function () {
            signupData.locations = [location];
            this._locStorage.set('signupData', signupData);
            return this._AuthenticateModal.activate('signup', {
                userData: signupData
            });
        }.bind(this));
    }
});
AddYourselfCtrl.$inject = [
    '$scope',
    '$analytics',
    'localStorageService',
    'Venues',
    'AuthenticateModal'
];
require('../../../app').controller('AddYourselfCtrl', AddYourselfCtrl);
//# sourceMappingURL=Ctrl.js.map