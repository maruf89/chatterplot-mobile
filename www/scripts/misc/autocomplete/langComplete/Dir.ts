/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

interface ILangObj {
    id:number
    name:string
}

interface ISelected {
    item:ILangObj
}
    
class LangCompleteCtrl {
    private _onAddHook:(languageID:number, name:string) => boolean | number;
    private _onRemoveHook:(index:number) => boolean;

    public suggestions:string[] = [];
    public prepopulated:ILangObj[] = [];
    public exclude:number[] = [];
    public getLangs:(q:string) => {id:number; name:string}[];
    public suggestLength:number = 25;
    public langs:number[] = [];

    // The primary variable that gets displayed
    public placeholder:string;

    // fallback placeholder text
    public placeholder_text:string;

    constructor(
        private _$scope,
        $attrs,
        Util
    ) {
        var languages:string[];

        this._onAddHook = $attrs.onAdd && _$scope.onAdd;
        this._onRemoveHook = $attrs.onRemove && _$scope.onRemove;

        if (_$scope.langs) {
            this.langs = _$scope.langs;
        }

        // Terms to exclude
        if (_$scope.exclude) {
            this.exclude = _$scope.exclude;
        } else if (this.langs.length) {
            this.exclude = _.pluck(this.langs, 'term');
        }

        Util.language.getTranslated(!!_$scope.allowAll)
            .then(function (_languages) {
                languages = _languages;

                // If we're not loading the 'All Languages' key, then offset the index of
                // the autocomplete results by + 1, otherwise we get the wrong index language
                var indexOffset = !_$scope.allowAll ? 1 : 0;

                this._buildPrepopulated(languages, indexOffset);

                this.getLangs = Util.language.ttSubstringMatcher(
                    languages,
                    this.suggestions,
                    this.exclude,
                    this.suggestLength,
                    indexOffset
                );
            }.bind(this));
    }

    private _buildPrepopulated(languages:string[], offset:number):void {
        this.prepopulated = _.map(
            languages.slice(0, this.suggestLength),
            function (lang:string, index:number):ILangObj {
                return {
                    name: lang,
                    id: index + offset,
                }
            }
        );
    }

    /**
     * @description get's called when a language is selected
     * Also has the option to set hooks which halt further execution depending on the response
     *      Hook Responses: true  - halts execution after calling $digest
     *                      1     - same as true except also updates the placeholder
     *                      false - continue add the language to our list and $digest
     * @param {ILangObj} selected
     * @returns {*}
     */
    public onAdd(selected:ISelected):void {
        // hide the placeholder text
        var langObj:ILangObj = selected.item,

            // Call hook
            hookRes = this._onAddHook && this._onAddHook({
                    languageID: langObj.id,
                    name: langObj.name,
                });

        // End here?
        if (hookRes === false) {
            this._$scope.$root.safeDigest(this._$scope);
            return;
        }

        this.langs.push({
            languageID: langObj.id,
            name: langObj.name,
        });

        this.exclude.push(langObj.name);
        this._$scope.$root.safeDigest(this._$scope);
    }

    public onRemove(index:number):void {
        if (index == null) {
            index = this.langs.length - 1;
        }

        if (index < 0) {
            return;
        }

        var hookRes = this._onRemoveHook && this._onRemoveHook(index);

        // Call hook
        if (hookRes === false) {
            return;
        }

        this.langs.splice(index, 1);

        // Enable ng-required if necessary
        if (this.langs.length) {
            this.exclude.splice(index, 1);
        } else {
            this.exclude.length = 0;
        }

        this._$scope.$root.safeDigest(this._$scope);
    }
}

var LangCompleteDirective = function (keyboardFilter, locale, Util, $templateCache) {
    locale.ready('common');

    return {
        restrict: 'E',
        replace: true,
        template: $templateCache.get('views/misc/autocomplete/langComplete/Template.html'),
        scope: {
            icon:           '@',        //
            langs:          '=?',       //
            exclude:        '=?',       //
            placeholder:    '@',        //
            disabled:       '=?',       //
            onAdd:          '&',        //
            onRemove:       '&',        //
            required:       '=?',       //
            allowAll:       '@',        //
            maxItems:       '@',        // {number} maximum number of allowed languages (default: 20)
            showDefaults:   '@',        //
        },
        controllerAs: 'LangComplete',
        controller: ['$scope', '$attrs', 'Util', LangCompleteCtrl],
        link: {
            pre: function (scope, iElem, iAttrs, Ctrl) {
                scope.vars = {
                    disabled: scope.disabled,
                    $touched: false,
                    isEmpty: !(scope.langs && scope.langs.length && scope.langs[0].languageID),
                    fieldName: iAttrs.name || 'langComplete',
                    value: null,
                    input: iElem[0].querySelector('.input-language'),
                };

                var vars = scope.vars,
                    maxItems = 20;

                if (typeof iAttrs.maxItems === 'string') {
                    maxItems = p(iAttrs.maxItems);
                }

                vars.input.setAttribute('max-selected-items', maxItems);

                locale.ready('common').then(function () {
                    vars.input.setAttribute('cancel-label', locale.getString('common.DONE'));
                    vars.input.setAttribute('set-items-label', locale.getString('common.SELECT_LANGUAGE'));
                    vars.input.setAttribute('selected-items-label', locale.getString('common.SELECTED_ITEMS'));
                });

                if (!vars.isEmpty) {
                    Util.language.expandPossible(scope.langs)
                        .then(function (complete) {
                            [].push.apply(Ctrl.langs, complete);
                        });
                }

                if (iAttrs.disabled) {
                    scope.$watch('disabled', function (newVal) {
                        vars.disabled = newVal;
                    });
                }
            },

            post: function (scope, iElem, iAttrs, Ctrl) {
                var vars = scope.vars,
                    placeholder = iAttrs.placeholder || 'profile.PLACEHOLDER-ADD_LANGUAGE',
                    updateRequired;

                if (iAttrs.required) {
                    updateRequired = function () {
                        return vars.required = scope.required ? vars.isEmpty : false;
                    };

                    scope.$watch('required', updateRequired);
                }


                locale.ready(locale.getPath(placeholder))
                    .then(function () {
                        Ctrl.placeholder = locale.getString(placeholder);
                        vars.input.setAttribute('placeholder', Ctrl.placeholder);
                    });

                vars.onAdd = function (selected) {
                    Ctrl.onAdd(selected);

                    // disable required flag because it's not empty
                    vars.isEmpty = vars.required = false;
                };

                vars.onRemove = function (index:number) {
                    Ctrl.onRemove(index);

                    if (!Ctrl.langs.length) {
                        vars.isEmpty = true;
                        vars.required = scope.required;
                    }
                };
            }
        }
    };
};


/**
 * @ngdoc directive
 * @description Updates the autocomplete placeholder value
 * depending on whether the field has any terms
 */
require('../../../app.js').directive('langComplete', [
    'keyboardFilter',
    'locale',
    'Util',
    '$templateCache',
    LangCompleteDirective
]);
