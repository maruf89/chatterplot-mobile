/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
"use strict";
/**
 * @ngdoc directive
 * @module singleLangCompleteDirective
 * @restrict E
 * @description wraps {@link langCompleteDirective} so that only a single language can be selected
 *
 * @requires Util
 */
require('../../../app').directive('singleLangComplete', ['Util', '$q', '$templateCache',
    function (Util, $q, $templateCache) {
        return {
            restrict: 'E',
            replace: true,
            template: $templateCache.get('views/misc/autocomplete/singleLangComplete/Template.html'),
            scope: {
                icon: '@',
                languages: '=',
                exclude: '=?',
                placeholder: '@',
                required: '=?',
                disabled: '=?',
                onAdd: '&',
                onRemove: '&',
                onFocus: '&',
                allowAll: '@',
                showDefaults: '@',
                autoSize: '@',
                curLang: '=?',
            },
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var vars, required = iAttrs.required ? scope.required : false;
                    scope.vars = vars = {
                        langs: Util.language.arrayToObject(scope.languages ? _.isArray(scope.languages) ? scope.languages : [scope.languages] : []),
                        exclude: scope.exclude,
                        placeholder: scope.placeholder,
                        fieldName: iAttrs.name || '',
                        required: required,
                        curLang: scope.curLang || {},
                        onBlur: function (event) {
                            event.target.value = '';
                            if (!vars.langs.length && vars.curLang.name) {
                                vars.langs.push(vars.curLang);
                            }
                            vars.resizeInput(true);
                            scope.$input.typeahead('close');
                            scope.$root.safeDigest(scope);
                        },
                        onFocus: function (event) {
                            var target = event.target;
                            // Set back to normal width if we're autosizing
                            vars.resizeInput(false);
                            // Set the input value to that of the currently selected language
                            if (vars.curLang.name) {
                                target.value = vars.curLang.name;
                            }
                            // Manually force a select of the contents
                            _.defer(function () {
                                target.select();
                            });
                            // Empty out the languages temporarily
                            vars.langs.length = 0;
                            // If there's a function above that needs this, go ahead
                            if (iAttrs.onFocus) {
                                scope.onFocus(event);
                            }
                            // Lastly only notify child scope
                            scope.$root.safeDigest(scope);
                            // On blur, we need to undo everything we just did
                            //scope.$input.off('blur.single').one('blur.single', );
                            return true;
                        },
                        resizeInput: function (shrink) {
                            if (!scope.autoSize) {
                                return false;
                            }
                            var className = 'shrink';
                            if (shrink && vars.curLang.name) {
                                iElem.addClass(className);
                                _.defer(function () {
                                    var $term = iElem.find('.term');
                                    if ($term && $term[0]) {
                                        if (Util.$isHidden($term)) {
                                            // if element is hidden then we set a default width
                                            scope.$input.width(110);
                                        }
                                        else {
                                            // otherwise we can calculate its width
                                            scope.$input.width($term.width() + 1);
                                        }
                                    }
                                });
                            }
                            else {
                                iElem.removeClass(className);
                                scope.$input.css('width', 'auto');
                            }
                        }
                    };
                    // Make sure they're one and the same
                    //if (vars.langs !== scope.languages) {
                    //    scope.languages = vars.langs;
                    //}
                    if (scope.vars.langs.length) {
                        Util.language.expand(scope.vars.langs)
                            .then(function (arg) {
                            scope.vars.curLang = arg[0];
                        });
                    }
                },
                post: function (scope, iElem, iAttrs) {
                    var vars = scope.vars, checkRequired = function () {
                        vars.required = !!(scope.required && !(vars.curLang.name || vars.langs.length));
                    };
                    if (scope.curLang) {
                        scope.$watch('curLang', function (post, prev) {
                            if (post && post !== prev) {
                                var promise;
                                if (_.isArray(post) && typeof post[0] === 'number') {
                                    promise = Util.language.expand(Util.language.arrayToObject(post));
                                }
                                else {
                                    promise = $q.when(post);
                                }
                                promise.then(function (langArr) {
                                    if (langArr) {
                                        vars.langs = langArr;
                                        scope.$root.safeDigest(scope);
                                        return _.defer(function () {
                                            vars.resizeInput(true);
                                        });
                                    }
                                    return vars.onRemove();
                                });
                            }
                        });
                    }
                    vars.onAdd = function (id, term) {
                        vars.langs.length = 0;
                        scope.languages.push(id);
                        vars.curLang = {
                            languageID: id,
                            name: term
                        };
                        scope.$input.blur();
                        if (iAttrs.onAdd) {
                            scope.onAdd(vars.curLang);
                        }
                        checkRequired();
                        scope.$root.safeDigest(scope);
                        return false;
                    };
                    vars.onRemove = function () {
                        scope.languages.length = vars.langs.length = 0;
                        vars.curLang = {};
                        if (iAttrs.onRemove) {
                            scope.onRemove();
                        }
                        checkRequired();
                        return true;
                    };
                    Util.deferX(function () {
                        scope.$input = iElem.find('.tt-input');
                        scope.autoSize && vars.resizeInput(true);
                    }, 1);
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map