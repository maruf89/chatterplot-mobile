/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var _days = {
    "2": 28,
    "4": 30,
    "6": 30,
    "9": 30,
    "11": 30
};
require('../../app').directive('dateInput', [
    'locale', 'Config', 'Util',
    function (locale, Config, Util) {
        return {
            restrict: 'C',
            templateUrl: 'views/misc/dateInput/Template.html',
            scope: {
                source: '=',
                required: '=?',
                minYear: '=?',
                maxYear: '=?'
            },
            link: {
                pre: function (scope) {
                    var curYear, date, ref;
                    scope.source = scope.source || null;
                    curYear = (new Date())
                        .getFullYear();
                    date = scope.date = {
                        year: {
                            label: 'year',
                            value: null,
                            pattern: /^\d{4}$/,
                            placeholder: null
                        },
                        month: {
                            label: 'month',
                            value: null,
                            pattern: /^\d{1,2}$/,
                            placeholder: null,
                            max: 12,
                            min: 1
                        },
                        day: {
                            label: 'day',
                            value: null,
                            pattern: /^\d{1,2}$/,
                            placeholder: null,
                            max: 31,
                            min: 1
                        },
                        previous: scope.source,
                        valid: '',
                        partialRequired: function () {
                            return scope.required || _.some([
                                'day', 'month', 'year'
                            ], function (key) {
                                return date[key].value;
                            });
                        }
                    };
                    locale.ready('common')
                        .then(function () {
                        date.year.placeholder = locale.getString('common.YYYY');
                        date.month.placeholder = locale.getString('common.MM');
                        return date.day.placeholder = locale.getString('common.DD');
                    });
                    // Set the date
                    ref = (scope.source || '')
                        .split('-'), date.year.value = ref[0], date.month
                        .value = ref[1], date.day.value = ref[2];
                    date.month.value = p(date.month.value);
                    date.day.value = p(date.day.value);
                    if (!isNaN(p(scope.minYear))) {
                        date.year.min = curYear - scope.minYear;
                    }
                    if (!isNaN(p(scope.maxYear))) {
                        date.year.max = curYear - scope.maxYear;
                    }
                    return date.pieces = Config.format.date.order(date);
                },
                post: function (scope) {
                    var date;
                    date = scope.date;
                    date.onChange = function (piece) {
                        var day, month, skipCheck, year;
                        date.error = null;
                        scope.dateInput.valid.$setValidity('required', true);
                        scope.dateInput.valid.$setValidity('invalid', true);
                        if (_.keys(scope.dateInput.$error)
                            .length > 1 || (scope.dateInput.$error.required &&
                            scope.dateInput.$error.required.length >
                                1)) {
                            scope.source = date.previous;
                            scope.dateInput.valid.$setValidity('required', false);
                            return;
                        }
                        // Double check the day isn't above the threshold
                        year = p(date.year.value);
                        day = p(date.day.value);
                        month = p(date.month.value);
                        // If all fields are empty and this is not required
                        skipCheck = isNaN(year) && isNaN(day) &&
                            isNaN(month) && !date.partialRequired();
                        // If month, update the max day
                        if (!skipCheck && (piece.label === 'month' ||
                            piece.label === 'year')) {
                            if (month === 2) {
                                date.day.max = year % 4 ? 28 : 29;
                            }
                            else if (_days[month]) {
                                date.day.max = _days[month];
                            }
                            else {
                                date.day.max = 31;
                            }
                        }
                        if (!skipCheck && piece.label === 'year' &&
                            !date.year.value) {
                            return locale.ready('profile')
                                .then(function () {
                                return scope.$root.safeApply(function () {
                                    date.error =
                                        locale.getString('profile.ERROR-INVALID_YEAR');
                                    return scope.dateInput
                                        .valid.$setValidity('invalid', false);
                                });
                            });
                        }
                        else if (!skipCheck && (isNaN(year) ||
                            year < date.year.max || year > date.year
                            .min || isNaN(month) || month < date
                            .month.min || month > date.month.max ||
                            isNaN(day) || day < date.day.min ||
                            day > date.day.max)) {
                            return scope.dateInput.valid.$setValidity('invalid', false);
                        }
                        else {
                            scope.source = date.day.value ? date.year
                                .value + "-" + (Util.pad(date.month.value, 2)) + "-" + (Util.pad(date.day.value, 2)) : null;
                            date.valid = 'true';
                            date.error = null;
                            scope.dateInput.valid.$setValidity('required', true);
                            return scope.dateInput.valid.$setValidity('invalid', true);
                        }
                    };
                    if (date.month.value) {
                        return date.onChange(date.month);
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map