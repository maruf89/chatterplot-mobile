/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app')
    .directive("i18nVariable", [
        'locale',
        function (locale) {
            return {
                restrict: 'A',
                scope: false,
                link: {
                    pre: function (scope, iElem, iAttrs) {
                        var watch = iAttrs.i18nVariable,
                            set = false;

                        return scope.$watch(watch, function (post, prev) {
                            if (typeof post === 'string' && (post !== prev || !set)) {
                                set = true;
                                return locale.ready(locale.getPath(post)).then(function () {
                                    return iElem.html(locale.getString(post));
                                });
                            }
                        });
                    }
                }
            };
        }
    ])
    .directive("i18nBoVariable", [
        'locale',
        function (locale) {
            return {
                restrict: 'A',
                scope: false,
                link: {
                    pre: function (scope, iElem, iAttrs) {
                        var listener, set, watch;
                        watch = iAttrs.i18nBoVariable;
                        set = false;
                        return listener = scope.$watch(watch, function (post,
                            prev) {
                            if (typeof post === 'string' && (post !==
                                    prev || !set)) {
                                set = true;
                                return locale.ready(locale.getPath(
                                        post))
                                    .then(function () {
                                        iElem.html(locale.getString(
                                            post));
                                        return listener();
                                    });
                            }
                        });
                    }
                }
            };
        }
    ])

    .directive('i18nHtml', [
        'locale', '$compile',
        function (locale, $compile) {
            /**
             * @description compiles an i18n key to html & accepts variables
             * @example
             *      p.error-msg(
             *          i18n-html="misc.INVALID_FILE_2_BIG"
             *          data-keys="float"
             *          data-values="{{::vars.max.fileSize}}"
             *      )
             */
            return {
                restrict: 'A',
                scope: false,
                link: {
                    pre: function (scope, iElem, iAttrs) {
                        return locale.ready(locale.getPath(iAttrs.i18nHtml))
                            .then(function () {
                                // must wrap the text in an element for angular to be able to compile properly
                                var keys = iAttrs.keys && iAttrs.values && iAttrs.keys.split('|'),
                                    values = keys && iAttrs.values.split('|'),
                                    data,
                                    args = [iAttrs.i18nHtml],
                                    translated,
                                    html;

                                if (values) {
                                    data = {};
                                    _.each(keys, function (key, index) {
                                        data[key] = values[index];
                                    });
                                    args.push(data);
                                }

                                translated = '<span>' + locale.getString.apply(locale, args) + '</span>';
                                html = $compile(translated)(scope);
                                return iElem.html(html);
                            });
                    }
                }
            };
        }
    ]);
