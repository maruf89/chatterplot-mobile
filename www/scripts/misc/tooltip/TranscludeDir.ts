/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var disappearMS, teardownMS, timeoutMS;

timeoutMS = 350;

teardownMS = 450;

disappearMS = 125;

require('../../app').directive('tooltipTransclude', [
    'Config', 'locale', '$timeout', 'Util',
    function (Config, locale, $timeout, Util) {
        return {
            restrict: 'E',
            replace: true,
            template: '<div class="Tooltip transclude">' +
                '<a ng-if="::!attach" class="icon-help"></a>' +
                '<div class="box ng-hide {{::pos}}" ng-show="vars.active">' +
                '<div class="inner">' +
                '<a class="carrot"></a><div ng-transclude></div>' + '</div>' +
                '</div>' + '</div>',
            transclude: true,
            scope: {
                pos: '@',
                attach: '@'
            },
            link: {
                pre: function (scope, iElem, iAttrs) {
                    return scope.vars = {
                        active: false
                    };
                },
                post: function (scope, iElem) {
                    var $elem, appear, disapearTimer, disappear,
                        disappearStart, onEnter, onLeave,
                        teardownComplete, teardownTimer, timer;
                    timer = null;
                    teardownTimer = null;
                    teardownComplete = true;
                    disappearStart = false;
                    disapearTimer = null;
                    appear = function () {
                        timer = null;
                        return scope.$root.safeApply(function () {
                            return scope.vars.active = true;
                        });
                    };
                    disappear = function () {
                        if (!scope.vars.active || !disappearStart) {
                            return disappearStart = false;
                        }
                        disappearStart = false;
                        teardownComplete = false;
                        if (teardownTimer) {
                            $timeout.cancel(teardownTimer);
                            teardownTimer = $timeout(function () {
                                return teardownComplete =
                                    true;
                            }, teardownMS);
                        }
                        return scope.$root.safeApply(function () {
                            return scope.vars.active = false;
                        });
                    };
                    onEnter = function () {
                        disappearStart = false;
                        if (!teardownComplete) {
                            $timeout.cancel(teardownTimer);
                            return appear();
                        }
                        return timer = $timeout(appear, timeoutMS);
                    };
                    onLeave = function () {
                        if (timer) {
                            $timeout.cancel(timer);
                            return timer = null;
                        } else {
                            disappearStart = true;
                            return $timeout(disappear, disappearMS);
                        }
                    };
                    $elem = scope.attach ? iElem.parent() : iElem;
                    return $elem.on('mouseenter', onEnter)
                        .on('mouseleave', onLeave);
                }
            }
        };
    }
]);
