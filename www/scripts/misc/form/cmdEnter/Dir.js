/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('cmdEnter', [
    'keyboardFilter',
    function (keyboardFilter) {
        return {
            restrict: 'A',
            link: function (scope, iElem) {
                var form = iElem[0].form;
                // Exit if there is no attached form
                if (!form) {
                    return false;
                }
                iElem.on('keydown', function (event) {
                    if (keyboardFilter(event.keyCode) === 'enter' && event.metaKey) {
                        $(form).trigger('submit');
                    }
                });
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map