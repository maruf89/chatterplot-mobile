/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app')
    .directive('passwordMeter', [
    function () {
        return {
            restrict: 'EA',
            scope: {
                complexity: '=passwordMeter'
            },
            template: '<div class="meter"></div>',
            link: function (scope, iElem, iAttrs) {
                var meter;
                meter = iElem.children('.meter')[0];
                return scope.$watch('complexity', function (complexity) {
                    return meter.style.width = complexity + "%";
                });
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map