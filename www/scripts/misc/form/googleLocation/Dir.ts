/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

/**
 * ngdoc directive
 * @namespace GoogleLocationDirective
 */

'use strict';

var googleLoc = require('front/util/googleLocation');

require('../../../app').directive('googleLocation', [
    'uiGmapGoogleMapApi', 'locale', 'Util', 'DataBus', '$q', '$templateCache', 'Venues',
    function (
        GoogleMapApi:any,
        locale:any,
        Util:any,
        DataBus:any,
        $q:ng.IQService,
        $templateCache:ng.ITemplateCacheService,
        Venues:any
    ) {
        return {
            restrict: 'E',
            replace: true,
            require: '^form',
            scope: {
                source: '=?',       // {object=} any existing location data to use if exists
                displaySrc: '=?',   // {string=} text to show in the input
                onSelect: '&',      // {function} callback to call when a location is selected
                onEmpty: '&',       // {function=} callback to call if/when the input is emptied
                doEmpty: '=?',      // {expr=} if passed will watch this and empty the input on change
                searchType: '@',    // {string} whether to geocode or search places (geocode|places)
                types: '@',         // {string} list of types separated by comma (no spaces) from table 3 of https://developers.google.com/places/supported_types
                required: '=?',     // {boolean=} if true, will throw an error if the field is empty
                update: '=?',       // {expr=} if set will watch for changes to this and update the display text accordingly
                curLocation: '@',   // {boolean} whether to add the 'Get current location' functionality option
                invalidate: '=?',   // {expr} if evaluates to true will mark the input as invalid
                invalidError: '@',  // {i18n} if invalidate is set, this is required
                showIcon: '@',      // {boolean} whether to show an icon before the input
                placeholder: '@',   // {i18n} text to set as the placeholder (default:'activities.PLACEHOLDER-FAKE_DESIRED_LOC')
                noGeneral: '@',     // {boolean} if true will disallow anything vaguer than a street (no Neukolln or Berlin)
                getCurLoc: '@',     // {boolean} if true will grab the users current location
                zoomLevel: '=?',    // {number=} the current zoom level of the passed in obj
                syncKey: '@',       // {string=} if set will read/save the display/stored value to the rootScope & sync on change
                inline: '@',        // {string} whether to style the input like an ionic input & which class to apply
            },
            template: $templateCache.get('views/misc/form/googleLocation/Template.html'),
            link: {
                pre: function (scope, iElem, iAttrs, Form) {
                    if (!scope.searchType) {
                        throw new Error('googleLocation requires searchType set to one of `geocode` or `places`');
                    }

                    var placeholder,
                        syncKey = iAttrs.syncKey,
                        vars = scope.vars = {
                            inputClasses: (function ():string {
                                var classes:string = '';

                                if (scope.showIcon) {
                                    classes += 'icon-location';
                                }

                                if (scope.inline) {
                                    classes += 'item item-input';
                                }

                                return classes;
                            })(),
                            inline: iAttrs.inline === 'true',
                            placeholder: '',
                            defaultValue: '',
                            curLocation: iAttrs.curLocation === 'true',
                            syncObj: null,
                            viewValueKey: scope.searchType === 'places' ? 'description' : 'formatted_address',
                            input: iElem[0].querySelector('.ion-autocomplete'),
                        };

                    if (iAttrs.update) {
                        scope.$watch('update', function (post, prev) {
                            if (post !== prev) {
                                googleLoc.setDefault(scope, Util);
                            }
                        });
                    }

                    locale.ready('common').then(function () {
                        vars.input.setAttribute('cancel-label', locale.getString('common.DONE'));
                        vars.input.setAttribute('set-items-label', locale.getString('common.SELECT'));
                        vars.input.setAttribute('selected-items-label', locale.getString('common.SELECTED_ITEMS'));
                    });

                    googleLoc.setDefault(scope, Util);
                    placeholder = iAttrs.placeholder || 'activities.PLACEHOLDER-FAKE_DESIRED_LOC';

                    locale.ready(locale.getPath(placeholder)).then(function () {
                        vars.placeholder
                            = iElem.find('.input-location')[0].placeholder
                            = locale.getString(placeholder);

                        scope.$root.safeDigest(scope);
                    });

                    googleLoc.initSync(syncKey, vars, scope.$root);

                    scope.Form = Form;
                },

                post: function (scope, iElem, iAttrs) {
                    var vars = scope.vars,
                        selected = false,
                        invalidationUpdate,
                        input;

                    vars.getCurrentLocation = function () {
                        DataBus.emit('progressLoader', { start: true });

                        locale.ready('common').then(function () {
                            vars.defaultValue = locale.getString('common.SEARCHING');
                        });

                        return Util.geo.getCurrentLocation().then(function (coords) {
                            if (scope.onSelect) {
                                scope.onSelect({
                                    location: coords,
                                    type: 'CURRENT',
                                    passive: false,
                                });
                            }

                            return locale.ready('common')
                                .then(function () {
                                    vars.defaultValue = locale.getString('common.MY_LOCATION');

                                    DataBus.emit('progressLoader');

                                    scope.$root.safeDigest(scope);
                                });
                        });
                    };

                    /**
                     * @context googleMaps
                     */
                    vars.locationSelected = function (location, isSync) {
                        selected = false;

                        if (typeof location === 'boolean') {
                            isSync = location;
                            location = this;
                        } else {
                            location = location.place_id ? location : location.item;
                        }

                        if (!location.geometry) {
                            // If an API response with place_id but no venue data
                            if (location.description) {
                                // get venue and recall callback
                                location.google = location.place_id;
                                return Venues.getPlace(location).then(vars.locationSelected);
                            }

                            invalidationUpdate(false);

                            return DataBus.emit('yap', {
                                type: 'warning',
                                title: 'updates.WARNING',
                                body: vars.googleError = 'updates.WARNING-INVALID_GOOG_LOC',
                            });
                        } else if (scope.noGeneral && googleLoc.isLocationVague(location)) {
                            invalidationUpdate(false);

                            return DataBus.emit('yap', {
                                type: 'warning',
                                title: 'updates.WARNING',
                                body: vars.googleError = 'updates.WARNING-MORE_SPECIFIC_GOOG_LOC',
                                duration: 4250,
                            });
                        }

                        invalidationUpdate(true);

                        googleLoc.locationSelected(location, vars, isSync);

                        if (iAttrs.onSelect) {
                            scope.onSelect({
                                location: location,
                                type: 'GOOGLE',
                                passive: isSync,
                            });
                        }

                        //$(input).data('selected', true);

                        if (scope.required || iAttrs.source) {
                            scope.source = location;
                        }

                        scope.$root.safeDigest(scope);
                    };

                    /**
                     * @description Updates the location field to be valid/invalid
                     * @param {boolean} val - pass true to validate
                     */
                    invalidationUpdate = function (val) {
                        (scope.Form.loc || scope.Form).$setValidity('invalidate', val);
                    };

                    if (iAttrs.invalidate && typeof scope.invalidate === 'boolean') {
                        scope.$watch('invalidate', function (post, prev) {
                            if (post !== prev) {
                                invalidationUpdate(post);
                            }
                        });

                        invalidationUpdate(scope.invalidate);
                    }

                    if (iAttrs.getCurLoc === 'true') {
                        googleLoc.getCurrentLocation(Util).then(function (location) {
                            if (iAttrs.onSelect) {
                                scope.onSelect({
                                    location: location,
                                    type: 'GOOGLE',
                                    passive: true // since not user triggered
                                });
                            }

                            $(input).data('selected', true);

                            if (scope.required || iAttrs.source) {
                                scope.source = location;
                            }

                            scope.$root.safeDigest(scope);
                        });
                    }

                    GoogleMapApi.then(function () {
                        var options:google.maps.GeocoderRequest|google.maps.places.AutocompletionRequest = {},
                            searchFn,
                            deferred:ng.IDeferred<google.maps.places.PlaceResult[]>,
                            searchField = scope.searchType === 'geocode' ? 'address' : 'input';

                        if (iAttrs.types) {
                            options.types = scope.types.split(',');
                        }

                        if (scope.source && scope.source.coords) {
                            options.radius = 50 * 1000;
                        }

                        searchFn = googleLoc[scope.searchType](searchField, options);

                        vars.doSearch = function (query) {
                            if (deferred) {
                                deferred.reject();
                            }

                            deferred = $q.defer();

                            searchFn(query, deferred);

                            return deferred.promise;
                        };
                    });

                    scope.$on('$destroy', function () {
                        if (vars.syncObj) {
                            googleLoc.removeSync(Util, vars);
                        }
                    });
                }
            }
        };
    }
]);
