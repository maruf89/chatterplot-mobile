/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var skillLevels = require('root/config/languageLevels'), skillLevelLength = _.keys(skillLevels).length, translated = null, translating = false;
require('../../../app').directive('skillSelect', [
    'locale', 'DataBus',
    function (locale, DataBus) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                source: '=',
                name: '@',
                min: '=?',
                max: '=?',
                onChange: '&',
                showDefault: '@',
                showNative: '@'
            },
            templateUrl: 'views/misc/form/skillSelect/Template',
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var levelsLength, skillz;
                    levelsLength = skillLevelLength;
                    skillz = _.clone(skillLevels);
                    if (!scope.showNative) {
                        levelsLength -= 1;
                        delete skillz[7];
                    }
                    scope.vars = {
                        levels: skillz,
                        showDefault: !!scope.showDefault
                    };
                    if (!(translated || translating)) {
                        // to prevent duplicate processing
                        translating = true;
                        locale.ready('profile')
                            .then(function () {
                            return _.each(skillLevels, function (skill, key) {
                                skillLevels[key].display =
                                    locale.getString("profile." +
                                        skill.key);
                                if (skill.cefr) {
                                    return skillLevels[key].display +=
                                        " - " + skill.cefr;
                                }
                            });
                        });
                        translating = false;
                        translated = true;
                    }
                    // if watching the minimum, than it must be the maximum itself
                    if (iAttrs.min) {
                        scope.$watch('min.level', function (post, prev) {
                            var num;
                            if (post !== prev) {
                                num = parseInt(post, 10);
                                return scope.$root.safeApply(function () {
                                    var i, j, ref;
                                    for (i = j = 1, ref =
                                        levelsLength; 1 <=
                                        ref ? j <= ref :
                                        j >= ref; i = 1 <=
                                        ref ? ++j : --j) {
                                        if (i < num) {
                                            delete scope
                                                .vars.levels[i];
                                        }
                                        else {
                                            scope.vars.levels[i] =
                                                scope.vars
                                                    .levels[i] ||
                                                    skillLevels[i];
                                        }
                                    }
                                    return null;
                                });
                            }
                        });
                    }
                    // same with max
                    if (iAttrs.max) {
                        return scope.$watch('max.level', function (post, prev) {
                            var num;
                            if (post !== prev) {
                                num = parseInt(post, 10);
                                return scope.$root.safeApply(function () {
                                    var i, j, ref;
                                    for (i = j = 1, ref =
                                        levelsLength; 1 <=
                                        ref ? j <= ref :
                                        j >= ref; i = 1 <=
                                        ref ? ++j : --j) {
                                        if (i > num) {
                                            delete scope
                                                .vars.levels[i];
                                        }
                                        else {
                                            scope.vars.levels[i] =
                                                scope.vars
                                                    .levels[i] ||
                                                    skillLevels[i];
                                        }
                                    }
                                    return null;
                                });
                            }
                        });
                    }
                },
                post: function (scope, iElem, iAttrs) {
                    var vars;
                    vars = scope.vars;
                    if (iAttrs.onChange) {
                        return iElem.on('change', 'select', function (event) {
                            return scope.$root.safeApply(function () {
                                return scope.onChange({
                                    event: event
                                });
                            });
                        });
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map