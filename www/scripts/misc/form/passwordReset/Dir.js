/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('passwordReset', [
    'Auth', 'User', 'DataBus',
    function (Auth, User, Bus) {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'views/misc/form/passwordReset/Template.html',
            scope: {
                onSubmit: '&',
                onCancel: '&',
                onCondition: '=?'
            },
            link: {
                pre: function (scope, iElem, iAttrs) {
                    scope.vars = {
                        password: null,
                        passwordStrength: null,
                        passwordComplexity: null,
                        confirmPassword: null,
                        showCancel: !!iAttrs.showCancel,
                        forgotPassword: iAttrs.forgotPassword ===
                            'true' && Auth.isAuthenticated(),
                        popoverDir: iAttrs.popoverDir ? iAttrs.popoverDir : 'top'
                    };
                    if (!iAttrs.onCondition) {
                        scope.onCondition = true;
                    }
                },
                post: function (scope) {
                    var vars = scope.vars;
                    vars.callForgotPassword = function () {
                        return Auth.passwordResetEmail(User.data.email)
                            .then(function () {
                            return Bus.emit('yap', {
                                type: 'info',
                                title: 'auth.RESET_PASSWORD_SENT',
                                body: 'auth.RESET_PASSWORD_SENT_TEXT'
                            });
                        });
                    };
                    vars.onSubmit = function (password) {
                        return scope.onSubmit({ password: password })
                            .then(function () {
                            // Reset form on success
                            vars.password = '';
                            vars.confirmPassword = '';
                            scope.reset.$setUntouched();
                            return scope.reset.$setPristine();
                        });
                    };
                    if (vars.showCancel) {
                        return vars.onCancel = scope.onCancel;
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map