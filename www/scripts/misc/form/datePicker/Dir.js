/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var moment = require('moment');
require('../../../app').directive('datePicker', [
    '$timeout', 'locale', 'Config',
    function ($timeout, locale, Config) {
        locale.ready('events');
        return {
            restrict: 'E',
            replace: true,
            scope: {
                source: '=',
                onChange: '&',
                required: '=',
                minDate: '=?',
                format: '@',
                autoclose: '@',
                placeholder: '@',
            },
            templateUrl: 'views/misc/form/datePicker/Template.html',
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var placeholder, safetyTimer = null, safetyFn = function () {
                        return safetyTimer = false;
                    }, from, localeData = moment.localeData(moment.locale()), vars;
                    vars = scope.vars = {
                        dateShow: null,
                        placeholder: null,
                        display: scope.source instanceof Date ?
                            Config.format.date.display(scope.source, scope.format) : '',
                        showPicker: function () {
                            scope.vars.dateShow = true;
                            safetyTimer = true;
                            return $timeout(safetyFn, 200);
                        },
                        dateObj: {
                            inputDate: scope.source,
                            mondayFirst: false,
                            monthList: null,
                            weekDaysList: null,
                            from: null,
                            titleLabel: null,
                            closeLabel: null,
                            setLabel: null,
                            todayLabel: null,
                            closeOnSelect: iAttrs.autoclose === 'true',
                            callback: function (selectedDate) {
                                if (!selectedDate) {
                                    return;
                                }
                                scope.source = selectedDate;
                                vars.display = Config.format.date.display(scope.source, scope.format);
                                if (iAttrs.onChange) {
                                    scope.onChange(selectedDate);
                                }
                            },
                        },
                    };
                    if (iAttrs.disablePast) {
                        from = new Date();
                        from.setDate(from.getDate() - 1);
                        vars.dateObj.from = from;
                    }
                    placeholder = scope.placeholder || 'events.PLACEHOLDER-SELECT_DATE';
                    locale.ready('events').then(function () {
                        vars.dateObj.titleLabel = locale.getString('events.PLACEHOLDER-SELECT_DATE');
                    });
                    locale.ready('common').then(function () {
                        vars.dateObj.closeLabel = locale.getString('common.CLOSE_BOX');
                        vars.dateObj.setLabel = locale.getString('common.SET_ACTION');
                    });
                    locale.ready('misc').then(function () {
                        vars.dateObj.todayLabel = locale.getString('misc.TODAY');
                    });
                    locale.ready(locale.getPath(placeholder))
                        .then(function () {
                        return scope.vars.placeholder = locale.getString(placeholder);
                    });
                    if (localeData._months) {
                        vars.dateObj.monthList = localeData._months;
                    }
                    if (localeData._weekdaysMin) {
                        vars.dateObj.weekDaysList = _.map(localeData._weekdaysMin, function (day) {
                            return day[0] + day[1];
                        });
                    }
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map