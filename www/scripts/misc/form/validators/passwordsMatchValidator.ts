/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

/**
 * This validator upon blurring of the input field verifies
 * that the user's password matches the current password in the database
 */
require('../../../app').directive('passwordsMatchValidator', [
    '$q', 'SocketIo',
    function ($q, SocketIo) {
        return {
            require: 'ngModel',
            scope: {
                validVariable: '=passwordsMatchValidator'
            },
            link: function (scope, elem, attrs, ngModel) {
                var curPromise, curValue;
                curPromise = null;
                curValue = null;
                elem.on('blur', function () {
                    if (!curPromise) {
                        return true;
                    }
                    if (!curValue) {
                        curPromise.reject();
                        curPromise = null;
                        return true;
                    }
                    return SocketIo.onEmitSock('/user/verify', {
                            persistent: true,
                            verify: {
                                password: curValue
                            }
                        })
                        .then(function (exists) {
                            scope.validVariable = exists;
                            if (exists) {
                                return curPromise.resolve();
                            }
                            curPromise.reject();
                            return curPromise = null;
                        });
                });
                return ngModel.$asyncValidators.passwordMatches =
                    function (value) {
                        curValue = value;
                        if (curPromise) {
                            curPromise.reject();
                            curPromise = null;
                        }
                        if (!value) {
                            return $q.reject(false);
                        }
                        curPromise = $q.defer();
                        return curPromise.promise;
                    };
            }
        };
    }
]);
