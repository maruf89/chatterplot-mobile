/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../../app').directive('mustMatchValidator', [
    function () {
        return {
            require: 'ngModel',
            scope: {
                mustMatch: '=mustMatchValidator'
            },
            link: function (scope, elem, attrs, ngModel) {
                return ngModel.$validators.mustMatch = function (value) {
                    var match;
                    match = scope.mustMatch || '';
                    if (attrs.mustMatchStrict) {
                        return value && value === match;
                    }
                    else {
                        return value && value.toLowerCase() ===
                            match.toLowerCase();
                    }
                };
            }
        };
    }
]);
//# sourceMappingURL=mustMatchDir.js.map