/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var CHARSETS, MAX_COMPLEXITY, additionalComplexityForCharset, callback, charsetLength, options;
MAX_COMPLEXITY = 100;
CHARSETS = [
    [0x0030, 0x0039],
    [0x0041, 0x005a],
    [0x0061, 0x007a],
    [0x0021, 0x002f],
    [0x003a, 0x0040],
    [0x005b, 0x0060],
    [0x007b, 0x007e],
    [0x0080, 0x00ff],
    [0x0100, 0x017f],
    [0x0180, 0x024f],
    [0x0250, 0x02af],
    [0x02b0, 0x02ff],
    [0x0300, 0x036f],
    [0x0370, 0x03ff],
    [0x0400, 0x04ff],
    [0x0530, 0x058f],
    [0x0590, 0x05ff],
    [0x0600, 0x06ff],
    [0x0700, 0x074f],
    [0x0780, 0x07bf],
    [0x0900, 0x097f],
    [0x0980, 0x09ff],
    [0x0a00, 0x0a7f],
    [0x0a80, 0x0aff],
    [0x0b00, 0x0b7f],
    [0x0b80, 0x0bff],
    [0x0c00, 0x0c7f],
    [0x0c80, 0x0cff],
    [0x0d00, 0x0d7f],
    [0x0d80, 0x0dff],
    [0x0e00, 0x0e7f],
    [0x0e80, 0x0eff],
    [0x0f00, 0x0fff],
    [0x1000, 0x109f],
    [0x10a0, 0x10ff],
    [0x1100, 0x11ff],
    [0x1200, 0x137f],
    [0x13a0, 0x13ff],
    [0x1400, 0x167f],
    [0x1680, 0x169f],
    [0x16a0, 0x16ff],
    [0x1780, 0x17ff],
    [0x1800, 0x18af],
    [0x1e00, 0x1eff],
    [0x1f00, 0x1fff],
    [0x2000, 0x206f],
    [0x2070, 0x209f],
    [0x20a0, 0x20cf],
    [0x20d0, 0x20ff],
    [0x2100, 0x214f],
    [0x2150, 0x218f],
    [0x2190, 0x21ff],
    [0x2200, 0x22ff],
    [0x2300, 0x23ff],
    [0x2400, 0x243f],
    [0x2440, 0x245f],
    [0x2460, 0x24ff],
    [0x2500, 0x257f],
    [0x2580, 0x259f],
    [0x25a0, 0x25ff],
    [0x2600, 0x26ff],
    [0x2700, 0x27bf],
    [0x2800, 0x28ff],
    [0x2e80, 0x2eff],
    [0x2f00, 0x2fdf],
    [0x2ff0, 0x2fff],
    [0x3000, 0x303f],
    [0x3040, 0x309f],
    [0x30a0, 0x30ff],
    [0x3100, 0x312f],
    [0x3130, 0x318f],
    [0x3190, 0x319f],
    [0x31a0, 0x31bf],
    [0x3200, 0x32ff],
    [0x3300, 0x33ff],
    [0x3400, 0x4db5],
    [0x4e00, 0x9fff],
    [0xa000, 0xa48f],
    [0xa490, 0xa4cf],
    [0xac00, 0xd7a3],
    [0xd800, 0xdb7f],
    [0xdb80, 0xdbff],
    [0xdc00, 0xdfff],
    [0xe000, 0xf8ff],
    [0xf900, 0xfaff],
    [0xfb00, 0xfb4f],
    [0xfb50, 0xfdff],
    [0xfe20, 0xfe2f],
    [0xfe30, 0xfe4f],
    [0xfe50, 0xfe6f],
    [0xfe70, 0xfefe],
    [0xfeff, 0xfeff],
    [0xff00, 0xffef],
    [0xfff0, 0xfffd]
];
charsetLength = CHARSETS.length;
options = {
    minimumChars: 6,
    strengthScaleFactor: 1,
    invalidClass: 'invalid',
    classes: ['secure', 'strong', 'medium', 'weak'],
    strengths: [50, 40, 32.5, 25]
};
additionalComplexityForCharset = function (str, charset) {
    var i;
    i = str.length - 1;
    while (i >= 0) {
        if (charset[0] <= str.charCodeAt(i) && str.charCodeAt(i) <= charset[1]) {
            return charset[1] - charset[0] + 1;
        }
        i--;
    }
    return 0;
};
if ($.isFunction(options) && !callback) {
    callback = options;
    options = {};
}
require('../../../app').directive('passwordStrengthValidator', [
    function () {
        return {
            require: 'ngModel',
            scope: {
                passwordStrength: '=passwordStrengthValidator',
                complexity: '=passwordComplexity'
            },
            link: function (scope, elem, attrs, ngModel) {
                var minLength;
                minLength = +(attrs.ngMin || options.minimumChars);
                return ngModel.$validators.passwordStrength = function (password) {
                    var className, complexity, i, valid;
                    if (password == null) {
                        password = '';
                    }
                    complexity = 0;
                    valid = false;
                    // Add character complexity
                    i = CHARSETS.length - 1;
                    while (i >= 0) {
                        complexity += additionalComplexityForCharset(password, CHARSETS[i]);
                        i--;
                    }
                    // Use natural log to produce linear scale
                    complexity = Math.log(Math.pow(complexity, password.length)) * (1 / options.strengthScaleFactor);
                    // valid = complexity > MIN_COMPLEXITY && password.length >= minLength
                    valid = password.length >= minLength;
                    className = options.invalidClass;
                    // Scale to percentage, so it can be used for a progress bar
                    complexity = (complexity / MAX_COMPLEXITY) * 100;
                    scope.complexity = complexity = complexity > 100 ?
                        100 : complexity;
                    if (valid) {
                        _.each(options.strengths, function (strength, index) {
                            if (complexity >= strength) {
                                className = options.classes[index];
                                return false;
                            }
                        });
                    }
                    scope.passwordStrength = className;
                    return valid;
                };
            }
        };
    }
]);
//# sourceMappingURL=passwordStrengthValidator.js.map