/// <reference path="../../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../../app').directive('popoverMsg', [function () {
        return {
            restrict: 'ECA',
            link: {
                pre: function (scope, iElem, iAttrs) {
                    var $error, $focus, $info;

                    if (!/popover-msg/.test(iAttrs["class"])) {
                        iElem.addClass('popover-msg');
                    }

                    $error = $info = null;
                    $focus = iElem.find('.on-focus');

                    iElem.on('focus', '.field', function () {
                        ($error = iElem.find('.pop.error')).show();
                        ($info = iElem.find('.pop.info')).show();
                        $focus.show();
                    });

                    iElem.on('blur', '.field', function () {
                        $error && $error.hide();
                        $info && $info.hide();
                        $info = $error = null;
                        $focus.hide();
                    });
                }
            }
        };
    }
]);
