/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
require('../../app.js').directive('sharePage', [
    '$window', 'FB', 'Twitter', 'GPlus', '$location', 'locale',
    function ($window, FB, Twitter, GPlus, $location, locale) {
        return {
            restrict: 'E',
            replace: true,
            scope: {
                url: '@',
                type: '@',
                text: '@',
                textKey: '@',
                lineText: '@'
            },
            templateUrl: 'views/misc/sharePage/Template.html',
            link: {
                pre: function (scope) {
                    scope.vars = {
                        shareType: scope.lineText === 'false' ?
                            false
                            :
                                scope.lineText || (scope.type ? scope.type + ".SHARE" : 'common.SHARE_THIS_PAGE')
                    };
                    scope.url = scope.url || $location.$$absUrl;
                    if (scope.textKey) {
                        return locale.ready(locale.getPath(scope.textKey))
                            .then(function () {
                            return scope.text = locale.getString(scope.textKey);
                        });
                    }
                },
                post: function (scope, iElem) {
                    var shares = {
                        facebook: _.bind(FB.share, FB, scope),
                        twitter: _.bind(Twitter.share, Twitter, scope),
                        google: _.bind(GPlus.share, GPlus, scope)
                    };
                    iElem.on('click', '.share-btn', function (event) {
                        var which = event.currentTarget.getAttribute('data-what');
                        shares[which]();
                        if (window._gaq) {
                            return _gaq.push(['_trackSocial', which, 'share', scope.url]);
                        }
                    });
                }
            }
        };
    }
]);
//# sourceMappingURL=Dir.js.map