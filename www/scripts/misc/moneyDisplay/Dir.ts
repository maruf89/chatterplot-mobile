/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app').directive('moneyDisplay', [
    'locale',
    function (locale) {
        return {
            restrict: 'E',
            replace: true,
            template: '<span class="money-display">' +
                '<span class="value"></span>' + '</span>',
            scope: {
                priceString: '@',
                ifNone: '@',
                ifTranslate: '@'

                // If the priceString matches the matchKey, the value will be the translated key
            },
            link: {
                pre: function (scope, iElem) {
                    var $value;
                    $value = iElem.children('.value');
                    if (scope.priceString) {
                        $value.text(scope.priceString);

                        // If we have some price values to match against then let's check them
                        if (scope.ifTranslate) {

                            // Break the string up into individual checks
                            return _.every(scope.ifTranslate.split('|'),
                                function (next) {
                                    var key, ref, val;
                                    ref = next.split(':'), key = ref[
                                        0], val = ref[1];
                                    if (scope.priceString !== key) {
                                        return true;
                                    }
                                    locale.ready(locale.getPath(val))
                                        .then(function () {
                                            return $value.text(
                                                locale.getString(
                                                    val));
                                        });
                                    return false;
                                });
                        }
                    } else {
                        return locale.ready(locale.getPath(scope.ifNone))
                            .then(function () {
                                return $value.text(locale.getString(
                                    scope.ifNone));
                            });
                    }
                }
            }
        };
    }
]);
