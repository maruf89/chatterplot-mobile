/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

require('../../app').directive('inlineLoader', ['$templateCache', function ($templateCache) {
    return {
        restrict: 'A',
        replace: true,
        template: $templateCache.get('views/misc/loader/Template.html'),
    };
}]);
