/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var translators = _.shuffle(require('./translators.json')),

TranslatorThankyouCtrl = function (translatorModal) {
    this._translatorModal = translatorModal;

    this.translators = translators;
};

TranslatorThankyouCtrl.prototype = _.extend(TranslatorThankyouCtrl.prototype, {
    openModal: function () {
        var self = this,
            vars = {
                modal: {
                    deactivate: this._translatorModal.deactivate,
                    callToAction: function () {
                        if (self._translatorModal.active()) {
                            return self._translatorModal.deactivate();
                        }
                    }
                }
            };
        this._translatorModal.activate(vars);
    }
});

TranslatorThankyouCtrl.$inject = ['translatorModal'];

require('../../app').controller('TranslatorThankyouCtrl', TranslatorThankyouCtrl);
