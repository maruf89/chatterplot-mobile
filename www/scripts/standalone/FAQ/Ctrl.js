/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
var FAQCtrl, faqQuestions, modals;
faqQuestions = require('./questions.json');
modals = {
    'standalone.contact': 'contactModal',
    'standalone.abuse': 'abuseModal',
    'standalone.feedback': 'feedbackModal'
};
/**
 * @ngdoc controller
 * @class
 * @classdesc handles all of Shana's dirty work answering questions
 */
FAQCtrl = (function () {
    function FAQCtrl($rootScope, $scope, $compile) {
        var bodyClass;
        this.topics = faqQuestions;
        bodyClass = ' standalone-auto';
        $rootScope.pageClasses += bodyClass;
        // this is dirty but it will change soon
        _.defer(function () {
            return $('.FAQ')
                .find('.answer [ui-sref]')
                .each(function (index, elem) {
                var $compiled, $parent;
                $parent = $(elem)
                    .parent();
                $compiled = $compile('<div class="answer">' +
                    $parent[0].innerHTML + '</div>')($scope);
                return $parent.replaceWith($compiled);
            });
        });
        // remove the body class when destroyed
        $scope.$on('$destroy', function () {
            return $rootScope.removePageClass(bodyClass);
        });
        return this;
    }
    FAQCtrl.prototype.clickLinkCheck = function (event) {
        var modal = modals[event.target.getAttribute('ui-sref')];
        if (modal) {
            event.preventDefault();
            event.stopImmediatePropagation();
            return false;
        }
    };
    return FAQCtrl;
})();
FAQCtrl.$inject = ['$rootScope', '$scope', '$compile'];
require('../../app').controller('FAQCtrl', FAQCtrl);
//# sourceMappingURL=Ctrl.js.map