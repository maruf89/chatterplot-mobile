/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var self,
    moment = require('moment'),
    now = moment(),

    _offsetTimeZone = function (settings) {
        return this.utcOffset(settings.curTimeOffset)
            .add(now.utcOffset() - settings.curTimeOffset, 'minute');
    },

    _transposeTimeZone = function (newZone) {
        var curZone = this.utcOffset();

        // If Daylight savings time is in effect than the current utc will be an hour ahead
        if (this.isDST()) {
            curZone -= 60;
        }

        // If there's nothing to change
        if (newZone === curZone) {
            return this;
        }

        return this.utcOffset(newZone)
            .add(curZone - newZone, 'minute');
    },

    // User settings defaults - Includes map + locale mappings
    defaults = require('root/config/defaultUserSettings'),

    _cached = {
        date: {
            order: null
        }
    },

    _icoPath = '/map/icon/',

/**
 * Site config + User Settings
 */
Config = function (
    _Util,
    _SocketIo,
    _DataBus,
    _locale,
    _$analytics
) {
    this._Util = _Util;
    this._SocketIo = _SocketIo;
    this._DataBus = _DataBus;
    this._locale = _locale;
    this._$analytics = _$analytics;

    self = this;

    this.name = 'chatterplot';
    this.display = 'Chatterplot';
    this.routes = {
        home: 'landing',
        register: 'standalone.addYourself',
        onLogin: 'map.dashboard.myActivities',
        onSignup: 'map.dashboard.profile.user',
        onAccntVerify: 'map.dashboard.profile.user',
        onLogout: 'landing',
        authenticate: 'account.auth({ page: "login" })',
        dashboard: 'map.dashboard.myActivities',
        messages: 'pStandalone.messages.list',
        myActivities: 'map.dashboard.myActivities',
        eventCreate: 'map.dashboard.eventCreate',
        profile: 'map.dashboard.profile.user',
        settings: 'standalone.settings',
        search: 'map.activity.search.default',
        favorites: 'map.dashboard.favorites'
    };

    this.profile = {
        defaultAvatar: (function () {
            var folder = '',
                type = 'svg';

            if (!Modernizr.svg) {
                type = 'png';
                folder = '/fallback';
            }

            return function (gender) {
                if (gender == null) {
                    gender = 'm';
                }
                gender = gender[0].toLowerCase();
                return "/images/profile" + folder + "/default-" + gender + "." + type;
            };
        })()
    };

    this.settings = null;
    this.socialMedia = {
        facebook: {
            link: 'https://www.facebook.com/chatterplot'
        },
        twitter: {
            link: 'https://twitter.com/Chatterplot'
        },
        blog: {
            link: 'https://blog.chatterplot.com/',
            rss: 'https://blog.chatterplot.com/feed/',
        },
        gplus: {
            link: 'https://plus.google.com/+Chatterplot'
        }
    };

    this.map = {
        icon: {
            away: _icoPath + 'away_icon.svg',
            home: _icoPath + 'home_icon.svg',
            hidden: _icoPath + 'hidden.svg',
            xs: _icoPath + 'xs.svg',
            sm: _icoPath + 'sm.svg',
            md: _icoPath + 'md.svg',
            lg: _icoPath + 'lg.svg',
        }
    };

    this._defaultSettings();

    if (!this.settings) {
        throw new Error('missing locale for `configService` in `defaultUserSettings.js`');
    }

    this.settings.curTimeOffset = now.utcOffset();
    moment.fn.transposeTz = _transposeTimeZone;
    this._DataBus.on('/settings/ready', this.init.bind(this));
    this._DataBus.on('/site/deauthenticated', this._defaultSettings.bind(this));
};

Config.prototype = _.extend(Config.prototype, {
    _defaultSettings: function () {
        this.settings = _.extend(defaults.mapLocale[CP.locale], defaults.settings);
        this.settings.timeZone = now.utcOffset();
        this._locale.setLocale(this.settings.locale = CP.locale);
        moment.locale(this.settings.locale.substr(0, 2));
    },

    init: function (settings) {
        var self = this,
            _settings = this.settings,

            // convert something like `de-DE` to just `de`
            thisLocale = this._locale.getLocale().substr(0, 2).toLowerCase(),

            // do the same with any saved locales
            savedLocale = typeof settings.locale === 'string' && settings.locale.substr(0, 2).toLocaleLowerCase();

        _settings.locale = CP.locale;

        if (settings.locale && thisLocale !== savedLocale) {
            return this._localeChange(settings.locale);
        }

        // lodash doesn't support deep `defaults` so we have to manually 
        this._Util.defaultsDeep(settings, _.merge(_.cloneDeep(settings), _settings));
        this.settings = settings;
        this._checkTimeZone();

        // An ugly hack to stop updating the user's settings to the comps setting on signup 
        this._ignoreUpdates = true;
        _.delay(function () {
            return self._ignoreUpdates = false;
        }, 1000);

        // this.settings.formats.date = defaults.map.formats.date[this.settings.formats.date] 
        this._DataBus.emit('/settings/processed', settings);
    },

    format: {
        date: {
            momentFormats: {
                longDate: 'dddd, MMMM Do YYYY',
                fullDate: 'MMMM Do YYYY',
                time24: 'H:mm',
                time12: 'h:mm A'
            },

            /**
             * Formats a date object based on the user's settings/locale
             *
             * @param  {Date} date   Date object to format
             * @return {string}      Formatted
             */
            display: function (date, format) {
                var _moment;
                if (format == null) {
                    format = 'longDate';
                }
                _moment = date._isAMomentObject ? date : moment(date);
                return _moment.format(self.format.date.momentFormats[format]);
            },

            displayTime: function (date) {
                return self.format.date.display(
                    date,
                    (p(self.settings.formats.hours) === 2 ? 'time12' : 'time24')
                );
            },

            /**
             * Formats an object containing the values: `year`, `month` & `day`
             * into an array in the correct order based on the users date format preferences.
             *
             * * If no object is passed in, only returns an array of the order
             *
             * @param  {Object=} obj  Object to rearrange
             * @return {array}        Correctly ordered array
             */
            order: function (obj) {
                var order = _cached.date.order || moment.localeData().longDateFormat('L')
                    .replace(/YYYY/, 'year')
                    .replace(/MM/, 'month')
                    .replace(/DD/, 'day')
                    .split(/\/|\./);

                _cached.date.order = order;

                // Return the order if no object passed in 
                if (!obj) {
                    return order;
                }

                // Otherwise return the pieces in the correct order 
                return _.map(order, function (piece) {
                    return obj[piece];
                });
            },

            /**
             * Accepts an Elasticsearch date format and formats the date accordingly
             * http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/mapping-date-format.html
             *
             * @param  {string} format  Elasticsearch format
             * @param  {*}      args    Depends on the format
             * @return {string}         Formatted date string
             */
            toString: function (format, args) {
                var date, day, month, tz, year;

                switch (format) {
                    case 'basic_date':
                        date = args;
                        year = date.getFullYear();
                        month = self.pad(date.getMonth() + 1, 2);
                        day = self.pad(date.getDate(), 2);
                        return year + month + day;
                        break;
                    case 'hour_minute':
                        if (_.isArray(args)) {
                            return self.pad(args[0], 2) + self.pad(args[
                                1], 2);
                        } else if (typeof args === 'string') {
                            return args;
                        }
                        break;
                    case 'basic_date_time_no_millis':

                        /**
                         * {Date=}   args[0]
                         * {Array=}  args[1]  Containing [Hours, minutes, seconds, miliseconds]
                         * {Number=} args[2]  TimeZone formatted as -360, 60, 720…
                         */
                        args = args || [];
                        date = args[0] || new Date();
                        if (args[1]) {
                            date.setHours.apply(date, args[1]);
                        }
                        if (tz = args[2]) {
                            return moment(date)
                                .transposeTz(tz)
                                .format('YYYYMMDDTHHmmssZ');
                        }
                        return moment(date)
                            .formatTimeZone()
                            .format('YYYYMMDDTHHmmssZ');
                }
            },

            /**
             * The opposite of format.date.toString
             *
             * @param  {string} format      The format the date's in
             * @param  {string} dateString  the date to parse
             * @param  {Boolean} tz         If false, will ignore the timezone
             * @return {Moment}             returns a moment date object
             */
            toObj: function (format, dateString, tz) {
                format = format || 'basic_date_time_no_millis';

                switch (format) {
                    case 'basic_date_time':
                        if (tz === false) {
                            return moment(
                                dateString.substr(0, 15),
                                'YYYYMMDDTHHmmss.SSSZ'
                            );
                        }
                        return moment(dateString, 'YYYYMMDDTHHmmss.SSSZ')
                            .utcOffset(self.settings.curTimeOffset);
                        break;
                    case 'basic_date_time_no_millis':
                        if (tz === false) {
                            return moment(
                                dateString.substr(0, 15),
                                'YYYYMMDDTHHmmss'
                            );
                        }
                        return moment(dateString, 'YYYYMMDDTHHmmssZ')
                            .utcOffset(self.settings.curTimeOffset);
                }
            }
        },

        /**
         * Formats time based on the user's settings/locale
         *
         * @param  {number}  hours     required hours range between 1 and 24
         * @param  {Number=} minutes   if passed in will be formatted
         * @param  {Number=} seconds  if passed in will be formatted
         * @return {string}            Fully formated time string
         */
        time: function (hours, minutes, seconds) {
            var postfix = '',
                formatted,
                localeData;

            // According to the mappings 1: 24 hour || 2: 12 hour 
            if (p(self.settings.formats.hours) === 2) {
                // if Midnight, set to 12am
                if (hours === 24) {
                    hours = 12;
                }

                // am if before 12pm
                localeData = moment.localeData();
                postfix = ' ' + localeData.meridiem(hours);
                hours = hours <= 12 ? hours : hours - 12;
            }

            formatted = String(hours);

            if (minutes != null) {
                if (!minutes) {
                    minutes = '00';
                }
                formatted += ":" + minutes;
            }

            if (seconds != null) {
                if (!seconds) {
                    seconds = '00';
                }
                formatted += ":" + seconds;
            }

            return formatted + postfix;
        }
    },

    updateSettings: function (updates) {
        if (this._ignoreUpdates) {
            return false;
        }

        var self = this;

        return this._SocketIo.onEmitSock('/user/settings/edit', {
            edits: updates
        })

        .then(function (response) {
            _.merge(self.settings, updates);
            self._checkTimeZone();

            if ('locale' in updates) {
                self._localeChange(updates.locale, true);
            }

            self._DataBus.emit('yapServerResponse', response);
            self._DataBus.emit('progressLoader');
            return response;
        }, function (err) {
            self._DataBus.emit('yapServerResponse', err);
            throw err;
        });
    },

    /**
     * If the timezone of the user doesn't match the users browser then we need
     * to create/update the formatTimeZone method of moment
     */
    _checkTimeZone: function () {
        this.settings.curTimeOffset = now.isDST() ? this.settings.timeZone + 60 : this.settings.timeZone;

        if (this.settings.curTimeOffset !== now.utcOffset()) {
            return moment.fn.formatTimeZone = _.partial(_offsetTimeZone, this.settings);
        }

        return moment.fn.formatTimeZone = function () { return this; };
    },

    _localeChange: function (locale:string, sendAnalytics?:boolean) {
        if (this._ignoreUpdates) {
            return false;
        }

        var localeSubst:string = this._getSubdomain(locale),
            oldLocale:string = this._locale.getLocale(),
            toSubdomain:boolean = CP.site.subdomains.indexOf(localeSubst) !== -1,
            newPrefix:string = toSubdomain ? localeSubst : CP.site.defaultPrefix,
            newURL:string = location.href.replace(/\/\/[^.]+\./, '//' + newPrefix + '.'),
            sameURL:boolean = newURL === window.location.href;

        moment.locale(localeSubst);

        if (sameURL) {
            this._locale.setLocale(locale);
        }

        if (sendAnalytics) {
            self._$analytics.eventTrack('change', {
                category: 'footer',
                label: `change locale: ${oldLocale}/${locale}`,
            });
        }

        _.defer(function () {
            if (sameURL) {
                return window.location.reload();
            }

            // replace the prefix with the correct one
            window.location = newURL;
        });
    }
});

Config.$inject = [
    'Util',
    'SocketIo',
    'DataBus',
    'locale',
    '$analytics',
];

require('../app.js').service('Config', Config);
