/// <reference path="../../../../typings/chatterplot/Chatterplot.d.ts" />

'use strict';

var self:any,

stateTransition = function (
    $rootScope:cp.IRootScopeService,
    $state:ng.ui.IStateService,
    Auth:any,
    DataBus:any,
    Config:any,
    $urlRouter:ng.ui.IUrlRouterService,
    SocketIo:any
) {
    self = <any>{
        _$rootScope: $rootScope,
        _$state: $state,
        _Auth: Auth,
        _DataBus: DataBus,
        _Config: Config,
        _$urlRouter: $urlRouter,
    };

    $rootScope.$on('$stateChangeError', events.stateChangeError.bind(self));
    $rootScope.$on('$stateChangeStart', events.stateChangeStart.bind(self));
    $rootScope.$on('$locationChangeSuccess', events.locationChangeSuccess.bind(self));
    SocketIo.on('/error/expiredToken', events.checkLoginState.bind(self, true));
    DataBus.once('/site/ready', events.checkLoginState.bind(self, false));
    Auth.on('/deauthenticated', events.onLogout.bind(self));
    _initURLWatch(self);
},

_initData = {
    state:null,
    params:null,
},

/**
 * @type {array<RegExp>}
 */
ignoreUrlChanges = [],

_initURLWatch = function (self) {
    self._DataBus.on('/state/url/watch', function (patterns) {
        return [].push.apply(ignoreUrlChanges, patterns);
    });

    return self._DataBus.on('/state/url/unwatch', function (patterns) {
        if (!_.isArray(patterns)) {
            throw new Error(
                'Expecting url watch patterns parameter to be an array'
            );
        }
        return ignoreUrlChanges = _.difference(patterns, ignoreUrlChanges);
    });
},

events = {
    checkLoginState: function ():void {
        var self = this,
            onAuth = null,
            state = this._$state.current;

        if (_initData.state) {
            state = _initData.state;
            state.params = _initData.params;
            _initData.state = _initData.params = null;
        }

        if (this._Auth.getStateRoles(state)) {
            onAuth = state;
            this._Auth.isLoggedIn().then(function () {
                self._$state.go(onAuth.name, onAuth.params, {
                    reload: true,
                    location: 'replace'
                });
            });
        }
    },

    stateChangeStart: function (event, toState, toParams, fromState, fromParams, error) {
        var opts = null;

        self._Auth.stateAuthentication(event.preventDefault, toState, toParams, fromState, fromParams, error)
            .then(function () {
                // If success do nothing, If Error process 
                return arguments;
            }, function (options) {
                opts = options || {};
                if (options.initData) {
                    _.extend(_initData, options.initData);
                    if (!options.replace) {
                        self._$state.go('account.auth', {
                            page: 'login',
                            redirect: true,
                        }, {
                            location: 'replace'
                        });
                    }
                }

                // Redirect 
                if (options.replace) {
                    if (_.isArray(options.replace)) {
                        options.params = options.replace[1];
                        options.replace = options.replace[0];
                    }
                    self._$state.go(options.replace, options.params, {
                        location: 'replace'
                    });
                }
                if (options.response) {
                    switch (options.response) {
                        case 403:
                            self._DataBus.emit('yap', {
                                type: 'error',
                                title: 'updates.ERROR_EXCL',
                                body: "updates." + options.type
                            });
                    }
                }
                throw false;
            })["finally"](function () {
                var notify;
                if (opts) {
                    if (opts.error) {
                        self._DataBus.emit('yapNextPage', {
                            type: 'error',
                            title: error.title || 'Error',
                            body: error.body
                        });
                    }
                    if (notify = opts.notify) {
                        return self._DataBus.emit('yapNextPage', {
                            type: notify.type,
                            title: notify.title,
                            body: notify.body
                        });
                    }
                }
            });

        // Remove any stray loaders 
        return self._DataBus.emit('progressLoader');
    },

    /**
     * Watches over a array of Regexes (ignoreUrlChanges) that is populated
     * via `_initURLWatch()` and _DataBus watches
     *
     * If any of the patterns match and their results differ when applied to both
     * `toURL` and `fromURL` then the URL will NOT be synced
     *
     * @param {object) event - event object which allows us to preventDefault
     * @param {string} toURL
     * @param {string} fromURL
     */
    locationChangeSuccess: function (event, toURL, fromURL) {
        var router = this._$urlRouter;

        if (!ignoreUrlChanges.length) {
            return router.sync();
        }

        var a, b, i, len, pattern, lenA, lenB;

        for (i = 0, len = ignoreUrlChanges.length; i < len; i++) {
            pattern = ignoreUrlChanges[i];
            a = toURL.match(pattern);
            b = fromURL.match(pattern);
            lenA = _.isArray(a) && a.length;
            lenB = _.isArray(b) && b.length;

            if (lenA !== lenB ||
                (lenA === 1 && a[0] !== b[0]) ||
                (lenA && _.some(a.slice(1), function (val, index) {
                    // since we start from the second slot in array `a` we need to do the same in array `b`
                    return val !== b[index + 1];
                }))
            ) {
                self._DataBus.emit('/state/url/prevented', {
                    pattern: pattern,
                    toURL: toURL,
                    fromURL: fromURL,
                    sync: router.sync.bind(router),
                });
                return event.preventDefault();
            }
        }

        return router.sync();
    },

    onLogout: function () {
        var roles, state;
        self = this;
        state = self._$state.current;
        roles = self._Auth.getStateRoles(state);
        if (roles) {
            return self._$state.go(self._Config.routes.onLogout);
        }
    },

    stateChangeError: function (event, toState, toParams, fromState, fromParams,
        error) {
        self = this;
        console.log('state change error');
        if ((error != null ? error.run : void 0) && events[error.run]) {
            events[error.run].apply(self, arguments);
        }
        throw error;
    }
};

stateTransition.$inject = [
    '$rootScope',
    '$state',
    'Auth',
    'DataBus',
    'Config',
    '$urlRouter',
    'SocketIo',
];


// load template cache on dev so that we can clear caches 

if (CP.Settings.isDev) {
    stateTransition.$inject.push('$templateCache');
}

require('../app').run(stateTransition.$inject.concat([stateTransition]));
