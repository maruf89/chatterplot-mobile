/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />

"use strict";

module.exports = function (routes) {
    var map = routes.routes,
        path = routes.path;

    return {
        name: 'pStandalone',
        url: '',
        abstract: true,
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                    return $templateCache.get('views/standalone/View.html');
                }],
            }
        },
        children: [{
            name: 'messages',
            url: '',
            abstract: true,
            roles: ['SELF_VIEW'],
            views: {
                "standalone@pStandalone": {
                    templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/interaction/message/Template.html');
                    }]
                }
            },
            children: [{
                name: 'list',
                url: '/messages',
                views: {
                    "message@pStandalone.messages": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                            return $templateCache.get('views/interaction/message/list/Template.html');
                        }],
                        controller: 'MessagingListCtrl as Interaction',
                    }
                },
            }, {
                name: 'single',
                url: '/messages/:msgID',
                roles: ['SELF_VIEW'],
                views: {
                    "message@pStandalone.messages": {
                        templateProvider: ['$templateCache', function ($templateCache) {
                            return $templateCache.get('views/interaction/message/single/Template.html');
                        }],
                        controller: 'MessageSingleCtrl as Single',
                    }
                },
                resolve: {
                    Conversation: [
                        '$stateParams', 'Interaction', 'User', '$state',
                        function ($stateParams, Messaging, User, $state) {
                            var users = Messaging.usersFromID($stateParams.msgID, true),
                                withUsers = !_.some(users, function (
                                    userID) {
                                    var user = User.users[userID];
                                    return user && (user.picture || user.details);
                                });

                            return Messaging.get({
                                msgID: $stateParams.msgID,
                                conversation: true,
                                messages: true,
                                withUsers: withUsers
                            }).catch(function () {
                                throw $state.go('pStandalone.messages.list');
                            });
                        }
                    ]
                }
            }]
        }]
    }
};
