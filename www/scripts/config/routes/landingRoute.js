/// <reference path="../../../../../typings/chatterplot/Chatterplot.d.ts" />
'use strict';
module.exports = function (routes) {
    var map = routes.routes;
    return {
        name: 'landing',
        url: '/',
        views: {
            "site": {
                templateProvider: ['$templateCache', function ($templateCache) {
                        return $templateCache.get('views/landing/Template.html');
                    }],
            }
        },
    };
};
//# sourceMappingURL=landingRoute.js.map