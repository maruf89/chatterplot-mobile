'use strict';

const path = require('path');
const Q = require('q');
const _ = require('lodash-node/modern');
const fs = require('fs');
const md5 = require('MD5');

const defaultEnvironments = {
    build: 'production',
    dev: 'development',
    staging: 'staging',
};

const supportedLocales = require('../common/config/locales.json');
const devLocales = {
    'en': null,
};

module.exports = function (grunt) {
    var jsonConfig = grunt.file.readJSON('../wwwconfig.json'),
        defEnv = grunt.cli.tasks.length ? grunt.cli.tasks[0] : 'development',
        targetenv = grunt.option('targetenv') || defaultEnvironments[defEnv] || 'development',
        envConfig = jsonConfig[targetenv],

        iconHashVar = 'iconHashres',
        jsHashVar = 'jsHashres',
        preloadHashVar = 'preloadHashVar';

    grunt.option('targetenv', targetenv);

    // Deduce the hashed var from the icon files
    if (targetenv === 'production' && !grunt.option(iconHashVar)) {
        let icons = fs.readdirSync('../dist/public/fonts/icomoon'),
            hashVar,
            hashVar2;

        if (icons.some(function (file) {
            return file.substr(0, 3) === 'ico' &&
                (hashVar = file.match(/(?:icomoon\.)([^.]+)/)) &&
                (hashVar = hashVar[1]);
        })) {
            grunt.option(iconHashVar, '.' + hashVar);
        }

        let preload = fs.readdirSync('../dist/public/scripts');
        if (preload.some(function (file) {
            return file.substr(0, 7) === 'preload' &&
                (hashVar2 = file.match(/(?:preload\.min\.)([^.]+)/)) &&
                (hashVar2 = hashVar2[1]);
        })) {
            grunt.option(preloadHashVar, '.' + hashVar2);
        }
    }

    require('jit-grunt')(grunt, {
        ngtemplates: 'grunt-angular-templates',
    });

    // Time how long tasks take. Can help when optimizing build times
    //require('time-grunt')(grunt);

    // Define the configuration for all the tasks
    grunt.initConfig({
        config: {
            app:            'www',
            scripts:        'www/scripts',
            styles:         'www/styles',
            lib:            'www/lib',
            templates:      'www/views',
            ionic:          'www/lib',
            external:       'www/external',
            common:         '../common',
            assets:         '../common/assets',
            distCommon:     '../dist/common',
            commonFront:    '../common/front',
            components:     '../common/front/bower_components',
            modules:        '../common/front/modules',
            libraries:      '../common/front/libraries',
            commonSections: '../common/front/sections',
            viewsDev:       '../views/m',
            publicDev:      '../public/m',
            distTemp:       '../dist/.tmp/m',
            nodeModules:    './node_modules',
            dist:           '../dist',
            distCommonFront:'../dist/common/front',
            publicDist:     '../dist/public/m',
            viewsDist:      '../dist/views/m',
            cacheBuster:    '',
        },

        browserify: {
            dev: {
                options: {
                    watch: false // doesn't always work
                },
                files: _.reduce(devLocales, function (obj, val, prefix) {
                    obj['<%= config.publicDev %>/scripts/bundle.' + prefix + '.js'] = [
                        '<%= config.commonSections %>/config/locales/' + prefix + '.app.js',
                        '<%= config.scripts %>/*/**/*.js',
                    ];

                    return obj;
                }, {})
            },
            dist: {
                files: _.reduce(supportedLocales.defaultMap, function (obj, val, prefix) {
                    obj['<%= config.distTemp %>/scripts/' + prefix + '.app.js'] =
                        '<%= config.commonSections %>/config/locales/' + prefix + '.app.js';

                    return obj;
                }, {
                    '<%= config.distTemp %>/scripts/main.js': '<%= config.app %>/scripts/*/**/*.js'
                }),
            }
        },

        // Empties folders to start fresh
        clean: {
            options: {
                force: true
            },
            dev: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.viewsDist %>',
                        '<%= config.publicDev %>',
                        '<%= config.templates %>',
                        '<%= config.scripts %>/bundle.js',
                        '<%= config.scripts %>/chatterplot.templates.js',
                        '../views/m',
                    ]
                }]
            },
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.publicDist %>',
                        '<%= config.viewsDist %>',
                        '<%= config.viewsDev %>',
                        '<%= config.distTemp %>',
                        '<%= config.templates %>',
                        '<%= config.scripts %>/bundle.js',
                        '<%= config.scripts %>/chatterplot.templates.js',
                    ]
                }]
            },
            server: '<%= config.publicDev %>',
            styles: '<%= config.publicDev %>/styles',
            scripts: '<%= config.publicDev %>/scripts',
            devStylusMain: {
                files: [{
                    dot: true,
                    src: [
                        '<%= config.styles %>/main.css',
                        '<%= config.publicDev %>/styles/main.css',
                    ]
                }]
            },
            document: './doc',
            docBundle: './doc/source/front/bundle.js',
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            dev1: [
                'copy:stylus2Styles',
                'copy:jade2Templates',
            ],
            dev2: [
                'replace:dev',
                'jade:some',
            ],
            dev3: [
                'clean:devStylusMain',
                'browserify:dev',
                'jade:remaining',
            ],
            nodemon: {
                options: {
                    logConcurrentOutput: true
                },
                tasks: [
                    'nodemon:dev',
                    'node-inspector:custom',
                    'watch',
                ]
            },
            preDist: [
                'copy:stylus2Styles',
                'copy:jade2Templates',
                'symlink:dist',
            ],
            dist: [
                'copy:dist',
                'jade:some',
            ],
            dist2: [
                'browserify:dist',
                'replace:dist',
                'ngtemplates:dist',
            ],
            dist3: [
                'stylus:dist',
                'jade:remaining',
                'jadeUsemin:dist',
            ],
            uglify: [
                'uglify:distNoMangle',
            ],
            dist5: [
                'replace:dist2',
            ],
        },

        concat: {
            dist: {
                files: _.reduce(supportedLocales.defaultMap, function (obj, val, prefix) {
                    obj['<%= config.publicDist %>/scripts/main.' + prefix + '.min.js'] =[
                        '<%= config.distTemp %>/scripts/scripts.min.js',
                        '<%= config.distTemp %>/scripts/' + prefix + '.app.min.js',
                        '<%= config.distTemp %>/scripts/main.min.js'
                    ];

                    return obj;
                }, {})
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,                       // Favicon
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.publicDist %>',
                    src: [
                        'favicon.ico',
                        'robots.txt',
                    ]
                }, {
                    expand: true,                       // Jade Templates
                    dot: true,
                    cwd: '<%= config.templates %>',
                    dest: '<%= config.viewsDist %>',
                    src: [
                        '**/*.jade',
                    ]
                }, {
                    expand: true,                       // CSS Stylus files
                    dot: true,
                    cwd: '<%= config.app %>/styles',
                    dest: '<%= config.publicDist %>/styles',
                    src: [
                        '{,*/}*.{styl,css}',
                        '**/{,*/}*.{styl,css}',
                    ]
                }, {
                    expand: true,                       // Javascript
                    dot: true,
                    cwd: '<%= config.app %>',
                    dest: '<%= config.distTemp %>',
                    src: [
                        'scripts/app.js',
                        'scripts/*/**/*.{js,json}',
                    ]
                }]
            },

            // For dev copying .jade files in /script to /views
            jade2Templates: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.scripts %>',
                    dest: '<%= config.templates %>',
                    src: [
                        '**/*.jade',
                        '!index.jade',
                    ]
                }]
            },

            stylus: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.styles %>',
                    dest: '<%= config.publicDev %>/styles/',
                    src: [
                        '**/*.{styl,css}',
                    ]
                }]
            },

            stylus2Styles: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.scripts %>',
                    dest: '<%= config.styles %>',
                    src: [
                        '**/*.{styl,css}',
                    ]
                }, {
                    expand: true,
                    dot: true,
                    cwd: '<%= config.components %>/MProgress/src/css',
                    dest: '<%= config.styles %>/_plugins',
                    src: '*.styl',
                }]
            },

            distStylus: {
                files: {
                    '<%= config.publicDist %>/styles/main.styl': '<%= config.styles %>/main.styl'
                }
            },

            document: {
                files: [{
                    expand: true,                       // CSS Stylus files
                    dot: true,
                    cwd: '<%= config.scripts %>',
                    dest: 'doc/source/front',
                    src: [
                        '**/*.js',
                    ]
                }]
            }
        },

        hashres: {
            options: {
                fileNameFormat: '${name}.${hash}.${ext}',
                renameFiles: true
            },
            dist: {
                dest: ['<%= config.viewsDist %>/index.jade'],
                src: [
                    '<%= config.publicDist %>/styles/main.css',
                    '<%= config.publicDist %>/scripts/preload.min.js',
                    '<%= config.publicDist %>/scripts/main.min.js',
                ]
            },
        },

        jade: {
            all: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.templates %>',
                    dest: (targetenv == 'production' ? '<%= config.distTemp %>/views' : '<%= config.viewsDev %>'),
                    ext: '.html',
                    src: [
                        '**/*.jade',
                        '!index.jade',
                    ]
                }]
            },
            some: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.templates %>',
                    dest: (targetenv == 'production' ? '<%= config.distTemp %>/views' : '<%= config.viewsDev %>'),
                    ext: '.html',
                    src: [
                        '**/*.jade',
                        '!index.jade',
                        '!standalone/pages/aboutTemplate.jade',
                        '!standalone/pages/privacyPolicyTemplate.jade',
                        '!standalone/pages/termsServiceTemplate.jade',
                        '!standalone/eventIdeas/Template.jade',
                        '!standalone/404/Template.jade',
                    ]
                }]
            },
            remaining: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= config.templates %>',
                    dest: (targetenv == 'production' ? '<%= config.distTemp %>/views' : '<%= config.viewsDev %>'),
                    ext: '.html',
                    src: [
                        '**/*.jade',
                        'standalone/pages/termsServiceTemplate.jade',
                        'standalone/pages/privacyPolicyTemplate.jade',
                        'standalone/pages/aboutTemplate.jade',
                        'standalone/eventIdeas/Template.jade',
                        'standalone/404/Template.jade',
                    ]
                }]
            },
        },

        // Performs rewrites based on rev and the useminPrepare configuration
        jadeUsemin: {
            dist: {
                options: {
                    prefix: '../dist/public/',
                    tasks: {
                        js: ['concat']
                    }
                },
                files: {
                    '<%= config.viewsDist %>/index.jade': '<%= config.viewsDist %>/index.jade'
                }
            }
        },

        jsdoc: {
            dev: {
                src: [
                    'doc/source/**/*.js',
                ],
                jsdoc: '/usr/local/bin/jsdoc',
                options: {
                    destination: 'doc',
                    template : "node_modules/ink-docstrap/template",
                    configure : "jsdoc.conf.json",
                },
            },
        },

        // Allow the use of non-minsafe AngularJS files. Automatically makes it
        // minsafe compatible so Uglify does not destroy the ng references
        ngAnnotate: {
            options: {
                singleQuotes: true
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= config.publicDist %>/scripts',
                    src: ['{,*/}*.js'],
                    dest: '<%= config.publicDist %>/scripts',
                    replace: true
                }]
            }
        },

        ngtemplates: {
            dev: {
                cwd: '<%= config.viewsDev %>',
                src: [
                    '**/*.html',
                    '!index.html',
                ],
                dest: '<%= config.scripts %>/chatterplot.templates.js',
                options: {
                    module: 'chatterplot.templates',
                    prefix: 'views/',
                    standalone: true,
                    htmlmin: {
                        removeRedundantAttributes:      true,
                    }
                }
            },
            dist: {
                cwd: '<%= config.distTemp %>/views',
                src: [
                    '**/*.html',
                    '!index.html',
                ],
                dest: '<%= config.scripts %>/chatterplot.templates.js',
                options: {
                    module: 'chatterplot.templates',
                    prefix: 'views/',
                    standalone: true,
                    htmlmin: {
                        collapseBooleanAttributes:      true,
                        collapseWhitespace:             true,
                        removeAttributeQuotes:          true,
                        removeComments:                 true, // Only if you don't use comment directives!
                        removeEmptyAttributes:          true,
                        removeScriptTypeAttributes:     true,
                        removeRedundantAttributes:      true,
                        removeStyleLinkTypeAttributes:  true,
                    }
                }
            }
        },

        rename: {
            cacheBuster: {
                files: [
                    {
                        src: '<%= config.publicDist %>/scripts/main.min.js',
                        dest: '<%= config.publicDist %>/scripts/main.min<%= config.cacheBuster %>.js'
                    },
                    {
                        src: '<%= config.publicDist %>/scripts/preload.min.js',
                        dest: '<%= config.publicDist %>/scripts/preload.min<%= config.cacheBuster %>.js'
                    },
                    {
                        src: '<%= config.publicDist %>/styles/main.styl',
                        dest: '<%= config.publicDist %>/styles/main<%= config.cacheBuster %>.styl'
                    }
                ]
            }
        },

        replace: {
            options: {
                patterns: [{
                    json: envConfig
                }]
            },
            dev: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.scripts %>/index.jade'],
                        dest: '<%= config.viewsDev %>'
                    }
                ]
            },
            dist: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: ['<%= config.scripts %>/index.jade'],
                        dest: '<%= config.viewsDist %>'
                    }
                ]
            },
            dist2: {
                options: {
                    patterns: [{
                        json: {
                            "jsHashVar": grunt.option(jsHashVar),
                            "preloadHashVar": grunt.option(preloadHashVar),
                        }
                    }]
                },
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: '<%= config.viewsDist %>/index.jade',
                        dest: '<%= config.viewsDist %>'
                    }
                ]
            },
        },

        setHashVar: {
            dist2: {
                expand: true,
                dot: true,
                cwd: '<%= config.publicDist %>/scripts',
                dest: '<%= config.publicDist %>/scripts',
                hashVar: jsHashVar,                     // <!--- Important: needed
                src: [
                    'main.*.min.js',
                ]
            }
        },

        stylus: {
            options: {
                paths: [
                    '<%= config.assets %>/styles',
                    '<%= config.assets %>/images',
                    '<%= config.components %>',
                    '<%= config.external %>',
                ],
                urlfunc: 'embedurl', // use embedurl('test.png') in our code to trigger Data URI embedding
                use: [
                    require('nib'), // use stylus plugin at compile time
                    require('rupture'),
                ],
                define: {
                    "iconHashres": grunt.option(iconHashVar) || '', // corresponds to `setHashVars`
                    "cdnBase": envConfig.cdnUrl,
                    "imageSizes": require('./www/scripts/config/imageSizes.json'),
                },
                compress: true,
                rawDefine: true,
                "include css": true,
            },
            dev: {
                files: {
                    '<%= config.publicDev %>/styles/main.css': '<%= config.styles %>/main.styl',
                }
            },
            dist: {
                files: {
                    '<%= config.publicDist %>/styles/main.css': '<%= config.styles %>/main.styl',
                }
            },
        },

        symlink: {
            dev: {
                files: [{
                    src: '<%= config.styles %>',
                    dest: '<%= config.publicDev %>/styles'
                }, {
                    src: '<%= config.scripts %>',
                    dest: '<%= config.publicDev %>/scripts'
                }, {
                    src: '<%= config.lib %>',
                    dest: '<%= config.publicDev %>/bower_components'
                }, {
                    src: '<%= config.ionic %>/ionic/fonts',
                    dest: '<%= config.publicDev %>/assets/fonts/ionicons'
                }]
            },
            dist: {
                files: [
                    {
                        src: '<%= config.lib %>',
                        dest: '<%= config.publicDist %>/bower_components',
                    }, {
                        src: '<%= config.ionic %>/ionic/fonts',
                        dest: '<%= config.publicDist %>/assets/fonts/ionicons'
                    }
                ]
            },
        },

        uglify: {
            distNoMangle: {
                options: {
                    mangle: false,
                },
                files: _.reduce(supportedLocales.defaultMap, function (obj, val, prefix) {
                    obj['<%= config.distTemp %>/scripts/' + prefix + '.app.min.js'] =
                        '<%= config.distTemp %>/scripts/' + prefix + '.app.js';

                    return obj;
                }, {
                    '<%= config.distTemp %>/scripts/main.min.js': '<%= config.distTemp %>/scripts/main.js'
                })
            }
        },

        watch: {
            options: {
                spawn: false,
            },
            js: {
                files: [
                    '<%= config.scripts %>/app.js',
                    '<%= config.scripts %>/*/**/*.js',
                    '<%= config.commonFront %>/modules/*.js',
                    '<%= config.commonFront %>/util/*.js',
                    '<%= config.commonFront %>/sections/**/*.js',
                ],
                tasks: [
                    'browserify:dev'
                ],
                options: {
                    spawn: false
                }
            },
            server: {
                files: ['.rebooted'],
                options: {
                    spawn: false,
                }

            },
            scriptsJade: {
                files: [
                    '<%= config.scripts %>/**/*.jade',
                    '!<%= config.scripts %>/index.jade',
                ],
                tasks: [
                    'newer:copy:jade2Templates',
                    'jade:some',
                    'ngtemplates:dev',
                    'browserify:dev',
                ],
            },
            indexJade: {
                files: '<%= config.scripts %>/index.jade',
                tasks: ['replace:dev'],
                options: {
                    spawn: false,
                }
            },
            stylus2Styles: {
                files: [
                    '<%= config.scripts %>/**/*.styl',
                ],
                tasks: [
                    'newer:copy:stylus2Styles',
                    'clean:devStylusMain',
                    'stylus:dev',
                ],
                options: {
                    spawn: false,
                }
            },
            stylus: {
                files: [
                    '<%= config.styles %>/*.styl',
                    '<%= config.styles %>/elements/*.styl',
                    '<%= config.styles %>/modules/**/*.styl',
                    //'<%= config.styles %>/_plugins/*.styl',
                ],
                tasks: [
                    'clean:devStylusMain',
                    'stylus:dev',
                ],
                options: {
                    spawn: false,
                }
            }
        },
    });

    require('../tasks/setHashVar')(grunt);

    grunt.registerTask('devBase', [
        'clean:dev',
        'copy:stylus',
        'concurrent:dev1',
        'symlink:dev',
        'concurrent:dev2',
        'ngtemplates:dev',
        'concurrent:dev3',
        'stylus:dev',
    ]);

    grunt.registerTask('dev', [
        'devBase',
        'watch',
    ]);

    grunt.registerTask('d', [
        'watch',
    ]);

    grunt.registerTask('staging', [
        'devBase',
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'concurrent:preDist',
        'concurrent:dist',
        'concurrent:dist2',
        'concurrent:dist3',
        'concurrent:uglify',
        'concat:dist',
        'setHashVar:dist2',
        'hashres',
        'concurrent:dist5',
        'copy:distStylus',
    ]);

    grunt.registerTask('document', [
        'clean:document',
        'copy:document',
        'clean:docBundle',
        'jsdoc',
    ]);
};
